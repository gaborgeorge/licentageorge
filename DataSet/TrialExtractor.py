import sys
import os
sys.path.append(os.path.join('..'))

import numpy as np

from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
import config
'''

Trials have the timestamps of the events [Start, On, Off, End] and the values and timestamps of each spike for each neuron

Trials = [ T1       T2              T3              ....]
                                    |
                                    V
                    [ 'Events timestampts' : t,          'spikes': s                       'meta':m]
                                    |                               |                         |
                                    V                               |                         |
                    [ 'start':t1, 'on':t2, 'off':t3, 'end':t4]      |                         |
                                                                    |                         |
                                            [ neuron1_spikes, neuron2_spikes, ....]           |
                                                    |                                         V
                                                    V                               {'cond_number':cn, 'contrast':c, 'orientation':o}
                                                [ ..... ]



OBS: WE will need also the orientation and the rest

'''

def scale(value, new_min, new_max, old_min, old_max):

    value = (value - old_min) * (new_max - new_min) / (old_max - old_min) + new_min

    return value


class TrialExtractor:
    '''
    parameters: - spike_reader - instance of SpikeTimestampReader
                - trial_meta_reader - instance of TrialMetadataReader
                - use_mean_amplitude - if it should put the value of the global mean of amplitudes
                                        insteed of the read value
                - use_mean_neuron - if it should put the value of the mean amplitude
                                        of each neuron for its amplitudes values
                - use_start - if it should include the interval [start, on]
                                of the trial
                - use_end - if it should include the interval [off, end]
                                of the trial
                - use_z_score - if it should convert every amplitude in range [0.5, 1.5] with mean 1
                                  no amplitude means 0
    '''
    def __init__(self, spike_reader, trial_meta_reader, use_mean_amplitude=False, use_mean_neuron=False, use_start=False, use_end=True, use_z_score=False, use_fisher=False):
        self.spike_reader = spike_reader
        self.trial_meta = trial_meta_reader
        self.trials = []
        self.use_mean = use_mean_amplitude
        self.use_mean_neuron = use_mean_neuron
        self.use_start = use_start
        self.use_end = use_end
        self.use_z_score = use_z_score
        self.use_fisher = use_fisher
        self.extract_trials()

    '''
    Description: - for each trial computes:
                        - a list of amplitudes and their timestamps
                    relative to the beginning of the trial (start or on)
                        - their event timestamps relative to the beggining
                        - the trial metadata
    '''
    def extract_trials(self):
        # indexes for the spikes of each neuron, we need to keep them because of how the trials were strutured
        neurons_spike_indexes = np.zeros((self.spike_reader.metadata.no_units,), dtype=np.int32)

        self.normal_amplitudes = []
        self.fisher_amplitudes = []

        # iterate over trials
        for i in range(self.spike_reader.trial_timestamps.shape[0]):
            trial_events = self.spike_reader.trial_timestamps[i]

            if self.use_start is True:
                trial_begin = trial_events['start']
            else:
                trial_begin = trial_events['on']

            if self.use_end is True:
                trial_end = trial_events['end']
            else:
                trial_end = trial_events['off']

            neurons_spikes = []
            # iterate over each neuron
            for neuron_index in range(self.spike_reader.metadata.no_units):

                neuron_spikes = []

                # move neuron spike index to the first spike
                while neurons_spike_indexes[neuron_index] < len(self.spike_reader.timestamps[neuron_index]) and \
                        self.spike_reader.timestamps[neuron_index][neurons_spike_indexes[neuron_index]] < trial_begin:
                    neurons_spike_indexes[neuron_index] += 1

                # read all spike until the end of trial
                while neurons_spike_indexes[neuron_index] < len(self.spike_reader.timestamps[neuron_index]) and \
                        self.spike_reader.timestamps[neuron_index][neurons_spike_indexes[neuron_index]] <= trial_end:

                    # determin the value of the spike
                    if self.use_mean is True:
                        amplitude = self.spike_reader.mean_amplitude
                    elif self.use_mean_neuron is True and self.use_z_score is True:
                        amplitude = 1.0
                    elif self.use_mean_neuron is True:

                        amplitude = self.spike_reader.mean_amplitudes[neuron_index]
                    elif self.use_z_score is True:
                        z_score = (self.spike_reader.amplitudes[neuron_index][neurons_spike_indexes[neuron_index]] - self.spike_reader.mean_amplitudes[neuron_index]) / \
                                    self.spike_reader.standard_deviations[neuron_index]
                        amplitude = z_score / 2.0 + 1.0 # should be close to range(0.5, 1.5) with mean 1
                    elif self.use_fisher is True and self.use_mean_neuron is True:
                        amplitude = self.spike_reader.mean_amplitudes[neuron_index]
                        # scale to [-0.95, 0.95]
                        amplitude = scale(amplitude, -0.95, 0.95, self.spike_reader.min, self.spike_reader.max)

                        amplitude = np.arctanh(amplitude)

                        # scale it back
                        amplitude = scale(amplitude, self.spike_reader.min, self.spike_reader.max, np.arctanh(-0.95), np.arctanh(0.95))

                    elif self.use_fisher is True:
                        amplitude = self.spike_reader.amplitudes[neuron_index][neurons_spike_indexes[neuron_index]]
                        # scale to [-0.95, 0.95]
                        amplitude = scale(amplitude, -0.95, 0.95, self.spike_reader.min, self.spike_reader.max)

                        amplitude = np.arctanh(amplitude)

                        # scale it back
                        amplitude = scale(amplitude, self.spike_reader.min, self.spike_reader.max, np.arctanh(-0.95), np.arctanh(0.95))
                    else:
        
                        amplitude = self.spike_reader.amplitudes[neuron_index][neurons_spike_indexes[neuron_index]]

                    neuron_spikes.append(((self.spike_reader.timestamps[neuron_index][neurons_spike_indexes[neuron_index]] - trial_begin) // 32, amplitude))
                    neurons_spike_indexes[neuron_index] += 1

                neurons_spikes.append(neuron_spikes)

            events = {}
            if self.use_start is True:
                events['start'] = (trial_events['start'] - trial_events['start']) // 32
                events['on'] = (trial_events['on'] - trial_events['start']) // 32
                events['off'] = (trial_events['off'] - trial_events['start']) // 32
                if self.use_end is True:
                    events['end'] = (trial_events['end'] - trial_events['start']) // 32
                    self.mili_duration = config.mili_between_START_END
                else:
                    self.mili_duration = config.mili_between_START_OFF
            else:
                events['on'] = (trial_events['on'] - trial_events['on']) // 32
                events['off'] = (trial_events['off'] - trial_events['on']) // 32
                if self.use_end is True:
                    events['end'] = (trial_events['end'] - trial_events['on']) // 32
                    self.mili_duration = config.mili_between_ON_END
                else:
                    self.mili_duration = config.mili_between_ON_OFF

            self.trials.append({'events_timestamps':events, 'spikes':neurons_spikes, 'meta':self.trial_meta.trials_meta[i]})

        print('[{0}.{1}] COMPLETE - {2} Trials'.format(self.__class__.__name__, sys._getframe().f_code.co_name, len(self.trials)))
