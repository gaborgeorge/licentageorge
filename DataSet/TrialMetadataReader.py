import sys
import os

debug=False

class TrialMetadataReader:
    def __init__(self, trial_meta_file):
        self.meta_file = trial_meta_file
        self.__read_trials_metadata()

    '''
    Description: - reads the information associated to every trial
                            - condition number
                            - contrast
                            - direction
    '''
    def __read_trials_metadata(self):
        with open(self.meta_file, 'r') as f:
            lines = f.readlines()

        self.trials_meta = []

        # we start at line 4
        for i in range(3, len(lines)):
            # separate data
            metadata = lines[i].split(',')

            trial_meta = {}

            # extract Condition number
            trial_meta['cond_number'] = int(metadata[1])

            # extract Contrast
            contrast_meta = metadata[2].split(' ')
            trial_meta['contrast'] = int(contrast_meta[1])

            # extract Direction
            direction_meta = metadata[3].split(' ')
            trial_meta['direction'] = int(direction_meta[2])

            # if need extract Duration and Error timing flag

            if debug is True:
                print('[{0}.{1}] Trial {2} meta {3}'\
                    .format(self.__class__.__name__, sys._getframe().f_code.co_name, i, trial_meta))

            self.trials_meta.append(trial_meta)

        print('[{0}.{1}] COMPLETE - {2} Trials'.format(self.__class__.__name__, sys._getframe().f_code.co_name, len(lines) - 3))

    '''
    shows statistics about constrasts in the dataset
    '''
    def trials_with_constrast_per_orientation(self):

        # print all constrast values
        contrasts = []
        for meta in self.trials_meta:
            contrasts.append(meta['contrast'])
        # convert to a set to remove duplicates
        unique_contrasts = set(contrasts)
        print('[{0}.{1}] Unique contrast values {2}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, set(unique_contrasts)))

        # extract for each orientation the number of trials with a given contrast
        orientations = {0:{}, 45:{}, 90:{}, 135:{}, 180:{}, 225:{}, 270:{}, 315:{}}
        for meta in self.trials_meta:
            if meta['contrast'] in orientations[meta['direction']]:
                orientations[meta['direction']][meta['contrast']] += 1
            else:
                orientations[meta['direction']][meta['contrast']] = 1

        print('[{0}.{1}] Number of trials with a given contrast for every orientation {2}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, orientations))
