'''
Written by George Gabor
Date 17.01.2019

This module read the amplitudes and corresponding timestamps of each neuron over the trial periond
'''

import numpy as np
import sys
import os
sys.path.append(os.path.join('..'))
from DataSet.MetadataReader import MetadataReader

class SpikeTimestampReader:
    '''
    parameters: - path_ssd: path to the folder containing the .ssd file
                - file_ssd: name of the ssd file in path_ssd

    # NOTE: Uses a MetadataReader instance to get the information about the
                spikes and events
    '''
    def __init__(self, path_ssd, file_ssd):
        self.metadata = MetadataReader(path_ssd, file_ssd)
        if (self.metadata.valid == False):
            return

        self.extract_spikes_and_timestamps()
        self.extract_events_and_codes()


    '''
    Description: - Reads the Spikes amplitudes and their timestamps
                 - Computes statistical values:
                                    - mean amplitude of all the amplitudes
                                    - mean amplitude of the amplitudes for each neuron
                                    - standard deviation of the amplitudes for each neuron
                        These informations are used by the Trial Extractor
    '''
    def extract_spikes_and_timestamps(self):

        self.timestamps = []
        self.amplitudes = []

        sum_of_amplitudes = 0.0
        cnt_of_amplitudes = 0

        # the mean amplitude for each neuron
        self.mean_amplitudes = np.zeros(self.metadata.no_units)
        self.standard_deviations = np.zeros(self.metadata.no_units)
        self.min = 1000
        self.max = 0

        with open(os.path.join(self.metadata.dataset_path, self.metadata.file_spike_timestamps), 'rb') as spike_file, \
                open(os.path.join(self.metadata.dataset_path, self.metadata.file_spike_waveform), 'rb') as waveform_file:

            # read timestamps and spikes for every neuron
            for i in range(self.metadata.no_units):
                # read 4 * number of spikes per neuron bytes,
                # because the timestamp values are uint32
                timestamps_bytes = spike_file.read(self.metadata.no_spikes_per_unit[i] * 4)

                # read 4 * waveform_length * number of spikes per neuron bytes
                # because a waveform gives one amplitude and it has a length
                # also waveform values are float32
                waveform_bytes = waveform_file.read(self.metadata.no_spikes_per_unit[i] * self.metadata.waveform_length_samples * 4)

                # convert to numpy arrays
                timestamps_array = np.frombuffer(timestamps_bytes, dtype=np.uint32)
                waveform_array = np.frombuffer(waveform_bytes, dtype=np.float32)

                # extract amplitudes from waveform and take the module
                # multiply with wave lenght and add the offset
                amplitude_indexes = np.arange(self.metadata.no_spikes_per_unit[i]) * self.metadata.waveform_length_samples + self.metadata.waveform_spike_align_offset
                amplitude_array = np.abs(waveform_array[amplitude_indexes])

                if amplitude_array.shape != timestamps_array.shape:
                    print('[{0}.{1}] Amplitudes and timestamps length don\' match for neuron {2}. {3}!={4}'\
                        .format(self.__class__.__name__, sys._getframe().f_code.co_name, i, amplitude_array.shape, timestamps_array.shape))
                    return

                # save information about neuron
                self.timestamps.append(timestamps_array)
                self.amplitudes.append(amplitude_array)

                # sum the amplitudes
                sum_of_amplitudes += np.sum(amplitude_array)
                cnt_of_amplitudes += amplitude_array.shape[0]

                c_max = amplitude_array.max()
                c_min = amplitude_array.min()

                if c_max > self.max:
                    self.max = c_max

                if c_min < self.min:
                    self.min = c_min

                self.mean_amplitudes[i] = np.sum(amplitude_array) / self.metadata.no_spikes_per_unit[i]
                self.standard_deviations[i] = np.sqrt(np.sum((amplitude_array - self.mean_amplitudes[i])**2) / self.metadata.no_spikes_per_unit[i])

        self.mean_amplitude = sum_of_amplitudes / cnt_of_amplitudes
        print('[{0}.{1}] COMPLETE - Mean Amplitude {2}, Mean Amplitudes {3}'\
                        .format(self.__class__.__name__, sys._getframe().f_code.co_name, self.mean_amplitude, self.mean_amplitudes))


    '''
    Description: - reads all the events codes and all their timestamps

    # NOTE: Currently the codes are repetitions off 128 129 150 192, may need change
                if more event codes appear
    '''
    def extract_events_and_codes(self):
        with open(os.path.join(self.metadata.dataset_path, self.metadata.file_event_timestamps), 'rb') as event_time_file, \
                open(os.path.join(self.metadata.dataset_path, self.metadata.file_events_codes), 'rb') as event_codes_file:

            timestamps_bytes = event_time_file.read()
            codes_bytes = event_codes_file.read()

            self.event_codes = np.frombuffer(codes_bytes, dtype=np.int32)
            event_timestamps = np.frombuffer(timestamps_bytes, dtype=np.int32)

            # make structured array from the timestamps
            trial_type = [('start', np.int32), ('on', np.int32), ('off', np.int32), ('end', np.int32)]
            self.trial_timestamps = event_timestamps.view(trial_type)

            '''
            Consider that all events are 128 129 150 192,
            OBS: if this is not true for future dataset need to change a bit this code
            '''
            print('[{0}.{1}] COMPLETE - Timestamps RAW {2}(first trial) - Unique Codes of Events {3}'\
                        .format(self.__class__.__name__, sys._getframe().f_code.co_name, event_timestamps[:4], np.unique(self.event_codes)))
