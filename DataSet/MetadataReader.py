'''
Module for reading the metadata of the .ssd file
'''

import sys
import os

no_units_ofs = 5
metadata_window = 3

class MetadataReader:
    '''
    parameters: - path_ssd: - path to the folder where the ssd file is located
                - file_ssd: - name of the file with extension .ssd
    '''
    def __init__(self, path_ssd, file_ssd):
        if file_ssd[-3:] != 'ssd':
            print('[{0}.{1}] {2} not a .ssd file'.format(self.__class__.__name__, sys._getframe().f_code.co_name, path_ssd))
            self.valid = False
        else:
            self.dataset_path = path_ssd
            self.ssd_file = file_ssd
            self.read_ssd_metadata()
            self.valid = True

    '''
    Description: Reads all the information in the ssd file
    '''
    def read_ssd_metadata(self):
        lines = None
        with open(os.path.join(self.dataset_path, self.ssd_file), 'r') as f:
            lines = f.readlines()

        # read the number of units
        current_ofs = no_units_ofs
        self.no_units = int(lines[current_ofs])

        # skip the units names and origins
        current_ofs += metadata_window + self.no_units - 1 + metadata_window + self.no_units - 1

        # read the number of skipes per unit
        current_ofs += metadata_window
        self.no_spikes_per_unit = []
        for i in range(0, self.no_units):
            self.no_spikes_per_unit.append(int(lines[current_ofs + i]))

        # read the event sampling frequency
        current_ofs += metadata_window + self.no_units - 1
        self.spike_event_sampling_freq = float(lines[current_ofs])

        # read the Waveform internal sampling frequency
        current_ofs += metadata_window
        self.waveform_spike_event_sampling_freq = float(lines[current_ofs])

        # read the waveform length in samples
        current_ofs += metadata_window
        self.waveform_length_samples = int(lines[current_ofs])

        # read the waveform spike align offset
        current_ofs += metadata_window
        self.waveform_spike_align_offset = int(lines[current_ofs])

        # read the number of events
        current_ofs += metadata_window
        self.no_events = int(lines[current_ofs])

        # read name File holding spike timestamps
        current_ofs += metadata_window
        self.file_spike_timestamps = lines[current_ofs][:-1]

        # read name File holding spike waveforms
        current_ofs += metadata_window
        self.file_spike_waveform = lines[current_ofs][:-1]

        # read name File holding unit statistics
        current_ofs += metadata_window
        self.file_unit_statistics = lines[current_ofs][:-1]

        # read name file holding event timestamps
        current_ofs += metadata_window
        self.file_event_timestamps = lines[current_ofs][:-1]

        # read name file holding codes of events corresponding to the event timestamps file
        current_ofs += metadata_window
        self.file_events_codes = lines[current_ofs][:-1]

        print('[{0}.{1}] COMPLETE'.format(self.__class__.__name__, sys._getframe().f_code.co_name))


    def print_metadata(self):
        print('Number of units ', self.no_units)
        print('Spikes per unit ', self.no_spikes_per_unit)
        print('Spikes sampling frequency ', self.spike_event_sampling_freq)
        print('Waveform sampling frequency ', self.waveform_spike_event_sampling_freq)
        print('Waveform length in samples ', self.waveform_length_samples)
        print('Waveform spike align offset ', self.waveform_spike_align_offset)
        print('Number of events ', self.no_events)
        print('File holding spike timestamps for the units, ', self.file_spike_timestamps)
        print('File holding spike waveforms for the units ', self.file_spike_waveform)
        print('File holding unit statistics ', self.file_unit_statistics)
        print('File holding event timestamps ', self.file_event_timestamps)
        print('File holding codes of events corresponding to the event timestamps file ', self.file_events_codes)
