import os
import sys

mili_between_START_ON = 1002
mili_between_ON_OFF = 2672
mili_between_OFF_END = 501
mili_between_START_OFF = mili_between_START_ON + mili_between_ON_OFF
mili_between_START_END = mili_between_START_OFF + mili_between_OFF_END
mili_between_ON_END = mili_between_ON_OFF + mili_between_OFF_END

data_dir = os.path.join('Data', 'M017_0002_sorted_full')
ssd_file = 'M017_S001_SRCS3L_25,50,100_0002.ssd'
result_file = 'Results M017_S001_SRCS3L_25,50,100_0002 Variable contrast, all orientations.csv'
