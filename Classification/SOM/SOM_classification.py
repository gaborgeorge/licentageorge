import sys
import os
sys.path.append(os.path.join('..', '..'))

import numpy as np

from sklearn.model_selection import train_test_split

from Classification.SOM.SOM_dataset_builder import build_color_dataset_time
from Classification.SOM.SOM_dataset_builder import build_color_dataset

from Classification.SOM.SOM_dataset_builder import build_histogram_datasets_amp_vs_mean
from Classification.SOM.SOM_dataset_builder import build_split_histogram_datasets_amp_vs_mean

from Classification.SOM.SOM_dataset_builder import build_full_trial_datasets_amp_vs_mean_color
from Classification.SOM.SOM_dataset_builder import build_full_trial_datasets_amp_vs_mean_centro

from Classification.SOM.SOM_dataset_builder import build_color_dataset_merged_time
from Classification.SOM.SOM_dataset_builder import build_color_dataset_merged_with_split_time

from Classification.SOM.SOM_dataset_builder import color_array_to_hist


from Utils.info_gain import discretization
from Utils.info_gain import information_gains
from Utils.info_gain import sort_mirror
from Classification.Classifiers.DecisionTree.decision_tree import decision_tree_classification
from Classification.Classifiers.CNN.cnn_1d import cnn_1d_classification

from Classification.classification_utils import my_split

from Visualization.SOMInfoGain.SOM_gain_visual import make_colors
from Visualization.SOMInfoGain.SOM_gain_visual_only_best_colors import leave_only_important_colors

show_histograms = True

def classification(size, tau, constrast, use_mean_neuron=False, use_z_score=False, use_fisher=False, discre=False, classifier=decision_tree_classification):

    # build the dataset
    X, Y, _, _, desc = build_color_dataset(size, tau, constrast,
                                            use_mean_neuron=use_mean_neuron,
                                            use_z_score=use_z_score,
                                            use_fisher=use_fisher)
    # TODO: we never used discretization
    if discre is True:
        X = discretization(X, Y)

    X_train, X_test, Y_train, Y_test = my_split(X, Y)

    score, confusion = classifier(X_train, X_test, Y_train, Y_test)

    return score, confusion, desc

def classification_with_time(size, tau, constrast, use_mean_neuron=False, use_z_score=False, use_fisher=False, classification_iterations=10, classifier=decision_tree_classification):

    # build the dataset
    X, Y, _, _, desc = build_color_dataset_time(size, tau, constrast,
                                            use_mean_neuron=use_mean_neuron,
                                            use_z_score=use_z_score,
                                            use_fisher=use_fisher)

    scores = []
    confusions = []
    for i in range(classification_iterations):
        X_train, X_test, Y_train, Y_test = my_split(X, Y, train_percentage=0.8)

        score, confusion = classifier(X_train, X_test, Y_train, Y_test)

        scores.append(score)
        confusions.append(confusion)

    return scores, confusions, desc


def classify_merged_data(X1, X2, Y1, Y2, classifier=decision_tree_classification):
    # split data
    permutation = np.random.permutation(X1.shape[0] // 8)
    X1_train, X1_test, Y1_train, Y1_test = my_split(X1, Y1, permutation=permutation)
    X2_train, X2_test, Y2_train, Y2_test = my_split(X2, Y2, permutation=permutation)

    # build amplitude model
    scores_amp = []
    confusions_amp = []
    for i in range(classification_iterations):
        score_amp, confusion_amp = classifier(X1_train, X1_test, Y1_train, Y1_test)

        scores_amp.append(score_amp)
        confusions_amp.append(confusion_amp)

    # buuild mean model
    scores_mean=[]
    confusions_mean = []
    for i in range(classification_iterations):
        score_mean, confusion_mean = classifier(X2_train, X2_test, Y2_train, Y2_test)

        scores_mean.append(score_mean)
        confusions_mean.append(confusion_mean)

    return scores_amp, confusions_amp, scores_mean, confusions_mean, desc

'''
The only difference is that this uses the sklearn split function
# NOTE: my_split does the split for every orientation, not for the entire dataset at once
'''
def classification_sk_split(size, tau, constrast, use_mean_neuron=True, use_z_score=False, use_fisher=False, classifier=decision_tree_classification):
    X1, X2, Y1, Y2, _, _, desc = build_histogram_datasets(size, tau, constrast,
                                                            use_mean_neuron=use_mean_neuron,
                                                            use_z_score=use_z_score,
                                                            use_fisher=use_fisher)

    X = np.vstack((X1, X2))
    Y = np.hstack((Y1, Y2))

    X = discretization(X, Y)

    X_train, X_test, Y_train, Y_test = train_test_split(X, Y, test_size=0.2, random_state=42)

    score, _ = classifier(X_train, X_test, Y_train, Y_test)

    return score, desc

'''
Parameters: X - feature matrix
            Y - label array
            percent - minumum percentege of info_gain to keep a feature
'''
def select_features(X, Y, color_cnt, percent):
    info_gain = information_gains(X, Y)
    feature_indexes = np.arange(color_cnt)

    sort_mirror(info_gain, feature_indexes)

    important_features_idx = []

    max_gain = info_gain.max()

    i = color_cnt - 1
    while i >= 0:
        if info_gain[i] > max_gain * percent:
            important_features_idx.append(feature_indexes[i])
        i -= 1

    return X[:, important_features_idx]

def clasification_histogram_amp_vs_mean(size, tau, constrast, use_mean_neuron=True, use_z_score=False,
                        use_fisher=False, discre=False, classification_iterations=1, important_features=False, percent=0.8, classifier=decision_tree_classification,
                        use_start=False, use_end=False):
    # get dataset
    X1, X2, Y1, Y2, _, _, desc = build_histogram_datasets_amp_vs_mean(size, tau, constrast,
                                                            use_mean_neuron=use_mean_neuron,
                                                            use_z_score=use_z_score,
                                                            use_fisher=use_fisher, use_start=use_start,
                                                            use_end=use_end)

    if discre is True:
        X1 = discretization(X1, Y1)
        X2 = discretization(X2, Y2)
        dis = 'Discre'
    else:
        dis = ''

    if important_features is True:
        desc += 'Important_' + str(percent) + '_'
        X1 = select_features(X1, Y1, size[0] * size[1] * size[2], percent)
        X2 = select_features(X2, Y2, size[0] * size[1] * size[2], percent)

    # split data
    permutation = np.random.permutation(X1.shape[0] // 8)
    X1_train, X1_test, Y1_train, Y1_test = my_split(X1, Y1, permutation=permutation)
    X2_train, X2_test, Y2_train, Y2_test = my_split(X2, Y2, permutation=permutation)

    # build amplitude model
    scores_amp = []
    confusions_amp = []
    for i in range(classification_iterations):
        score_amp, confusion_amp = classifier(X1_train, X1_test, Y1_train, Y1_test)

        scores_amp.append(score_amp)
        confusions_amp.append(confusion_amp)

    # buuild mean model
    scores_mean=[]
    confusions_mean = []
    for i in range(classification_iterations):
        score_mean, confusion_mean = classifier(X2_train, X2_test, Y2_train, Y2_test)

        scores_mean.append(score_mean)
        confusions_mean.append(confusion_mean)

    return scores_amp, confusions_amp, scores_mean, confusions_mean, desc


'''
This should return 8 datasets, or something like that 4 for amplitude and 4 for mean
    Datasets1: trial values between START and ON
               trial values between On and OFF
               trial values between OFF and END

    # NOTE: I believe we can use decision_tree_classification_for_merged and then just split the return matrixes after their columns
                but the return matrixes have the columns the colors, not the values in time
                so I think we need to write the whole code
'''
def clasification_split_histogram_amp_vs_mean(size, tau, contrast, use_mean_neuron=True, use_z_score=False, use_fisher=False, discre=False,
                            classification_iterations=1, important_features=False, percent=0.8, classifier=decision_tree_classification):
    X1s, X2s, Y1, Y2, _, _, desc = build_split_histogram_datasets_amp_vs_mean(size, tau, contrast,
                                                                        use_mean_neuron=use_mean_neuron,
                                                                        use_z_score=use_z_score,
                                                                        path=os.path.join('..', '..'))

    if discre is True:
        for i in range(len(X1s)):
            X1s[i] = discretization(X1s[i], Y1)
        for i in renge(len(X2s)):
            X2s[i] = discretization(X2s[i], Y2)

    if important_features is True:
        desc += 'Important'
        X1s = [select_features(X1s[k], Y1, size[0] * size[1] * size[2], percent) for k in range(len(X1s))]
        X2s = [select_features(X2s[k], Y2, size[0] * size[1] * size[2], percent) for k in range(len(X2s))]

    X1s_train = []
    X2s_train = []
    X1s_test = []
    X2s_test = []

    permutation = np.random.permutation(X1s[0].shape[0] // 8)
    for i in range(len(X1s)):
        X1_train, X1_test, Y1_train, Y1_test = my_split(X1s[i], Y1, permutation=permutation)
        X2_train, X2_test, Y2_train, Y2_test = my_split(X2s[i], Y2, permutation=permutation)
        X1s_train.append(X1_train)
        X2s_train.append(X2_train)
        X1s_test.append(X1_test)
        X2s_test.append(X2_test)

    # # NOTE: we currently know their are 4 intervals
    scores_amp = [[], [], [], []]
    confusions_amp = [[], [], [], []]
    for _ in range(classification_iterations):

        for i in range(len(X1s_train)):
            score_amp, confusion_amp = classifier(X1s_train[i], X1s_test[i], Y1_train, Y1_test)

            scores_amp[i].append(score_amp)
            confusions_amp[i].append(confusion_amp)

    scores_mean = [[], [], [], []]
    confusions_mean = [[], [], [], []]
    for _ in range(classification_iterations):

        for i in range(len(X2s_train)):
            score_mean, confusion_mean = classifier(X2s_train[i], X2s_test[i], Y2_train, Y2_test)

            scores_mean[i].append(score_mean)
            confusions_mean[i].append(confusion_mean)

    return scores_amp, confusions_amp, scores_mean, confusions_mean, desc


def classification_for_merged_time(size, tau, constrast, use_mean_neuron=True, use_z_score=False,
                        use_fisher=False, classification_iterations=1, classifier=cnn_1d_classification,
                        use_important=False, percent=0.8, use_start=False, use_end=True):
    # get dataset
    X1, X2, Y1, Y2, _, _, desc = build_color_dataset_merged_time(size, tau, constrast,
                                                            use_mean_neuron=use_mean_neuron,
                                                            use_z_score=use_z_score,
                                                            use_fisher=use_fisher,
                                                            use_start=use_start,
                                                            use_end=use_end)
    print(X1.shape, X2.shape, Y1.shape, Y2.shape)
    # important colors
    if use_important is True:
        X1_h = np.zeros((X1.shape[0], size[0] * size[1] * size[2]), dtype=np.uint32)
        X2_h = np.zeros((X1.shape[0], size[0] * size[1] * size[2]), dtype=np.uint32)

        for i in range(X1.shape[0]):
            color_array_to_hist(X1_h[i], X1[i], 255 // (size[0] - 1))

        for i in range(X2.shape[0]):
            color_array_to_hist(X2_h[i], X2[i], 255 // (size[0] - 1))

        info_gains = information_gains(X1_h, Y1)
        leave_only_important_colors(X1, 255 // (size[0] - 1), info_gains, size[0] * size[1] * size[2], percent)

        info_gains = information_gains(X2_h, Y2)
        leave_only_important_colors(X2, 255 // (size[0] - 1), info_gains, size[0] * size[1] * size[2], percent)

        desc += 'Impor_col_' + str(percent)


    # split data
    permutation = np.random.permutation(X1.shape[0] // 8)
    X1_train, X1_test, Y1_train, Y1_test = my_split(X1, Y1, permutation=permutation)
    X2_train, X2_test, Y2_train, Y2_test = my_split(X2, Y2, permutation=permutation)

    # build amplitude model
    scores_amp = []
    confusions_amp = []
    for i in range(classification_iterations):
        score_amp, confusion_amp = classifier(X1_train, X1_test, Y1_train, Y1_test)

        scores_amp.append(score_amp)
        confusions_amp.append(confusion_amp)

    # buuild mean model
    scores_mean=[]
    confusions_mean = []
    for i in range(classification_iterations):
        score_mean, confusion_mean = classifier(X2_train, X2_test, Y2_train, Y2_test)

        scores_mean.append(score_mean)
        confusions_mean.append(confusion_mean)

    return scores_amp, confusions_amp, scores_mean, confusions_mean, desc

'''
This should return 8 datasets, or something like that 4 for amplitude and 4 for mean
    Datasets1: trial values between START and ON
               trial values between On and OFF
               trial values between OFF and END

    # NOTE: I believe we can use decision_tree_classification_for_merged and then just split the return matrixes after their columns
                but the return matrixes have the columns the colors, not the values in time
                so I think we need to write the whole code
'''
def classification_for_merged_with_trial_split_time(size, tau, contrast, use_mean_neuron=True, use_z_score=False, use_fisher=False, discre=False,
                            classification_iterations=1, important_features=False, percent=0.8, classifier=decision_tree_classification):
    X1s, X2s, Y1, Y2, _, _, desc = build_color_dataset_merged_with_split_time(size, tau, contrast,
                                                                        use_mean_neuron=use_mean_neuron,
                                                                        use_z_score=use_z_score,
                                                                        path=os.path.join('..', '..'))

    if discre is True:
        for i in range(len(X1s)):
            X1s[i] = discretization(X1s[i], Y1)
        for i in renge(len(X2s)):
            X2s[i] = discretization(X2s[i], Y2)

    if important_features is True:
        desc += 'Important'
        X1s = [select_features(X1s[k], Y1, size[0] * size[1] * size[2], percent) for k in range(len(X1s))]
        X2s = [select_features(X2s[k], Y2, size[0] * size[1] * size[2], percent) for k in range(len(X2s))]

    X1s_train = []
    X2s_train = []
    X1s_test = []
    X2s_test = []

    permutation = np.random.permutation(X1s[0].shape[0] // 8)
    for i in range(len(X1s)):
        X1_train, X1_test, Y1_train, Y1_test = my_split(X1s[i], Y1, permutation=permutation)
        X2_train, X2_test, Y2_train, Y2_test = my_split(X2s[i], Y2, permutation=permutation)
        X1s_train.append(X1_train)
        X2s_train.append(X2_train)
        X1s_test.append(X1_test)
        X2s_test.append(X2_test)

    # # NOTE: we currently know their are 4 intervals
    scores_amp = [[], [], [], []]
    confusions_amp = [[], [], [], []]
    for _ in range(classification_iterations):

        for i in range(len(X1s_train)):
            score_amp, confusion_amp = classifier(X1s_train[i], X1s_test[i], Y1_train, Y1_test)

            scores_amp[i].append(score_amp)
            confusions_amp[i].append(confusion_amp)

    scores_mean = [[], [], [], []]
    confusions_mean = [[], [], [], []]
    for _ in range(classification_iterations):

        for i in range(len(X2s_train)):
            score_mean, confusion_mean = classifier(X2s_train[i], X2s_test[i], Y2_train, Y2_test)

            scores_mean[i].append(score_mean)
            confusions_mean[i].append(confusion_mean)

    return scores_amp, confusions_amp, scores_mean, confusions_mean, desc


def clasification_full_trial_amp_vs_mean(
        size,
        tau,
        contrast,
        orientations,
        use_mean_neuron=True,
        use_z_score=False,
        use_fisher=False,
        discre=False,
        classification_iterations=1,
        important_features=False,
        important_percent=0.8,
        classifier=cnn_1d_classification,
        training_percent=0.8,
        use_start=False,
        use_end=False,
        use_color=True):
    # get dataset
    if use_color is True:
        X1, X2, Y1, Y2, _, _, desc = build_full_trial_datasets_amp_vs_mean_color(size, tau,
                                                            contrast=contrast,
                                                            orientations=orientations,
                                                            use_mean_neuron=use_mean_neuron,
                                                            use_z_score=use_z_score,
                                                            use_fisher=use_fisher,
                                                            use_start=use_start,
                                                            use_end=use_end)
        #X1 = X1 / 255.0
        #X2 = X2 / 255.0
        print(X1[0])
    else:
        X1, X2, Y1, Y2, _, desc = build_full_trial_datasets_amp_vs_mean_centro(size, tau,
                                                            contrast=contrast,
                                                            orientations=orientations,
                                                            use_mean_neuron=use_mean_neuron,
                                                            use_z_score=use_z_score,
                                                            use_fisher=use_fisher,
                                                            use_start=use_start,
                                                            use_end=use_end)

        #print(np.amax(X1))
        if use_z_score is False:
            pass
        #    X1 = X1 / np.amax(X1)
        #        X2 = X2 / np.amax(X2)

    if discre is True:
        X1 = discretization(X1, Y1)
        X2 = discretization(X2, Y2)
        dis = 'Discre'
    else:
        dis = ''

    if important_features is True:
        X1_h = np.zeros((X1.shape[0], size[0] * size[1] * size[2]), dtype=np.uint32)
        X2_h = np.zeros((X1.shape[0], size[0] * size[1] * size[2]), dtype=np.uint32)

        for i in range(X1.shape[0]):
            color_array_to_hist(X1_h[i], X1[i], 255 // (size[0] - 1))

        for i in range(X2.shape[0]):
            color_array_to_hist(X2_h[i], X2[i], 255 // (size[0] - 1))

        info_gains = information_gains(X1_h, Y1)
        leave_only_important_colors(X1, 255 // (size[0] - 1), info_gains, size[0] * size[1] * size[2], important_percent)

        info_gains = information_gains(X2_h, Y2)
        leave_only_important_colors(X2, 255 // (size[0] - 1), info_gains, size[0] * size[1] * size[2], important_percent)

        desc += 'Impor_col_' + str(important_percent)


    # split data
    permutation = np.random.permutation(X1.shape[0] // len(np.unique(Y1)))
    X1_train, X1_test, Y1_train, Y1_test = my_split(X1, Y1, permutation=permutation, train_percentage=training_percent)
    X2_train, X2_test, Y2_train, Y2_test = my_split(X2, Y2, permutation=permutation, train_percentage=training_percent)

    # build amplitude model
    scores_amp = []
    confusions_amp = []
    for i in range(classification_iterations):
        score_amp, confusion_amp = classifier(X1_train, X1_test, Y1_train, Y1_test)

        scores_amp.append(score_amp)
        confusions_amp.append(confusion_amp)

    # buuild mean model
    scores_mean=[]
    confusions_mean = []
    for i in range(classification_iterations):
        score_mean, confusion_mean = classifier(X2_train, X2_test, Y2_train, Y2_test)

        scores_mean.append(score_mean)
        confusions_mean.append(confusion_mean)

    return scores_amp, confusions_amp, scores_mean, confusions_mean, desc
