import sys
import os
sys.path.append(os.path.join('..', '..'))

import numpy as np

# to following two are not used
from Classification.SOM.SOM_classification import classification
from Classification.SOM.SOM_classification import classification_with_time

from Classification.SOM.SOM_classification import clasification_histogram_amp_vs_mean
from Classification.SOM.SOM_classification import clasification_split_histogram_amp_vs_mean

from Classification.SOM.SOM_classification import clasification_full_trial_amp_vs_mean


from Classification.SOM.SOM_classification import classification_for_merged_time
from Classification.SOM.SOM_classification import classification_for_merged_with_trial_split_time
from Classification.Classifiers.DecisionTree.decision_tree import decision_tree_classification
from Classification.Classifiers.SVM.support_vector_machines import svm_classification
from Classification.Classifiers.CNN.cnn_1d import cnn_1d_classification

from Visualization.Plots_matplot import plot_connected_lines

# always som functions
def save_result(ss_amp, cs_amp, ss_mean, cs_mean, desc, s_iterations, c_iterations, folder='Results'):
    desc +=  '_SOM_Iterations_' + str(s_iterations) + '_Classifications_Iterations_' + str(c_iterations)

    file_path = os.path.join(folder, desc + '.txt')
    amp_means_SOM=[]
    mean_means_SOM=[]
    amp_cm_SOM = np.zeros(cs_amp[0][0].shape)
    mean_cm_SOM = np.zeros(cs_mean[0][0].shape)

    with open(file_path, 'w') as f:
        for i in range(len(ss_amp)):
            f.write('For %s iteration of SOM\n' % (i))
            # we need to iterate
            amp_score_values = []
            mean_score_values = []
            amp_cm = np.zeros(cs_amp[0][0].shape)
            mean_cm = np.zeros(cs_mean[0][0].shape)
            for j in range(len(ss_amp[i])):
                amp_score_values.append(ss_amp[i][j])
                mean_score_values.append(ss_mean[i][j])

                amp_cm += cs_amp[i][j]
                mean_cm += cs_mean[i][j]

            amp_mean = np.mean(np.array(amp_score_values))
            amp_std = np.std(np.array(amp_score_values))
            mean_mean = np.mean(np.array(mean_score_values))
            mean_std = np.std(np.array(mean_score_values))

            amp_means_SOM.append(amp_mean)
            mean_means_SOM.append(mean_mean)

            f.write('Amplitude mean %s, Amplitude std %s\n' % (amp_mean, amp_std))
            f.write('Mean mean %s, Mean std %s\n' % (mean_mean, mean_std))

            amp_cm /= len(cs_amp[i])
            mean_cm /= len(cs_mean[i])

            f.write('Amplitude Confusion Matrix\n%s\n' % amp_cm)
            f.write('Mean Confusion Matrix\n%s\n' % mean_cm)

            amp_cm_SOM += amp_cm
            mean_cm_SOM += mean_cm

        ss_a = np.array(amp_means_SOM)
        f.write('Mean of SOMS\n')
        f.write('Amplitude mean score %s, Amplitude score std %s\n' % (np.mean(ss_a), np.std(ss_a)))

        ss_m = np.array(mean_means_SOM)
        f.write('Mean mean score %s, Mean score std %s\n' % (np.mean(ss_m), np.std(ss_m)))

        amp_cm_SOM /= len(cs_amp)
        mean_cm_SOM /= len(cs_mean)

        f.write('Confusion Amplitude mean\n%s\n' % (amp_cm_SOM))
        f.write('Confusion Mean mean\n%s\n' % (mean_cm_SOM))

    return file_path, np.mean(ss_a), np.mean(ss_m)

'''
first 4 parameters structure:
    ss_amp = list of s_iterations values
    ss_amp[i] = list of intervals of s_iteration i (usually four)
    ss_amp[i][j] = list of c_iterations values for the interval j of s_iteration i

'''
def save_results_split_data(ss_amp, cs_amp, ss_mean, cs_mean, desc, s_iterations, c_iterations, folder='Results'):
    desc += '_SOM_Ite_' + str(s_iterations) + '_Classifications_Ite_' + str(c_iterations) + '_D_Split_'

    file_path = os.path.join(folder, desc + '.txt')

    amp_means_SOM = [[] for x in range(len(ss_amp[0]))]
    mean_means_SOM = [[] for x in range(len(ss_mean[0]))]
    amp_cm_SOM = [np.zeros(cs_amp[0][0][0].shape) for x in range(len(cs_amp[0]))]
    mean_cm_SOM = [np.zeros(cs_mean[0][0][0].shape) for x in range(len(cs_mean[0]))]

    with open(file_path, 'w') as f:
        # for every iteration of the SOM
        for i in range(len(ss_amp)):
            f.write('For %s iteration of SOM\n' % (i))

            amp_score_values = [[] for x in range(len(ss_amp[0]))]
            mean_score_values = [[] for x in range(len(ss_mean[0]))]
            amp_cm = [np.zeros(cs_amp[0][0][0].shape) for x in range(len(cs_amp[0]))]
            mean_cm = [np.zeros(cs_mean[0][0][0].shape) for x in range(len(cs_mean[0]))]

            # for every interval
            for k in range(len(ss_amp[i])):

                # for every classification
                for j in range(len(ss_amp[i][k])):

                    amp_score_values[k].append(ss_amp[i][k][j])
                    mean_score_values[k].append(ss_mean[i][k][j])
                    amp_cm[k] += cs_amp[i][k][j]
                    mean_cm[k] += cs_mean[i][k][j]

            amp_int_means = [np.mean(np.array(amp_score_values[k])) for k in range(len(cs_amp[0]))]
            amp_int_stds = [np.std(np.array(amp_score_values[k])) for k in range(len(cs_amp[0]))]
            mean_int_means = [np.mean(np.array(mean_score_values[k])) for k in range(len(cs_mean[0]))]
            mean_int_stds = [np.std(np.array(mean_score_values[k])) for k in range(len(cs_mean[0]))]

            # for every interval write the score
            for k in range(len(ss_amp[0])):

                # save mean for interval k to make the mean over the SOMs
                amp_means_SOM[k].append(amp_int_means[k])
                mean_means_SOM[k].append(mean_int_means[k])

                f.write('Interval %s\n' % k)
                f.write('Amplitude mean %s, Amplitude std %s\n' % (amp_int_means[k], amp_int_stds[k]))
                f.write('Mean mean %s, Mean std %s\n' % (mean_int_means[k], mean_int_stds[k]))

            # for every interval write the confusion matrix
            for k in range(len(ss_amp[0])):
                amp_cm[k] /= len(cs_amp[i][0])
                mean_cm[k] /= len(cs_amp[i][0])

                f.write('Interval %s\n' % k)
                f.write('Amplitude Confusion Matrix\n%s\n' % amp_cm[k])
                f.write('Mean Confusion Matrix\n%s\n' % mean_cm[k])

                # add to the confusion matrix of all SOMs
                amp_cm_SOM[k] += amp_cm[k]
                mean_cm_SOM[k] += mean_cm[k]

        f.write('Mean of SOMS\n')
        ss_as = [np.array(amp_means_SOM[k]) for k in range(len(ss_amp[0]))]
        ss_ms = [np.array(mean_means_SOM[k]) for k in range(len(ss_mean[0]))]
        amp_cm_SOM = [amp_cm_SOM[k] / len(cs_amp) for k in range(len(cs_amp[0]))]
        mean_cm_SOM = [mean_cm_SOM[k] / len(cs_mean) for k in range(len(cs_mean[0]))]

        # for every interval
        for k in range(len(ss_amp[0])):
            f.write('Interval %s\n' % k)
            f.write('Amplitude mean score %s, Amplitude score std %s\n' % (np.mean(ss_as[k]), np.std(ss_as[k])))
            f.write('Mean mean score %s, Mean score std %s\n' % (np.mean(ss_ms[k]), np.std(ss_ms[k])))
        for k in range(len(ss_amp[0])):
            f.write('Interval %s\n' % k)
            f.write('Amplitude Confusion Matrix\n%s\n' % (amp_cm_SOM[k]))
            f.write('Mean Confusion Matrix\n%s\n' % (mean_cm_SOM[k]))

    return file_path

'''
Params: size            - som cube size
        tau             - integration constant
        contrast        - constrast to consider
        use_mean_neuron - if to use the mean neuron of channels for second dataset
        use_z_score     - apply Z-score on datasets
        som_iterations  - how many som mappings to build
        classification_iterations
                        - how many classifiers to train for a map
        classifier      - what classfier to use
        important_features - if it should apply info gain to reduce colors
        percent         - minimum amount of info gain to consider
        train_percent   - amount of training data
        use_start       - if it should consider the start event
        use_end         - if it should consider the end event
        folder          - where to save the results
'''
def clasification_histogram(
                size,
                tau,
                contrast,
                use_mean_neuron=False,
                use_z_score=False,
                use_fisher=False,
                som_iterations=50,
                classification_iterations=100,
                use_splits=False,
                important_features=False,
                percent=0.8,
                classifier=decision_tree_classification,
                use_start=False,
                use_end=False,
                folder='Results'):
    ss_amp = []
    cs_amp = []
    ss_mean = []
    cs_mean = []

    for i in range(som_iterations):
        if use_splits is False:
            s_amp, c_amp, s_mean, c_mean, desc = clasification_histogram_amp_vs_mean(
                                                size, tau, contrast,
                                                use_mean_neuron=use_mean_neuron,
                                                use_z_score=use_z_score,
                                                use_fisher=use_fisher,
                                                classification_iterations=classification_iterations,
                                                important_features=important_features,
                                                percent=percent,
                                                classifier=classifier,
                                                #discre=True,
                                                use_start=use_start,
                                                use_end=use_end)

        else:
            s_amp, c_amp, s_mean, c_mean, desc = clasification_split_histogram_amp_vs_mean(
                                                size, tau, contrast,
                                                use_mean_neuron=use_mean_neuron,
                                                use_z_score=use_z_score,
                                                use_fisher=use_fisher,
                                                classification_iterations=classification_iterations,
                                                important_features=important_features,
                                                percent=percent,
                                                classifier=classifier)
        ss_amp.append(s_amp)
        cs_amp.append(c_amp)
        ss_mean.append(s_mean)
        cs_mean.append(c_mean)

    if use_splits is False:
        file_path = save_result(ss_amp, cs_amp, ss_mean, cs_mean, desc, som_iterations, classification_iterations, folder=folder)
    else:
        file_path = save_results_split_data(ss_amp, cs_amp, ss_mean, cs_mean, desc, som_iterations, classification_iterations, folder=folder)


    print('[{0}] Saved in {1}'.format(sys._getframe().f_code.co_name, file_path))

# currently not used
def classification_time(size, tau, contrast, use_mean_neuron=False, use_z_score=False, use_fisher=False,
                                som_iterations=2, classification_iterations=10, d='', classifier=cnn_1d_classification,
                                use_important=False, percent=0.8, use_start=False, use_end=True, folder='Results'):
    ss_amp = []
    cs_amp = []
    ss_mean = []
    cs_mean = []

    for i in range(som_iterations):
        s_amp, c_amp, s_mean, c_mean, desc = classification_for_merged_time(
                                                size, tau, contrast,
                                                use_mean_neuron=use_mean_neuron,
                                                use_z_score=use_z_score,
                                                use_fisher=use_fisher,
                                                classification_iterations=classification_iterations,
                                                classifier=classifier,
                                                use_important=use_important,
                                                percent=percent,
                                                use_start=use_start,
                                                use_end=use_end)
        ss_amp.append(s_amp)
        cs_amp.append(c_amp)
        ss_mean.append(s_mean)
        cs_mean.append(c_mean)

    file_path, aq_amplitude, aq_mean = save_result(ss_amp, cs_amp, ss_mean, cs_mean, desc, som_iterations, classification_iterations, d, folder=folder)

    print('[{0}] Saved in {1}'.format(sys._getframe().f_code.co_name, file_path))

    return aq_amplitude, aq_mean


'''
Params: size            - som cube size
        tau             - integration constant
        orientations    - list o orientations to consider
        contrast        - list of constrast to consider
        use_mean_neuron - if to use the mean neuron of channels for second dataset
        use_z_score     - apply Z-score on datasets
        som_iterations  - how many som mappings to build
        classification_iterations
                        - how many classifiers to train for a map
        classifier      - what classfier to use
        train_percent   - amount of training data
        use_start       - if it should consider the start event
        use_end         - if it should consider the end event
        folder          - where to save the results
'''
def classification_full_trial(
                    size,
                    tau,
                    orientations=[0, 45, 90, 135, 180, 225, 270, 315],
                    contrast=[25, 50, 100],
                    use_mean_neuron=False,
                    use_z_score=False,
                    use_fisher=False,
                    som_iterations=1,
                    classification_iterations=1,
                    classifier=cnn_1d_classification,
                    training_percent=0.8,
                    use_important=False,
                    important_percent=0.8,
                    use_start=False,
                    use_end=False,
                    use_color=False,
                    folder='Results'):
    ss_amp = []
    cs_amp = []
    ss_mean = []
    cs_mean = []

    for i in range(som_iterations):
        s_amp, c_amp, s_mean, c_mean, desc = clasification_full_trial_amp_vs_mean(
                                                size, tau, contrast=contrast,
                                                orientations=orientations,
                                                use_mean_neuron=use_mean_neuron,
                                                use_z_score=use_z_score,
                                                use_fisher=use_fisher,
                                                classification_iterations=classification_iterations,
                                                classifier=classifier,
                                                use_start=use_start,
                                                use_end=use_end,
                                                important_features=use_important,
                                                important_percent=important_percent,
                                                training_percent=training_percent,
                                                use_color=use_color)
        ss_amp.append(s_amp)
        cs_amp.append(c_amp)
        ss_mean.append(s_mean)
        cs_mean.append(c_mean)


    desc = 'CNN_S_' + str(size) + '_T_' + str(tau) + '_C_' + str(contrast) + '_O_' + str(orientations) + '_T_SP_'

    if use_start is False:
        desc += '_ON_'

    if use_end is False:
        desc += '_OFF_'

    if use_z_score is True:
        desc += '_Z_SCORE_'

    file_path, aq_amplitude, aq_mean = save_result(ss_amp, cs_amp, ss_mean, cs_mean, desc, som_iterations, classification_iterations, folder)

    print('[{0}] Saved in {1}'.format(sys._getframe().f_code.co_name, file_path))

    return aq_amplitude, aq_mean

# asta nu mai
def clasification_split_data_time(size, tau, contrast, use_mean_neuron=False, use_z_score=False, use_fisher=False, som_iterations=50,
                                classification_iterations=100, important_features=False, percent=0.8, d='', classifier=cnn_1d_classification):
    ss_amp = []
    cs_amp = []
    ss_mean = []
    cs_mean = []

    for i in range(som_iterations):
        s_amp, c_amp, s_mean, c_mean, desc = classification_for_merged_with_trial_split_time(
                                                size, tau, contrast,
                                                use_mean_neuron=use_mean_neuron,
                                                use_z_score=use_z_score,
                                                use_fisher=use_fisher,
                                                classification_iterations=classification_iterations,
                                                important_features=important_features,
                                                percent=percent,
                                                classifier=classifier)
        ss_amp.append(s_amp)
        cs_amp.append(c_amp)
        ss_mean.append(s_mean)
        cs_mean.append(c_mean)

    file_path = save_results_split_data(ss_amp, cs_amp, ss_mean, cs_mean, desc, som_iterations, classification_iterations, d)

    print('[{0}] Saved in {1}'.format(sys._getframe().f_code.co_name, file_path))

def graph_variable_tau(size, taus, contrast):
    aqs_amplitude = []
    aqs_mean = []

    for tau in taus:
        aq_amplitude, aq_mean = classification_time(size, tau, contrast, use_mean_neuron=True, use_z_score=False, som_iterations=5, classification_iterations=5, d='CNN_5', use_start=False, use_end=False)
        aqs_amplitude.append(aq_amplitude)
        aqs_mean.append(aq_mean)

    plot_connected_lines(taus, aqs_amplitude, aqs_mean,
                        x_label='Tau', y_label='Accuracy',
                        plot_1_label='Amplitude', plot_2_label='Mean',
                        save=True, save_file=os.path.join('Results', 'Variable_tau', 'size_' + str(size) + '_Variable_Tau_contrast_' + str(contrast) + '.png'))


def grapth_variable_size(sizes, tau, contrast):
    aqs_amplitude = []
    aqs_mean = []

    for size in sizes:
        aq_amplitude, aq_mean = classification_time(size, tau, contrast, use_mean_neuron=True, use_z_score=False, som_iterations=5, classification_iterations=5, use_start=False, use_end=False)
        aqs_amplitude.append(aq_amplitude)
        aqs_mean.append(aq_mean)

    plot_connected_lines(sizes, aqs_amplitude, aqs_mean,
                        x_label='Size', y_label='Accuracy',
                        plot_1_label='Amplitude', plot_2_label='Mean',
                        save=True, save_file=os.path.join('Results', 'Variable_size', 'Variable_Size_Tau_' + str(tau) + '_Contrast_' + str(contrast) + '.png'))

def grapth_variable_contrast(size, tau, contrasts):
    aqs_amplitude = []
    aqs_mean = []

    for contrast in contrasts:
        aq_amplitude, aq_mean = classification_time(size, tau, contrast, use_mean_neuron=True, use_z_score=False, som_iterations=5, classification_iterations=5, use_start=False, use_end=False)
        aqs_amplitude.append(aq_amplitude)
        aqs_mean.append(aq_mean)

    plot_connected_lines(contrasts, aqs_amplitude, aqs_mean,
                        x_label='Contrast', y_label='Accuracy',
                        plot_1_label='Amplitude', plot_2_label='Mean',
                        save=True, save_file=os.path.join('Results', 'Variable_contrast', 'Size_' + str(size) + '_Tau_' + str(tau) + '_Variable_Contrast.png'))


if __name__ == '__main__':
    #clasification_histogram((10, 10, 10), 20, 100, use_z_score=True, use_start=False, use_end=True, som_iterations=1, classification_iterations=100, percent=0.4, folder='DEMO_RESULTS', important_features=False, classifier=svm_classification)
    classification_full_trial((10, 10, 10), 20, contrast=[100], folder='DEMO_RESULTS', use_start=False, use_end=True, som_iterations=1, classification_iterations=1, use_mean_neuron=True, use_z_score=True, classifier=cnn_1d_classification)
