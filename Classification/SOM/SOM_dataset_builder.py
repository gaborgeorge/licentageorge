import os
import sys
sys.path.append(os.path.join('..', '..'))

import numpy as np
import time

from numba import njit

from Visualization.SOM.Alg.SOM import SOM
from Visualization.SOM.SOM_projections_utils import projections_to_plot
from Visualization.SOM.SOMdataBuilder import build_dataset
from Visualization.SOM.SOMdataBuilder import build_merged_dataset
from Visualization.Plots_cv import plot_bands_cv

from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
from DataSet.TrialExtractor import TrialExtractor
from config import ssd_file, result_file, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END, mili_between_ON_OFF, mili_between_OFF_END

# TO DO: maybe can remove this
from Classification.classification_utils import build_merged_dataset_orientations


pro_path = os.path.join('..', '..')

def build_description(size, tau, contrast, use_fisher, use_mean, use_mean_neuron, use_z_score, use_start, use_end):
    if use_fisher is True:
        desc = 'Fisher_Z_'
    else:
        desc = ''

    if use_mean is True:
        desc += 'Mean_'
    elif use_mean_neuron is True:
        desc += 'Mean_neuron_'

    if use_z_score is True:
        desc += 'Z_score_'

    if use_start is True:
        desc += 'From_Start_'
    else:
        desc += 'From_ON_'

    if use_end is True:
        desc += 'To_End_'
    else:
        desc += 'To_OFF_'

    desc = 'Size_' + str(size) + '_' + desc
    desc = 'Tau_' + str(tau) + '_' + desc

    if contrast != -1:
        desc = 'Contrast_' + str(contrast) + '_' + desc

    return desc


'''
Paramters: - trial_extractor
           - tau
           - size
           - contrast

return: projections = 3D array of SOM colors for each trial
        color_inc   = increment from one color to the other in their channels
        Y           = 1D array of labels for each trial
'''
def __SOM_projections(trial_extractor, tau, size, contrast):
    start = time.time()

    dataset = build_dataset(trial_extractor, tau=tau, contrast=contrast)
    orientations_count = 8

    # create som
    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.project(dataset)
    plot_projections = projections_to_plot(projections, (trial_extractor.spike_reader.metadata.no_units, trial_extractor.mili_duration),
                            10, size[0] - 1, orientations_count=orientations_count)

    color_inc = 255 // (size[0] - 1)

    # stiu ca din 10 in zece se schimba
    Y = np.empty((plot_projections.shape[0] * plot_projections.shape[1],), dtype=np.uint32)
    ori = -45
    inc = 45
    interval = 10

    for i in range(Y.shape[0]):
        if i % interval == 0:
            ori += inc
        Y[i] = ori

    print('[{0}] COMPLETE - time {1}, size {2}, tau {3}'.format(sys._getframe().f_code.co_name, time.time() - start, size, tau))

    return plot_projections, color_inc, Y


'''
Parameters: histogram = 1D array of size = number of unique colors posible
            colors = 2D array, second dimention represents the color
            color_inc = increment for the color channel
                            Ex (0, 0, 0) -> (0, 0, color_Inc)

# NOTE: histogram color indexes are as follows:
                (0, 0, 0), (0, 0, color_inc), ...., (0, 0, max), (0, color_inc, max), .... (0, max, max), (color_inc, max, max)

            steps = channel color / color_inc # number of steps per channel

            index = max_steps * max_steps * steps_first_channel +
                    max_steps * steps_second_channel +
                    steps_third_channel
'''
@njit
def color_array_to_hist(histogram, colors, color_inc):

    channel_steps = 255 // color_inc + 1

    channel_1_stride = channel_steps ** 2
    channel_2_stride = channel_steps

    for i in range(colors.shape[0]):
        first_channel_steps = colors[i, 0] // color_inc
        second_channel_steps = colors[i, 1] // color_inc
        third_channel_steps = colors[i, 2] // color_inc

        histogram[int(channel_1_stride * first_channel_steps + channel_2_stride * second_channel_steps + third_channel_steps)] += 1


'''
return: X = 2D matrix of features
        Y = 1D array of labels
        desc = a string representing human redable configuration of the dataset
# NOTE:
'''
def build_color_dataset(size, tau, contrast=-1, use_mean=False, use_mean_neuron=False, use_z_score=False, use_fisher=False, use_start=False, use_end=True, path=pro_path):

    # TODO: fix the directory issue for the input files
    # read the data and metadata and extract trials
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor = TrialExtractor(spike_reader, trial_meta_reader,
                                        use_mean_neuron=use_mean,
                                        use_mean_amplitude=use_mean,
                                        use_z_score=use_z_score,
                                        use_fisher=use_fisher,
                                        use_start=use_start,
                                        use_end=use_end)

    # build the string description of the dataset
    desc = build_description(size=size, tau=tau, contrast=contrast,
                                    use_fisher=use_fisher, use_mean=use_mean, use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score, use_start=True, use_end=True)

    # Apply the SOM
    plot_projections, color_inc, Y = __SOM_projections(trial_extractor, tau, size, contrast)

    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    # make the X matrix
    X = np.zeros((color_trials.shape[0], size[0] * size[1] * size[2]), dtype=np.uint32)
    for i in range(color_trials.shape[0]):
        color_array_to_hist(X[i, :], color_trials[i, :], color_inc)

    return X, Y, plot_projections, color_inc, desc

'''
return: X = 2D matrix of features
        Y = 1D array of labels
        desc = a string representing human redable configuration of the dataset
# NOTE:
    this will keep the representation in time of the trial, it will not make a hitogram
'''
def build_color_dataset_time(size, tau, contrast=-1, use_mean=False, use_mean_neuron=False, use_z_score=False, use_fisher=False, use_start=False, use_end=True, path=pro_path):

    # TODO: fix the directory issue for the input files
    # read the data and metadata and extract trials
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor = TrialExtractor(spike_reader, trial_meta_reader,
                                        use_mean_neuron=use_mean,
                                        use_mean_amplitude=use_mean,
                                        use_z_score=use_z_score,
                                        use_fisher=use_fisher,
                                        use_start=use_start,
                                        use_end=use_end)

    # build the string description of the dataset
    desc = build_description(size=size, tau=tau, contrast=contrast,
                                    use_fisher=use_fisher, use_mean=use_mean, use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score, use_start=True, use_end=True)

    # Apply the SOM
    plot_projections, _, Y = __SOM_projections(trial_extractor, tau, size, contrast)

    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    # make the X matrix
    X = np.zeros((color_trials.shape[0], color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)
    for i in range(color_trials.shape[0]):
        X[i, :] = color_trials[i, :]

    return X, Y, plot_projections, _, desc

'''
Return: X1 = feature 2D matrix for amplitude dataset
        X2 = faeture 2D matrix for mean amplitude dataset
        Y1 = label 1D array for amplitude datset
        Y2 = label 1D array for mean amplidute dataset
        plot_projections = bands for the merged plot
        color_inc = how much a channel of the color changes for one color to the next

# NOTE: Y1 == Y2
'''
def __SOM_dataset_merged(extractor1, extractor2, size, tau, contrast):
    start = time.time()

    dataset = build_merged_dataset(extractor1, extractor2, tau=tau, contrast=contrast)
    orientations_count = 16

    #create SOM_
    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.project(dataset)
    plot_projections = projections_to_plot(projections, (extractor1.spike_reader.metadata.no_units, extractor1.mili_duration),
                        10, size[0] - 1, orientations_count=orientations_count)

    #print(plot_projections.shape)
    color_inc = 255 // (size[0] - 1)

    # because the dataset is merged we need two vector for Y, one for the emplitudes and one for the mean
    Y1 = np.empty(((plot_projections.shape[0] * plot_projections.shape[1]) // 2,), dtype=np.uint32)
    ori = -45
    inc = 45
    interval = 10

    for i in range(Y1.shape[0]):
        if i % interval == 0:
            ori += inc
        Y1[i] = ori

    Y2 = np.copy(Y1)

    return plot_projections, color_inc, Y1, Y2

'''
return: X = 2D matrix of features
        Y = 1D array of labels
        desc = a string representing human redable configuration of the dataset
'''
def build_histogram_datasets_amp_vs_mean(size, tau, contrast=-1, use_mean_amp=False, use_mean_neuron=False, use_z_score=False, use_fisher=False, use_start=False, use_end=True, path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=use_start,
                                                        use_end=use_end)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=use_start,
                                                        use_end=use_end)

    desc = build_description(size=size, tau=tau, contrast=contrast,
                                    use_fisher=use_fisher, use_mean=use_mean_amp, use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score, use_start=use_start, use_end=use_end)

    plot_projections, color_inc, Y1, Y2 = __SOM_dataset_merged(trial_extractor_amplitudes,
                                                                    trial_extractor_mean,
                                                                    size,
                                                                    tau,
                                                                    contrast)
    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    X1 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X2 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)

    # there are 8 orientations
    orientations_cnt = 8
    trials_per_ori_cnt = 10
    dataset2 = 2
    trials_idx = 0
    for i in range(orientations_cnt):

        for j in range(trials_per_ori_cnt):
            color_array_to_hist(X1[i * trials_per_ori_cnt + j, :], color_trials[trials_idx], color_inc)
            trials_idx += 1

        for j in range(trials_per_ori_cnt):
            color_array_to_hist(X2[i * trials_per_ori_cnt + j, :], color_trials[trials_idx], color_inc)
            trials_idx += 1

    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return X1, X2, Y1, Y2, plot_projections, color_inc, desc


'''
Return: X1s = list of feature 2D matrixes for amplitude dataset
                # NOTE: Their are 4 matrixes:
                                1 = Features aplied to interval [START - ON]
                                2 = Features aplied to interval [ON - ON + 500]
                                3 = Features aplied to interval [ON + 500 - OFF]
                                4 = Features aplied to interval [OFF - END]
                # NOTE: this split aplies also for X2s, Y1s, Y2s
        X2s = list of feature 2D matrixes for mean amplitude dataset
        Y1 = label 1D array for amplitude datset
                # NOTE: Y1 and Y2 are not lists their are arrays
        Y2 = label 1D array for mean amplidute dataset
        plot_projections = bands for the merged plot
        color_inc = how much a channel of the color changes for one color to the next

# NOTE: Y1 == Y2
'''
def build_split_histogram_datasets_amp_vs_mean(size, tau, contrast=-1, use_mean_amp=False, use_mean_neuron=False, use_z_score=False, use_diff=True, use_fisher=False, path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=True,
                                                        use_end=True)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=True,
                                                        use_end=True)

    desc = build_description(size=size, tau=tau, contrast=contrast,
                                use_fisher=use_fisher, use_mean=use_mean_amp, use_mean_neuron=use_mean_neuron,
                                use_z_score=use_z_score, use_start=True, use_end=True)

    plot_projections, color_inc, Y1, Y2 = __SOM_dataset_merged(trial_extractor_amplitudes,
                                                                    trial_extractor_mean,
                                                                    size,
                                                                    tau,
                                                                    contrast)
    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    X1s = []
    X2s = []

    X11 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X12 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X13 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X14 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)

    X21 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X22 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X23 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)
    X24 = np.zeros((color_trials.shape[0] // 2, size[0] * size[1] * size[2]), dtype=np.uint32)

    X1s.append(X11)
    X1s.append(X12)
    X1s.append(X13)
    X1s.append(X14)

    X2s.append(X21)
    X2s.append(X22)
    X2s.append(X23)
    X2s.append(X24)

    # there are 8 orientations
    orientations_cnt = 8
    trials_per_ori_cnt = 10
    dataset2 = 2
    trials_idx = 0

    for i in range(orientations_cnt):

        for j in range(trials_per_ori_cnt):
            # build histogram only from values in [START - ON]
            color_array_to_hist(X11[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, 0:mili_between_START_ON], color_inc)
            # build histogram only from values in [ON - ON + 500]
            color_array_to_hist(X12[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, mili_between_START_ON:mili_between_START_ON + 500], color_inc)
            # build histogram only from values in [ON + 500 - OFF]
            color_array_to_hist(X13[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, mili_between_START_ON + 500:mili_between_START_OFF], color_inc)
            # build histogram only from values in [OFF - END]
            color_array_to_hist(X14[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, mili_between_START_OFF: mili_between_START_END], color_inc)
            trials_idx += 1

        for j in range(trials_per_ori_cnt):
            # build histogram only from values in [START - ON]
            color_array_to_hist(X21[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, 0:mili_between_START_ON], color_inc)
            # build histogram only from values in [ON - ON + 500]
            color_array_to_hist(X22[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, mili_between_START_ON:mili_between_START_ON + 500], color_inc)
            # build histogram only from values in [ON + 500 - OFF]
            color_array_to_hist(X23[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, mili_between_START_ON + 500:mili_between_START_OFF], color_inc)
            # build histogram only from values in [OFF - END]
            color_array_to_hist(X24[i * trials_per_ori_cnt + j, :], color_trials[trials_idx, mili_between_START_OFF: mili_between_START_END], color_inc)
            trials_idx += 1

    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return X1s, X2s, Y1, Y2, plot_projections, color_inc, desc


'''
return: X = 2D matrix of features
        Y = 1D array of labels
        desc = a string representing human redable configuration of the dataset
'''
def build_color_dataset_merged_time(size, tau, contrast=-1, use_mean_amp=False, use_mean_neuron=False, use_z_score=False, use_diff=True, use_fisher=False, use_start=False, use_end=True, path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=use_start,
                                                        use_end=use_end)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=use_start,
                                                        use_end=use_end)

    desc = build_description(size=size, tau=tau, contrast=contrast,
                                    use_fisher=use_fisher, use_mean=use_mean_amp, use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score, use_start=True, use_end=True)

    plot_projections, color_inc, Y1, Y2 = __SOM_dataset_merged(trial_extractor_amplitudes,
                                                                    trial_extractor_mean,
                                                                    size,
                                                                    tau,
                                                                    contrast)
    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    X1 = np.zeros((color_trials.shape[0] // 2, color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)
    X2 = np.zeros((color_trials.shape[0] // 2, color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)
    # there are 8 orientations
    orientations_cnt = 8
    trials_per_ori_cnt = 10
    dataset2 = 2
    trials_idx = 0
    for i in range(orientations_cnt):

        # amplitude
        for j in range(trials_per_ori_cnt):
            X1[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx]
            trials_idx += 1

        # mean
        for j in range(trials_per_ori_cnt):
            X2[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx]
            trials_idx += 1

    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return X1, X2, Y1, Y2, plot_projections, color_inc, desc

'''
return: X = 2D matrix of features
        Y = 1D array of labels
        desc = a string representing human redable configuration of the dataset
'''
def build_color_dataset_merged_with_split_time(size, tau, contrast=-1, use_mean_amp=False, use_mean_neuron=False, use_z_score=False, use_diff=True, use_fisher=False, path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=True,
                                                        use_end=True)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=True,
                                                        use_end=True)

    desc = build_description(size=size, tau=tau, contrast=contrast,
                                use_fisher=use_fisher, use_mean=use_mean_amp, use_mean_neuron=use_mean_neuron,
                                use_z_score=use_z_score, use_start=True, use_end=True)

    plot_projections, color_inc, Y1, Y2 = __SOM_dataset_merged(trial_extractor_amplitudes,
                                                                    trial_extractor_mean,
                                                                    size,
                                                                    tau,
                                                                    contrast)
    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    X1s = []
    X2s = []

    X11 = np.zeros((color_trials.shape[0] // 2, mili_between_START_ON, color_trials.shape[2]), dtype=np.uint32)
    X12 = np.zeros((color_trials.shape[0] // 2, 500, color_trials.shape[2]), dtype=np.uint32)
    X13 = np.zeros((color_trials.shape[0] // 2, mili_between_ON_OFF - 500, color_trials.shape[2]), dtype=np.uint32)
    X14 = np.zeros((color_trials.shape[0] // 2, mili_between_OFF_END, color_trials.shape[2]), dtype=np.uint32)

    X21 = np.zeros((color_trials.shape[0] // 2, mili_between_START_ON, color_trials.shape[2]), dtype=np.uint32)
    X22 = np.zeros((color_trials.shape[0] // 2, 500, color_trials.shape[2]), dtype=np.uint32)
    X23 = np.zeros((color_trials.shape[0] // 2, mili_between_ON_OFF - 500, color_trials.shape[2]), dtype=np.uint32)
    X24 = np.zeros((color_trials.shape[0] // 2, mili_between_OFF_END, color_trials.shape[2]), dtype=np.uint32)

    X1s.append(X11)
    X1s.append(X12)
    X1s.append(X13)
    X1s.append(X14)

    X2s.append(X21)
    X2s.append(X22)
    X2s.append(X23)
    X2s.append(X24)

    # there are 8 orientations
    orientations_cnt = 8
    trials_per_ori_cnt = 10
    dataset2 = 2
    trials_idx = 0

    for i in range(orientations_cnt):

        for j in range(trials_per_ori_cnt):
            # build histogram only from values in [START - ON]
            X11[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, 0:mili_between_START_ON]
            # build histogram only from values in [ON - ON + 500]
            X12[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, mili_between_START_ON:mili_between_START_ON + 500]
            # build histogram only from values in [ON + 500 - OFF]
            X13[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, mili_between_START_ON + 500:mili_between_START_OFF]
            # build histogram only from values in [OFF - END]
            X14[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, mili_between_START_OFF: mili_between_START_END]
            trials_idx += 1

        for j in range(trials_per_ori_cnt):
            # build histogram only from values in [START - ON]
            X21[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, 0:mili_between_START_ON]
            # build histogram only from values in [ON - ON + 500]
            X22[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, mili_between_START_ON:mili_between_START_ON + 500]
            # build histogram only from values in [ON + 500 - OFF]
            X23[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, mili_between_START_ON + 500:mili_between_START_OFF]
            # build histogram only from values in [OFF - END]
            X24[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx, mili_between_START_OFF: mili_between_START_END]
            trials_idx += 1
    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return X1s, X2s, Y1, Y2, plot_projections, color_inc, desc

def __SOM_dataset_merged_orientations(extractor1, extractor2, size, tau, contrast, orientations):
    start = time.time()
    dataset = build_merged_dataset_orientations(extractor1, extractor2, orientations=orientations, tau=tau, contrast=contrast)
    orientations_count = len(orientations) * 2

    #create SOM_
    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.project(dataset)
    plot_projections = projections_to_plot(projections, (extractor1.spike_reader.metadata.no_units, extractor1.mili_duration),
                        10 * len(contrast), size[0] - 1, orientations_count=orientations_count)
    color_inc = 255 // (size[0] - 1)

    # because the dataset is merged we need two vector for Y, one for the emplitudes and one for the mean
    Y1 = np.empty(((plot_projections.shape[0] * plot_projections.shape[1]) // 2,), dtype=np.uint32)
    interval = 10 * len(contrast)
    j = 0
    for i in range(Y1.shape[0]):
        if i % interval == 0:
            ori = orientations[j]
            j += 1
        Y1[i] = ori

    Y2 = np.copy(Y1)

    return plot_projections, color_inc, Y1, Y2

def build_full_trial_datasets_amp_vs_mean_color(size, tau, orientations=[0, 45, 90, 135, 180, 225, 270, 315], contrast=[25, 50, 100], use_mean_amp=False, use_mean_neuron=False, use_z_score=False, use_diff=True, use_fisher=False, use_start=False, use_end=True, path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=use_start,
                                                        use_end=use_end)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=use_start,
                                                        use_end=use_end)

    desc = build_description(size=size, tau=tau, contrast=contrast,
                                    use_fisher=use_fisher, use_mean=use_mean_amp, use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score, use_start=True, use_end=True)

    plot_projections, color_inc, Y1, Y2 = __SOM_dataset_merged_orientations(trial_extractor_amplitudes,
                                                                    trial_extractor_mean,
                                                                    size,
                                                                    tau,
                                                                    contrast,
                                                                    orientations)
    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    X1 = np.zeros((color_trials.shape[0] // 2, color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)
    X2 = np.zeros((color_trials.shape[0] // 2, color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)

    # there are 8 orientations
    orientations_cnt = len(orientations)
    trials_per_ori_cnt = 10 * len(contrast)
    dataset2 = 2
    trials_idx = 0
    for i in range(orientations_cnt):

        for j in range(trials_per_ori_cnt):
            X1[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx]
            trials_idx += 1

        for j in range(trials_per_ori_cnt):
            X2[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx]
            trials_idx += 1

    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return X1, X2, Y1, Y2, plot_projections, color_inc, desc

def __SOM_dataset_merged_centro(extractor1, extractor2, size, tau, contrast, orientations):
    start = time.time()
    dataset = build_merged_dataset_orientations(extractor1, extractor2, orientations=orientations, tau=tau, contrast=contrast)
    orientations_count = len(orientations) * 2

    #create SOM_
    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.centro_project(dataset)
    plot_projections = projections.reshape((orientations_count, 10 * len(contrast),  extractor1.mili_duration, extractor1.spike_reader.metadata.no_units))

    # because the dataset is merged we need two vector for Y, one for the emplitudes and one for the mean
    Y1 = np.empty(((plot_projections.shape[0] * plot_projections.shape[1]) // 2,), dtype=np.uint32)
    print(Y1.shape)
    interval = 10 * len(contrast)
    j = 0
    for i in range(Y1.shape[0]):
        if i % interval == 0:
            ori = orientations[j]
            j += 1
        Y1[i] = ori

    Y2 = np.copy(Y1)

    return plot_projections, Y1, Y2

def build_full_trial_datasets_amp_vs_mean_centro(size, tau, orientations=[0, 45, 90, 135, 180, 225, 270, 315], contrast=[25, 50, 100], use_mean_amp=False, use_mean_neuron=False, use_z_score=False, use_fisher=False, use_start=False, use_end=True, path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))
    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=use_start,
                                                        use_end=use_end)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=use_start,
                                                        use_end=use_end)

    desc = build_description(size=size, tau=tau, contrast=contrast,
                                    use_fisher=use_fisher, use_mean=use_mean_amp, use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score, use_start=True, use_end=True)

    plot_projections, Y1, Y2 = __SOM_dataset_merged_centro(trial_extractor_amplitudes,
                                                                    trial_extractor_mean,
                                                                    size,
                                                                    tau,
                                                                    contrast,
                                                                    orientations)
    color_trials = plot_projections.reshape([plot_projections.shape[0] * plot_projections.shape[1], plot_projections.shape[2], plot_projections.shape[3]])

    X1 = np.zeros((color_trials.shape[0] // 2, color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)
    X2 = np.zeros((color_trials.shape[0] // 2, color_trials.shape[1], color_trials.shape[2]), dtype=np.uint32)
    # there are 8 orientations
    orientations_cnt = len(orientations)
    trials_per_ori_cnt = 10 * len(contrast)
    dataset2 = 2
    trials_idx = 0
    for i in range(orientations_cnt):

        for j in range(trials_per_ori_cnt):
            X1[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx]
            trials_idx += 1

        for j in range(trials_per_ori_cnt):
            X2[i * trials_per_ori_cnt + j, :] = color_trials[trials_idx]
            trials_idx += 1

    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return X1, X2, Y1, Y2, plot_projections, desc




if __name__ == '__main__':
    pass
    #build_full_trial_datasets_amp_vs_mean_centro((5, 5, 5), 20, orientations=[0, 90, 315], contrast=[100])
