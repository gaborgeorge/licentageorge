import sys
import os
sys.path.append(os.path.join('..', '..'))

from sklearn.metrics import confusion_matrix
from sklearn import svm
import numpy as np

from Classification.classification_utils import transform

def svm_classification(X_train, X_test, Y_train, Y_test):
    if X_train.ndim != 2:
        X_train, X_test = transform(X_train, X_test)

    clf = svm.SVC(gamma='scale', decision_function_shape='ovo')
    clf.fit(X_train, Y_train)

    predict = clf.predict(X_test)
    confusion = confusion_matrix(Y_test, predict)
    confusion = confusion / confusion.astype(np.float).sum(axis=1)

    score = clf.score(X_test, Y_test)

    return score, confusion
