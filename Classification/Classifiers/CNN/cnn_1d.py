import tensorflow as tf
from tensorflow.keras.models import Sequential
from tensorflow.keras.layers import Conv1D, MaxPooling1D, Dense, Flatten, Dropout
from tensorflow.keras.callbacks import EarlyStopping
from tensorflow.keras.callbacks import ModelCheckpoint
from sklearn.metrics import confusion_matrix

import numpy as np
import sys


'''
# NOTE: This makes a copy for Y
'''
def convert_one_hot(y, nr_classes, d):
    Y = np.zeros((y.shape[0], nr_classes), dtype=np.uint32)
    for i in range(y.shape[0]):
        Y[i, d[y[i]]] = 1

    return Y

#TO DO: vezi daca kernel_size trebuie
def cnn_1d_classification(X_train, X_test, Y_train, Y_test, kernel_size=10):
    model = Sequential()
    
    # convert to one hot
    o = np.unique(Y_train)
    z = np.arange(len(o))
    d = {}
    for i in range(len(o)):
        d[o[i]] = z[i]

    Y_train_one_hot = convert_one_hot(Y_train, len(np.unique(Y_train)), d)
    Y_test_one_hot = convert_one_hot(Y_test, len(np.unique(Y_train)), d)

    print('[{0}] Building model'.format(sys._getframe().f_code.co_name))
    print(X_train.shape)
    print(X_test.shape)
    model.add(Conv1D(filters=64, kernel_size=kernel_size, strides=1, activation='relu', input_shape=(X_train.shape[1], X_train.shape[2])))
    model.add(MaxPooling1D(2))

    model.add(Conv1D(filters=64, kernel_size=kernel_size, strides=1, activation='relu'))
    model.add(MaxPooling1D(2))

    model.add(Flatten())
    model.add(Dense(256, activation='sigmoid')) # sigmoid
    model.add(Dropout(0.5))
    model.add(Dense(128, activation='sigmoid')) # sigmoi

    model.add(Dense(len(o), activation='softmax'))

    model.compile(loss='mean_squared_error',
                    optimizer='adam',
                    metrics=['accuracy'])

    es = EarlyStopping(monitor = 'acc', patience = 3)
    mc = ModelCheckpoint('cnn_best_model.hs', monitor='acc', mode='max', save_best_only=True)

    print('[{0}] Train model'.format(sys._getframe().f_code.co_name))
    model.fit(X_train, Y_train_one_hot, batch_size=8, epochs=10, validation_split=0.0, callbacks=[es, mc])

    print('[{0}] Predict'.format(sys._getframe().f_code.co_name))
    predict = model.predict(X_test)
    Y_predict = np.copy(Y_test)
    max_indxs = np.argmax(predict, axis=1)

    for i in range(max_indxs.shape[0]):
        Y_predict[i] = o[max_indxs[i]]

    print(Y_predict, Y_test)
    e = model.evaluate(X_test, Y_test_one_hot)

    confusion = confusion_matrix(Y_test, Y_predict, labels=np.unique(Y_train))
    confusion = confusion / confusion.astype(np.float).sum(axis=1)
    confusion = np.round(confusion, decimals=2)

    tf.keras.backend.clear_session()

    return e[1], confusion
