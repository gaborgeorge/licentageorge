import sys
import os
sys.path.append(os.path.join('..', '..'))

from sklearn.metrics import confusion_matrix
from sklearn import tree
import numpy as np

from Classification.classification_utils import transform

def decision_tree_classification(X_train, X_test, Y_train, Y_test):
    if X_train.ndim != 2:
        X_train, X_test = transform(X_train, X_test)

    dtc = tree.DecisionTreeClassifier(criterion='entropy')
    dtc = dtc.fit(X_train, Y_train)

    predict = dtc.predict(X_test)
    confusion = confusion_matrix(Y_test, predict)
    confusion = confusion / confusion.astype(np.float).sum(axis=1)
    confusion = np.round(confusion, decimals=2)

    score = dtc.score(X_test, Y_test)

    return score, confusion
