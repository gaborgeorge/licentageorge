import numpy as np
import sys

from Utils.KernelDecay import apply_kernel_function_trial
from Utils.KernelDecay import apply_kernel_decay_neuron

def transform(X_tr, X_te):
    X_train = np.empty((X_tr.shape[0], X_tr.shape[1] * X_tr.shape[2]), dtype=np.float32)
    X_test = np.empty((X_te.shape[0], X_te.shape[1] * X_te.shape[2]), dtype=np.float32)

    for i in range(X_train.shape[0]):
        X_train[i] = X_tr[i].flatten()

    for i in range(X_test.shape[0]):
        X_test[i] = X_te[i].flatten()

    return X_train, X_test

'''
Parameters: X = feature matrix
            Y = label vector
            permutation = how to choose the split
            train_percentage = how much data to put in the train set

# NOTE: X must be a 2D or 3D array
'''
def my_split(X, Y, permutation=None, train_percentage=0.8):
    nr_samples = X.shape[0]

    nr_samples_train = int(nr_samples * train_percentage)
    nr_samples_test = nr_samples - nr_samples_train

    # for the time we use the colors explicitely
    if X.ndim == 3:
        X_train = np.empty((nr_samples_train, X.shape[1], X.shape[2]), dtype=np.uint32)
        X_test = np.empty((nr_samples_test, X.shape[1], X.shape[2]), dtype=np.uint32)
    # for the time we use the color histogram
    elif X.ndim == 2:
        X_train = np.empty((nr_samples_train, X.shape[1]), dtype=np.uint32)
        X_test = np.empty((nr_samples_test, X.shape[1]), dtype=np.uint32)

    Y_train = np.empty((nr_samples_train,), dtype=np.uint32)
    Y_test = np.empty((nr_samples_test,), dtype=np.uint32)


    samples_per_orientation = nr_samples // len(np.unique(Y))
    samples_train_orientation = int(samples_per_orientation * train_percentage)
    samples_test_orientation = samples_per_orientation - samples_train_orientation

    if permutation is None:
        permutation = np.random.permutation(samples_per_orientation)

    for o in range(len(np.unique(Y))):
        j = 0
        for i in range(samples_train_orientation):
            X_train[samples_train_orientation * o + i, :] = X[samples_per_orientation * o + permutation[j], :]
            Y_train[samples_train_orientation * o + i] = Y[samples_per_orientation * o + permutation[j]]
            j += 1

        for i in range(samples_test_orientation):
            X_test[samples_test_orientation * o + i, :] = X[samples_per_orientation * o + permutation[j], :]
            Y_test[samples_test_orientation * o + i] = Y[samples_per_orientation * o + permutation[j]]
            j += 1

    # shuffle the training data
    perm = np.random.permutation(X_train.shape[0])
    X_train = X_train[perm]
    Y_train = Y_train[perm]

    return X_train, X_test, Y_train, Y_test



'''
DESCRIPTION: this will create a SOM dataset with trials that have a specific contrast and orientation
'''
def build_merged_dataset_orientations(trial_extractor1, trial_extractor2, orientations=[0, 45, 90, 135, 180, 225, 270, 315], tau=20, count=10, contrast=[25, 50, 100]):
    print('[{0}] Start'.format(sys._getframe().f_code.co_name))

    trials1 = [[], [], [], [], [], [], [], []]
    trials2 = [[], [], [], [], [], [], [], []]

    orientation_inc = 45

    for i in range(len(trial_extractor1.trials)):
        contrast_ok = False
        for c in contrast:
            if trial_extractor1.trials[i]['meta']['contrast'] == c:
                contrast_ok = True
                break

        orientation_ok = False
        orientation = trial_extractor1.trials[i]['meta']['direction']
        for o in orientations:
            if o == orientation:
                orientation_ok = True
                break

        if contrast_ok is True and orientation_ok is True:
            trial_values1 = trial_extractor1.trials[i]['spikes']
            trial_values2 = trial_extractor2.trials[i]['spikes']

            trial_decay1 = apply_kernel_function_trial(trial_values1, trial_extractor1.mili_duration, tau, apply_kernel_decay_neuron)
            trial_features1 = np.transpose(trial_decay1)
            # NOTE:
            trial_decay2 = apply_kernel_function_trial(trial_values2, trial_extractor2.mili_duration, tau, apply_kernel_decay_neuron)
            trial_features2 = np.transpose(trial_decay2)
            trials1[orientation // 45].append(trial_features1)
            trials2[orientation // 45].append(trial_features2)
    dataset = []
    for i in range(len(trials1)):
        if len(trials1[i]) != 0:
            dataset.append(np.vstack(trials1[i]))
            dataset.append(np.vstack(trials2[i]))

    result = np.vstack(dataset)
    print('[{0}] COMPLETE - dataset shape {1}'.format(sys._getframe().f_code.co_name, result.shape))

    return result

'''
DESCRIPTION: this will create a SOM dataset with trials that have a specific contrast and orientation

RETURN: for each orientation a list o trials in shape(number_neurons, time_values)

'''
def build_dataset_orientations(trial_extractor, orientations=[0, 45, 90, 135, 180, 225, 270, 315], tau=20, contrast=[100]):
    print('[{0}] Start'.format(sys._getframe().f_code.co_name))

    trials = {}
    for o in orientations:
        trials[o] = []

    for i in range(len(trial_extractor.trials)):
        contrast_ok = False
        for c in contrast:
            if trial_extractor.trials[i]['meta']['contrast'] == c:
                contrast_ok = True
                break

        orientation_ok = False
        orientation = trial_extractor.trials[i]['meta']['direction']
        for o in orientations:
            if o == orientation:
                orientation_ok = True
                break

        if contrast_ok is True and orientation_ok is True:
            trial_values = trial_extractor.trials[i]['spikes']

            trial_decay = apply_kernel_function_trial(trial_values, trial_extractor.mili_duration, tau, apply_kernel_decay_neuron)
            trials[orientation].append(trial_decay)

    print('[{0}] COMPLETE'.format(sys._getframe().f_code.co_name))

    return trials
