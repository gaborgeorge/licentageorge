import sys
import os
sys.path.append(os.path.join('..', '..'))

from Classification.Classifiers.CNN.cnn_1d import cnn_1d_classification
from Classification.Classifiers.DecisionTree.decision_tree import decision_tree_classification
from Classification.Classifiers.SVM.support_vector_machines import svm_classification
from Classification.RawConvolution.Raw_dataset_builder import build_neuron_dataset
from Classification.RawConvolution.Raw_dataset_builder import build_neurons_dataset
from Classification.RawConvolution.Raw_dataset_builder import build_neurons_dataset_without



import numpy as np


def classify_each_neuron(dataset_dict, classification_iterations=5, percent_train=0.8, classifier=cnn_1d_classification, normalize=False):
    a_key = next(iter(dataset_dict.keys()))
    nr_neurons = dataset_dict[a_key][0].shape[0]

    neuron_dict = {}

    for n in range(nr_neurons):

        aq = np.empty(classification_iterations, dtype=np.float32)
        conf = 0

        for c_i in range(classification_iterations):

            # building dataset
            permutation = np.random.permutation(len(dataset_dict[a_key]))
            X_train, X_test, Y_train, Y_test = build_neuron_dataset(dataset_dict, n, permutation, percent_train=percent_train)

            if normalize is True:
                X_train = X_train / np.amax(X_train)
                X_test = X_test / np.amax(X_test)

            # classification
            aq_a, conf_a = classifier(X_train, X_test, Y_train, Y_test)
            aq[c_i] = aq_a
            conf += conf_a

        conf /= classification_iterations

        neuron_dict[str(n)] = (np.mean(aq), np.std(aq), conf)

    return neuron_dict

def classify_all(dataset_dict, classification_iterations=5, percent_train=0.8, classifier=cnn_1d_classification, normalize=False):
    a_key = next(iter(dataset_dict.keys()))
    nr_neurons = dataset_dict[a_key][0].shape[0]

    aq = np.empty(classification_iterations, dtype=np.float32)
    conf = 0

    for c_i in range(classification_iterations):

        # building dataset
        permutation = np.random.permutation(len(dataset_dict[a_key]))
        X_train, X_test, Y_train, Y_test = build_neurons_dataset(dataset_dict, permutation, percent_train=percent_train)

        if normalize is True:
            X_train = X_train / np.amax(X_train)
            X_test = X_test / np.amax(X_test)

        # classification
        aq_a, conf_a = classifier(X_train, X_test, Y_train, Y_test)#, second_pool=False)
        aq[c_i] = aq_a
        conf += conf_a

    conf /= classification_iterations
    print(aq)
    return (np.mean(aq), np.std(aq), conf)

def classify_all_without(dataset_dict, neurons, classification_iterations=5, percent_train=0.8, classifier=cnn_1d_classification, normalize=False):
    a_key = next(iter(dataset_dict.keys()))
    nr_neurons = dataset_dict[a_key][0].shape[0]

    aq = np.empty(classification_iterations, dtype=np.float32)
    conf = 0

    for c_i in range(classification_iterations):

        # building dataset
        permutation = np.random.permutation(len(dataset_dict[a_key]))
        X_train, X_test, Y_train, Y_test = build_neurons_dataset_without(dataset_dict, neurons, permutation, percent_train=percent_train)

        if normalize is True:
            X_train = X_train / np.amax(X_train)
            X_test = X_test / np.amax(X_test)

        # classification
        aq_a, conf_a = classifier(X_train, X_test, Y_train, Y_test)#, second_pool=False)
        aq[c_i] = aq_a
        conf += conf_a

    conf /= classification_iterations
    print(aq)
    return (np.mean(aq), np.std(aq), conf)
