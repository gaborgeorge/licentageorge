import sys
import os
sys.path.append(os.path.join('..', '..'))

from Classification.Classifiers.CNN.cnn_1d import cnn_1d_classification
from Classification.Classifiers.DecisionTree.decision_tree import decision_tree_classification
from Classification.Classifiers.SVM.support_vector_machines import svm_classification
from Classification.RawConvolution.Raw_utils import get_trials_with_orientations
from Classification.RawConvolution.Raw_classification import classify_each_neuron
from Classification.RawConvolution.Raw_classification import classify_all
from Classification.RawConvolution.Raw_classification import classify_all_without
from Visualization.Plots_matplot import plot_bar_chart

import numpy as np

def get_description(
                use_mean_amplitude,
                use_mean_neuron,
                use_z_score,
                use_fisher,
                use_start,
                use_end,
                percent_train,
                orientations,
                contrasts,
                d=''):
    desc = ''

    if use_mean_amplitude is True:
        desc += 'Mean_AMP'

    if use_mean_neuron is True:
        desc += 'Mean_AMP_Neuron'

    if use_z_score is True:
        desc += '_Z_score_'

    if use_fisher is True:
        desc += '_Fisher_'

    desc += '_Train_Per_' + str(percent_train)

    desc += '_Ori_' + str(orientations)

    desc += '_Cont_' + str(contrasts)

    if use_start is True:
        desc += '_Start_'
    else:
        desc += '_On_'

    if use_end is True:
        desc += '_End_'
    else:
        desc += '_Off_'

    desc += d

    return desc


def save_results(neuron_dict, file, folder='Results'):
    file_path = os.path.join(folder, file + '.txt')

    with open(file_path, 'w') as f:
        for n, t in neuron_dict.items():
            f.write('Neuron %s - Aq %f - Std %f\nConfusion\n%s\n\n' % (n, t[0], t[1], t[2]))

    return file_path

def save_plot(neuron_dict, file, folder='Results'):
    file_path = os.path.join(folder, file + '.png')

    X = []
    Y = []

    for n, t in neuron_dict.items():
        X.append(n)
        Y.append(t[0])

    plot_bar_chart(X, Y,
                title='',
                x_label='Neurons',
                y_label='Aq',
                save=True,
                save_file=file_path)

    return file_path

'''
Parameters: use_mean_amplitude - use mean amplitude for spike value
            use_mean_neuron    - use mean amplitude per channel for spike value
            use_z_score        - if it should apply z-score
            use_start          - if it should consider the START event
            use_end            - if it should consider the END event
            orientations       - list of orientations should consider
            contrasts          - list of contrasts to consider
            classification_iterations
                               - how many classifiers to build
            percent_train      - amount of train use_set
            use_all            - if it should do a classification with all the channels
            use_each           - if it should do a classfication for every channel
            use_without        - if it should do a classfication without some channels
            neurons            - what channels to remove if use_withou is True
            d                  - description
            folder             - where to save the results
            classifier         - what classifier to use
'''
def classify(use_mean_amplitude=False,
            use_mean_neuron=False,
            use_z_score=False,
            use_fisher=False,
            use_start=False,
            use_end=False,
            orientations=[0, 45, 90, 135, 180, 225, 270, 315],
            contrasts=[25, 50, 100],
            classification_iterations=1,
            percent_train=0.8,
            use_all=False,
            use_each=False,
            use_without=False,
            neurons=[12],
            d='',
            folder='Results_test',
            classifier=cnn_1d_classification):

    dataset_dict, trial_extractor = get_trials_with_orientations(
                    use_mean_amplitude=use_mean_amplitude,
                    use_mean_neuron=use_mean_neuron,
                    use_z_score=use_z_score,
                    use_fisher=use_fisher,
                    use_start=use_start,
                    use_end=use_end,
                    orientations=orientations,
                    contrasts=contrasts)

    if use_z_score is False:
        normalize = True
    else:
        normalize = False

    if use_each is True:
        neuron_dict = classify_each_neuron(dataset_dict, classification_iterations=classification_iterations, percent_train=percent_train, classifier=classifier, normalize=normalize)
    else:
        neuron_dict = {}

    if use_all is True:
        neuron_dict['all'] = classify_all(dataset_dict, classification_iterations=classification_iterations, percent_train=percent_train, classifier=classifier, normalize=normalize)

    if use_without is True:
        neuron_dict['out'] = classify_all_without(dataset_dict, neurons=neurons, classification_iterations=classification_iterations, percent_train=percent_train, classifier=classifier, normalize=normalize)

    file = get_description(
                    use_mean_amplitude,
                    use_mean_neuron,
                    use_z_score,
                    use_fisher,
                    use_start,
                    use_end,
                    percent_train,
                    orientations,
                    contrasts,
                    d=d)

    file_path = save_results(neuron_dict, file=file, folder=folder)

    plot_paht = save_plot(neuron_dict, file=file, folder=folder)

    print('[{0}] Saved in {1}'.format(sys._getframe().f_code.co_name, file_path))

if __name__ == '__main__':
    #classify(use_mean_neuron=True, use_all=True, contrasts=[100], classification_iterations=2, d='_CNN', percent_train=0.8, folder='DEMO_RESULTS')
    classify(use_mean_neuron=True, use_all=True, contrasts=[100], classification_iterations=2, d='_CNN', percent_train=0.2, folder='DEMO_RESULTS', classifier=decision_tree_classification)
