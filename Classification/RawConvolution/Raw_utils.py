import sys
import os
sys.path.append(os.path.join('..', '..'))
import numpy as np

from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
from DataSet.TrialExtractor import TrialExtractor
from config import ssd_file, result_file, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END, mili_between_ON_OFF, mili_between_OFF_END
pro_path = os.path.join('..', '..')

from Classification.classification_utils import build_dataset_orientations

'''
Extract one neuron activity and aply decay on it
'''
def get_trials_with_orientations(
            orientations,
            contrasts,
            use_mean_amplitude=False,
            use_mean_neuron=False,
            use_z_score=False,
            use_fisher=False,
            use_start=False,
            use_end=False,
            path=pro_path):
    spike_reader = SpikeTimestampReader(os.path.join(path, 'Data', 'M017_0002_sorted_full'), ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(os.path.join(path, 'Data', 'M017_0002_sorted_full'), result_file))

    trial_extractor = TrialExtractor(
                        spike_reader,
                        trial_meta_reader,
                        use_mean_amplitude=use_mean_amplitude,
                        use_mean_neuron=use_mean_neuron,
                        use_z_score=use_z_score,
                        use_fisher=use_fisher,
                        use_start=use_start,
                        use_end=use_end)

    orientations = build_dataset_orientations(
                        trial_extractor,
                        orientations=orientations,
                        contrast=contrasts)

    return orientations, trial_extractor

if __name__ == '__main__':
    get_orientation_with_trials()
