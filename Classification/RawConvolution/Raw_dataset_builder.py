import sys
import os
sys.path.append(os.path.join('..', '..'))

from Classification.classification_utils import my_split

import numpy as np

def __get_nr_trials(dataset_dict):
    nr_trials = 0
    for ts in dataset_dict.values():
        nr_trials += len(ts)

    return nr_trials

def extract_neuron_activity(dataset_dict, neuron_idx):

    nr_trials = __get_nr_trials(dataset_dict)

    a_key = next(iter(dataset_dict.keys()))
    trial_len = dataset_dict[a_key][0].shape[1]

    X = np.empty((nr_trials, trial_len), dtype=np.float32)
    Y = np.empty((nr_trials,), dtype=np.uint32)

    e_i = 0

    # go through orientation
    for o, ts in dataset_dict.items():

        # got through trials
        for t in ts:
            X[e_i] = t[neuron_idx]
            Y[e_i] = o
            e_i += 1

    return X, Y

def extract_neurons_activity(dataset_dict):

    nr_trials = __get_nr_trials(dataset_dict)

    a_key = next(iter(dataset_dict.keys()))
    nr_neurons = dataset_dict[a_key][0].shape[0]
    trial_len = dataset_dict[a_key][0].shape[1]

    X = np.empty((nr_trials, trial_len, nr_neurons), dtype=np.float32)
    Y = np.empty((nr_trials,), dtype=np.uint32)

    e_i = 0

    for o, ts in dataset_dict.items():
        for t in ts:
            X[e_i] = np.transpose(t)
            Y[e_i] = o
            e_i += 1

    return X, Y

def extract_neurons_activity_without(dataset_dict, neurons):
    nr_trials = __get_nr_trials(dataset_dict)

    a_key = next(iter(dataset_dict.keys()))
    nr_neurons = dataset_dict[a_key][0].shape[0]
    trial_len = dataset_dict[a_key][0].shape[1]

    # remove the values of neurons to exclude
    nr_neurons_dataset = nr_neurons - len(neurons)

    X = np.empty((nr_trials, trial_len, nr_neurons_dataset), dtype=np.float32)
    Y = np.empty((nr_trials,), dtype=np.uint32)
    e_i = 0

    for o, ts in dataset_dict.items():
        for t in ts:

            n_i = 0

            # exclude values of some neurons
            for n in range(nr_neurons):
                if n in neurons:
                    pass
                else:
                    X[e_i, :, n_i] = np.transpose(t[n, :])
                    n_i += 1

            Y[e_i] = o
            e_i += 1

    return X, Y

def build_neuron_dataset(dataset_dict, neuron_idx, permutation, percent_train=0.8):
    X, Y = extract_neuron_activity(dataset_dict, neuron_idx)
    X_train, X_test, Y_train, Y_test = my_split(X, Y, permutation=permutation, train_percentage=percent_train)

    X_train = X_train.reshape(X_train.shape + (1, ))
    X_test = X_test.reshape(X_test.shape + (1, ))

    return X_train, X_test, Y_train, Y_test

def build_neurons_dataset(dataset_dict, permutation, percent_train=0.8):
    X, Y = extract_neurons_activity(dataset_dict)
    X_train, X_test, Y_train, Y_test = my_split(X, Y, permutation=permutation, train_percentage=percent_train)

    return X_train, X_test, Y_train, Y_test

def build_neurons_dataset_without(dataset_dict, neurons_idx, permutation, percent_train=0.8):
    X, Y = extract_neurons_activity_without(dataset_dict, neurons_idx)
    X_train, X_test, Y_train, Y_test = my_split(X, Y, permutation=permutation, train_percentage=percent_train)

    return X_train, X_test, Y_train, Y_test
