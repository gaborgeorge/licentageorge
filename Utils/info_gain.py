import numpy as np
from numba import njit
from queue import Queue


'''
Parameters: Y - np array 1D, describing the labels of the classes

Return: entp - the entropy value of the dataset

# NOTE: Entropy = sum(-pi * log2(pi)), where pi is the fraction of example that
        have class i
'''
def entropy(Y):
    # get all the classes labels
    unique_classes = np.unique(Y)

    # create a vector to store all pi values
    pis = np.empty([unique_classes.shape[0], ], dtype=np.float32)

    for i in range(unique_classes.shape[0]):
        pis[i] = Y[Y == unique_classes[i]].shape[0] / Y.shape[0]
    entp = -np.sum(pis * np.log2(pis))

    return entp

'''
Parameters: X - np array 1D, reprezenting the values of one atribute over a dataset
            Y - np array 1D, describing the labels of the dataset

Return: info_gain - the information gain resulted if we split the dataset after that atribute

# NOTE: number of elements in X and Y must be equal

        Info gain = I(Y) - E(A),
            where I(Y) = entropy for the whole dataset
                  E(A) = sum(len(Yi) / len(Y) * I(Yi)), I(Yi) = entropy if we consider
                        only the samples with atribute value i
'''
def information_gain(A, Y):
    assert A.shape[0] == Y.shape[0], 'X and Y must have same number of elements'

    # get all values for the attribute
    values = np.unique(A)

    # find Iy
    Iy = entropy(Y)
    #print('Entropy', Iy)
    # find E(A)
    Ea = 0.0
    for i in range(values.shape[0]):
        Yi = Y[A == values[i]]
        Ea += Yi.shape[0] / Y.shape[0] * entropy(Yi)

    entp = Iy - Ea

    return entp


'''
Parameters: x - 1d numpy array
            y - 1d numpy array

result: None

# NOTE: the sort will go on x. y elements will be swap acourding to the elements in x
        using selection sort
'''
@njit
def sort_mirror(x, y):

    for i in range(x.shape[0]):
        min_idx = i

        for j in range(i+1, x.shape[0]):
            if x[j] < x[min_idx]:
                min_idx = j

        temp = x[min_idx]
        x[min_idx] = x[i]
        x[i] = temp

        temp = y[min_idx]
        y[min_idx] = y[i]
        y[i] = temp

'''
Parameters: X - np array 1D, representing the values of one feature
            Y - np array 1D, describing the labels of the samples
            nr_bins - the number of bins for every Attribute
                        # NOTE: DO we need a vector with one value for every feature, I Dont see the value right now

Return: thresholds - values that best split the data in nr_bins bins

# NOTE: Gain = Einitial - Enew
        where - Einitial = entropy of the whole dataset
              - Enew = |D1|/|D| * entropy(D1) + |D2|/|D| * entropy(D2)

# NOTE: threshold of 0.0 will be excluded, so if we ask for n splits we make get fewer
'''
def entropy_based_discretization(X, Y, nr_bins=3):
    # make copy of Y
    Y_c = np.copy(Y)
    X_c = np.copy(X)

    # sort X. sortY_c to mirror X_s
    sort_mirror(X_c, Y_c)

    # define a queue for X and one for Y and list for thresholds
    x_queue = Queue()
    y_queue = Queue()
    thresholds = []

    # find thresholds
    x_queue.put(X_c)
    y_queue.put(Y_c)

    for k in range(nr_bins):
        # get next range of values
        X_current = x_queue.get()
        Y_current = y_queue.get()

        if X_current.shape[0] == 0:
            continue

        # get initial entropy
        E_initial = entropy(Y_current)

        best_threshold = 0.0
        best_gain = 0.0

        for i in range(X_current.shape[0] - 1):
            threshold = (X_current[i] + X_current[i + 1]) / 2

            Y1 = Y_current[X_current <= threshold]
            Y2 = Y_current[X_current > threshold]
            #print(Y1, Y2)

            E_new = Y1.shape[0] / Y_current.shape[0] * entropy(Y1) + \
                    Y2.shape[0] / Y_current.shape[0] * entropy(Y2)

            gain = E_initial - E_new
            #print(gain, E_initial, E_new, threshold)
            if gain > best_gain:
                best_gain = gain
                best_threshold = threshold

        # if the threshold is 0 the split will produce the same results
        if best_threshold != 0.0:
            thresholds.append(best_threshold)

        X1_best = X_current[X_current <= best_threshold]
        X2_best = X_current[X_current > best_threshold]
        Y1_best = Y_current[X_current <= best_threshold]
        Y2_best = Y_current[X_current > best_threshold]
        x_queue.put(X1_best)
        x_queue.put(X2_best)
        y_queue.put(Y1_best)
        y_queue.put(Y2_best)


    thresholds = np.sort(np.array(thresholds))

    return thresholds


'''
Parameters: X - 2D matrix of features
            thresholds - a 1D array of values
            j - to witch column in X to apply the discretization

# NOTE: will change the value in column j in X to only have thresholds.shape[0] + 1 distinct values
'''
@njit
def discretization_helper(X, thresholds, j):
    for i in range(X.shape[0]):

        # find first threshold that is bigger than current value of feature
        ok = False
        for k in range(thresholds.shape[0]):
            if X[i, j] < thresholds[k]:
                X[i, j] = k
                ok = True
                break

        if ok is False:
            X[i, j] = thresholds.shape[0]

'''
Parameters: X = 2D matrix, representing the feature values of the dataset
            Y = 1D array, representing the labels of the dataset

Return: X_c - values are modified in place

# NOTE: X has continuous values for the feature
        We take each feature and

# NOTE: values in X are modified
        in case we need the old values of X make a copy, modify that and return it
'''
def discretization(X, Y):

    # may need to be changed to a copy
    X_c = np.copy(X)

    for j in range(X.shape[1]):

        # extract each feature from X (in this case each column)
        feature = X_c[:, j]
        # get the thresholds for each feature
        # NOTE: if it cant get 10 it will get as much as it can
        thresholds = entropy_based_discretization(feature, Y, nr_bins=10)

        # discretize the feature
        discretization_helper(X_c, thresholds, j)

    return X_c


'''
Parameters: X = 2D matrix, representing the feature values of the dataset
            Y = 1D array, representing the labels of the dataset

Return: gains = 1D array, element i represents the info gain of feature i
'''
def information_gains(X, Y):

    # create gains array
    gains = np.empty((X.shape[1],))
    #print(Y)
    # compute information gain for each features
    for j in range(X.shape[1]):

        # extract feature j
        feature = X[:, j]
        #if j == 0:
        #        print('Before', gains[j])
        gains[j] = information_gain(feature, Y)
        #if j == 0:
        #        print('After', gains[j])
    return gains


if __name__ == '__main__':
    information_gain(np.array([1, 1, 1]), np.array([1, 1, 1]))
