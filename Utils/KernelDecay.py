import numpy as np
import sys
import math

from numba import njit
from math import pow

'''
parameters: - neuron: a list of tuples (timestamp, amplitude)
            - no_values: how many values should the result have
            - tau: integration constant
            - dest: result

Result:     - None
            # NOTE: dest parameter will hold the values

Description: - this will uses exponential decay
'''
def apply_kernel_decay_neuron(neuron, no_values, tau, dest):
    # check if the neuron has no spikes
    if len(neuron) == 0:
        dest.fill(0)
        return

    # first value in kernel_decay
    # [0] = timestamp, [1] = amplitude
    if neuron[0][0] == 0:
        dest[0] = neuron[0][1]
        amplitude_index = 1
    else:
        dest[0] = 0
        amplitude_index = 0

    for i in range(1, no_values):
        # check if at time i the neuron had an aplitude
        if neuron[amplitude_index][0] == i:
            dest[i] = dest[i-1] + neuron[amplitude_index][1]
            amplitude_index += 1
            if amplitude_index == len(neuron):
                amplitude_index = 0
        else:
            dest[i] = dest[i-1] * pow(math.e, -1/tau)


'''
parameters: - neuron: a list of tuples (timestamp, amplitude)
            - no_values: how many values should the result have
            - tau: integration constant
            - dest: result

Result:     - None
                # NOTE: dest parameter will hold the values

Description: - this will not use decay
'''
def apply_kernel_zeros_neuron(neuron, no_values, tau, dest):
    # check if the neuron has no spikes
    if len(neuron) == 0:
        dest.fill(0)
        return

    # first value in kernel_decay
    # [0] = timestamp, [1] = amplitude
    if neuron[0][0] == 0:
        dest[0] = neuron[0][1]
        amplitude_index = 1
    else:
        dest[0] = 0
        amplitude_index = 0

    for i in range(1, no_values):
        # check if at time i the neuron had an aplitude
        if neuron[amplitude_index][0] == i:
            dest[i] = neuron[amplitude_index][1]
            amplitude_index += 1
            if amplitude_index == len(neuron):
                amplitude_index = 0
        else:
            dest[i] = 0

def apply_kernel_decay_neuron_constant(neuron, no_values, tau, dest):
    # check if the neuron has no spikes
    if len(neuron) == 0:
        dest.fill(0)
        return

    # first value in kernel_decay
    # [0] = timestamp, [1] = amplitude
    if neuron[0][0] == 0:
        dest[0] = neuron[0][1]
        amplitude_index = 1
    else:
        dest[0] = 0
        amplitude_index = 0

    aux = 0
    for i in range(1, no_values):
        # check if at time i the neuron had an aplitude
        if neuron[amplitude_index][0] == i:
            aux = aux + neuron[amplitude_index][1]

            dest[i] = neuron[amplitude_index][1]

            amplitude_index += 1
            if amplitude_index == len(neuron):
                amplitude_index = 0

        else:
            aux = aux * pow(math.e, -1/tau)
            if aux < 0.1:
                dest[i] = 0
                aux = 0.0
            else:
                dest[i] = dest[i-1]

'''
Parameters:     - trial_data: list of neurons for a trial. Every neuron should have a list of tuples (timestamp, amplitude)
                - no_values: the number of values in the trial after aplying decay
                - tay: value used for kenel decay
                - kernel_function: the function that maps the values

Return:         - 2D array where:
                    - Dimension 1 = number of neurons
                    - Dimension 2 = number of values per neuron

# NOTE: The timestamps of the neurons should be in the range(0, no_values),
    no_values and the timestamps need to be in the same time space, same unit
'''
def apply_kernel_function_trial(trial_channels, no_values, tau, kernel_function=apply_kernel_decay_neuron):
    no_neurons = len(trial_channels)
    # we need to create no_values for each neuron
    kernel_decay_trial = np.empty((no_neurons, no_values), dtype=np.float32)
    for i in range(no_neurons):
        # take neuron, apply kernel, the values will be pull in the finale kernel matrix
        kernel_function(trial_channels[i], no_values, tau, kernel_decay_trial[i])

    return kernel_decay_trial


'''
Parameters:     - values: numpy array
                - new_max: max value of the new interval
                - new_min: min value of the new interval

Return:         - values with new interval

Description:    - will convert INPLACE values from current interval (max, min) to a new
                    interval
'''
def rescale(values, new_max=0.9, new_min=0.0):
    maxim = np.amax(values)
    minim = np.amin(values)

    values = ((values - minim) * (new_max - new_min) / (maxim - minim)) + new_min

    return values
