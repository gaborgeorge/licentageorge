import numpy as np
from numba import njit

'''
Parameters:  - value    = value to be scaled
             - old_min  = the lowest value of the current interval
             - old_max  = the highest value of the current interval
             - new_min  = the lowest value of the new interval
             - new_max  = the highest value of the new interval

Return:      - the value resulted after conferting to [new_min, new_max]

Description: - takes --value-- (a value in [old_min, old_max]) and
                scales it to [new_min, new_max]
             - keepts the type of input
'''
@njit
def scale(value, old_min, old_max, new_min, new_max):
    return ((value - old_min) * (new_max - new_min)) / (old_max - old_min) + new_min


'''
Parameters:  - value    = value to be scaled
             - old_min  = the lowest value of the current interval
             - old_max  = the highest value of the current interval
             - new_min  = the lowest value of the new interval
             - new_max  = the highest value of the new interval

Return:      - the value resulted after conferting to [new_min, new_max]

Description: - takes --value-- (a value in [old_min, old_max]) and
                scales it to [new_min, new_max]
             - keepts the type of input
'''
@njit
def scale_to_int(value, old_min, old_max, new_min, new_max):
    return int(((value - old_min) * (new_max - new_min)) / (old_max - old_min) + new_min)


'''
Parameters:     - values    = 1D array of values in range[0, 100] to be converted to colors
                - colors    = 2D array to put the colors for every value in values
                                - Dimension 1 = number of values
                                - Dimension 2 = 3 (number of color channels)
Result:         - does not return anything

Description:    - Look in notebook, it follows a JET distribution of colors, aproximative
'''

@njit
def to_colormap(values, colors):
    __color_steps = [0.0, 12.5, 37.5, 62.5, 87.5, 100]
    for i in range(values.shape[0]):

        # find in what case the current value is
        value = values[i]

        # case 1 [0 - 12.5)
        if value < __color_steps[1]:
            colors[i, 0] = scale_to_int(value, __color_steps[0], __color_steps[1], 128, 255)
            colors[i, 1] = 0
            colors[i, 2] = 0
        # case 2 [12.5 - 37.5)
        elif value < __color_steps[2]:
            colors[i, 0] = 255
            colors[i, 1] = scale_to_int(value, __color_steps[1], __color_steps[2], 0, 255)
            colors[i, 2] = 0
        # case 3 [37.5 - 62.5)
        elif value < __color_steps[3]:
            colors[i, 0] = scale_to_int(value, __color_steps[2], __color_steps[3], 255, 0)
            colors[i, 1] = 255
            colors[i, 2] = scale_to_int(value, __color_steps[2], __color_steps[3], 0, 255)
        # case 4 [62.5 - 87.5)
        elif value < __color_steps[4]:
            colors[i, 0] = 0
            colors[i, 1] = scale_to_int(value, __color_steps[3], __color_steps[4], 255, 0)
            colors[i, 2] = 255
        # case 5 [87.5 - 100]
        else:
            colors[i, 0] = 0
            colors[i, 1] = 0
            colors[i, 2] = scale_to_int(value, __color_steps[4], __color_steps[5], 255, 128)


'''
Parameters:     - values    = np array
Return:         - np array of same shape as values with an aditional dimension of size 3 for the colors
'''
def colormap(values):
    # find interval extremities
    min = values.min()
    max = values.max()

    values_shape = values.shape
    values = np.ravel(values)

    # convert values in 0-100
    values = scale(values, min, max, 0, 100)

    # create the color array
    colors = np.empty([values.shape[0], 3], dtype=np.uint8)
    to_colormap(values, colors)

    colors = np.reshape(colors, values_shape + (3, ))

    return colors
