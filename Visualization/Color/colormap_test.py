import numpy as np
import matplotlib.pyplot as plt
import matplotlib

import colormap


def test_scale():

    result = colormap.scale(100, 0, 100, 0, 100)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(100, 0, 100, result, 0, 100))

    result = colormap.scale(50, 0, 100, 0, 200)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(50, 0, 100, result, 0, 200))

    result = colormap.scale(100, 0, 200, 100, 200)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(100, 0, 200, result, 100, 200))

    result = colormap.scale(0.5, 0.0, 1.0, 1.4, 1.5)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(0.5, 0.0, 1.0, result, 1.4, 1.5))

    result = colormap.scale(50, 100, 0, 0, 10)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(50, 100, 0, result, 0, 10))

    result = colormap.scale(50, 0, 100, 10, 3)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(50, 0, 100, result, 10, 3))

    result = colormap.scale(500, 0, 100, 10, 3)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(50, 0, 100, result, 10, 3))


def test_scale_to_int():
    result = colormap.scale_to_int(0.5, 0.0, 1.0, 1.4, 1.5)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(0.5, 0.0, 1.0, result, 1.4, 1.5))

    result = colormap.scale_to_int(50, 0, 100, 10.0, 3.5)
    print('Old {0} [{1}-{2}] New {3} [{4} - {5}]'.format(50, 0, 100, result, 10.0, 3.5))


def test_colormap():
    a = np.arange(1000)

    colors = colormap.colormap(a)

    _, ax = plt.subplots()

    colors = colormap.scale(colors, colors.min(), colors.max(), 0.0, 1.0)

    for i in range(colors.shape[0]):
        ax.add_patch(matplotlib.patches.Rectangle((i, 20), 1, 10, color=[colors[i, 2], colors[i, 1], colors[i, 0]]))

    plt.xlim(0, 1000)
    plt.ylim(0, 50)
    plt.show()


if __name__ == '__main__':
    test_colormap()
