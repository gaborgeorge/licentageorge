import os
import sys
sys.path.append(os.path.join('..', '..'))

import numpy as np

from numba import njit

from Classification.SOM.SOM_dataset_builder import build_color_dataset
from Classification.SOM.SOM_dataset_builder import build_histogram_datasets_amp_vs_mean
from Utils.info_gain import information_gains
from Visualization.Plots_cv import plot_bands_cv
from Visualization.Color.colormap import colormap
from Visualization.SOM.SOM_projections_utils import get_diff_projections

'''
Parameters: colors = list of array of colors
               info_gains = information_gain for every color
               desc = description of how to colors were made
'''
def save_info_gain_file(colors, info_gains, desc):
    with open(os.path.join('Results', desc + '.txt'), 'w') as f:

        for i in range(colors.shape[0]):

            f.write('%s - %s\n' % (str(colors[i]), str(info_gains[i])))

'''
Parameters: x - array of information gain values
            y - arrya of colors coresponding to every info gain value
# NOTE: this will sort the colors after the X array
'''
def sort_mirror(x, y):

    for i in range(x.shape[0]):
        min_idx = i

        for j in range(i+1, x.shape[0]):
            if x[j] < x[min_idx]:
                min_idx = j

        temp = x[min_idx]
        x[min_idx] = x[i]
        x[i] = temp

        temp = y[min_idx, 0]
        y[min_idx, 0] = y[i, 0]
        y[i, 0] = temp

        temp = y[min_idx, 1]
        y[min_idx, 1] = y[i, 1]
        y[i, 1] = temp

        temp = y[min_idx, 2]
        y[min_idx, 2] = y[i, 2]
        y[i, 2] = temp


'''
Parameters: color_cnt = number of colors to generate
            color_inc = the increment to the next color in a channel
'''
def make_colors(color_cnt, color_inc):
    colors = np.empty((color_cnt, 3), dtype=np.uint32)

    c_channel_0 = 0
    c_channel_1 = 0
    c_channel_2 = 0

    for i in range(color_cnt):
        colors[i, 0] = c_channel_0
        colors[i, 1] = c_channel_1
        colors[i, 2] = c_channel_2

        # update pattern color
        c_channel_2 += color_inc
        if c_channel_2 > 255:
            c_channel_2 = 0
            c_channel_1 += color_inc

        if c_channel_1 > 255:
            c_channel_1 = 0
            c_channel_0 += color_inc

    return colors


'''
Parameters: band - 2D array of colors
            gains_jet - information gains values turned into jet color values
            colors - for every gains_jet its color
            color_width - how many pixels in width should a color have in the band
            color_height - how many pixels in heigth should a color have in the band
            color_inc - increment for the channels of the colors
'''
def make_gain_band(band, gains_jet, colors, color_width, color_height, color_inc):

    row = 0
    column = 0

    for x in range(colors.shape[0]):

        for i in range(color_height):
            for j in range(color_width):

                r = row + i
                c = column + j
                band[r, c, 0] = colors[x, 0] # make it
                band[r, c, 1] = colors[x, 1] # make it
                band[r, c, 2] = colors[x, 2] # make it

        row = color_height

        for i in range(color_height):
            for j in range(color_width):

                r = row + i
                c = column + j
                band[r, c, 0] = gains_jet[x, 0]
                band[r, c, 1] = gains_jet[x, 1]
                band[r, c, 2] = gains_jet[x, 2]

        row = 0
        column += color_width


'''
Parameters: bands = 2D arrays of colors representing the result of the SOM alg
            color_inc = same as before
            info_gains = oen or two arrays of colors, each to add a new band
            color_count = number of colors
            desc = colors description
'''
def add_info_gains_bands(bands, color_inc, info_gains_o, color_count, desc):
    info_gains = np.copy(info_gains_o)
    band_size = (bands.shape[1], bands.shape[2])

    colors = make_colors(color_count, color_inc)
    sort_mirror(info_gains, colors)

    save_info_gain_file(colors, info_gains, desc)

    gains_jet = colormap(info_gains)

    color_width = band_size[1] // color_count
    color_height = band_size[0] // 2


    if info_gains.ndim == 2:

        info_gains_bands = np.empty((2, band_size[0], band_size[1]), dtype=np.uint32)
        make_gain_band(info_gains_bands[0], gains_jet[0], colors, color_width, color_height, color_inc)
        make_gain_band(info_gains_bands[1], gains_jet[1], colors, color_width, color_height, color_inc)

    elif info_gains.ndim == 1:
        info_gains_bands = np.full((1, band_size[0], band_size[1], 3), 255, dtype=np.uint32)
        make_gain_band(info_gains_bands[0], gains_jet, colors, color_width, color_height, color_inc)

    else:
        return None

    return np.vstack((bands, info_gains_bands))


def plot_info_gain_with_bands(size, tau, contrast, use_mean_neuron=False, use_z_score=False, use_fisher=False, folder='Results'):

    X, Y, color_trials, color_inc, desc = build_color_dataset(size, tau, contrast,
                                                                            use_mean_neuron=use_mean_neuron,
                                                                            use_z_score=use_z_score,
                                                                            use_fisher=use_fisher,
                                                                            path=os.path.join('..', '..'))

    gains = information_gains(X, Y)

    bands = add_info_gains_bands(color_trials, color_inc, gains, size[0] * size[1] * size[2], desc)

    orientations_values = [0, 45, 90, 135, 180, 225, 270, 315, 'gain']
    plot = plot_bands_cv(bands, orientations_values, color_height=10, color_width=1, events=(0, 2700, 3100),
                        title='', save=True, save_name=os.path.join(folder, desc + '.png'))

def plot_info_gain_with_bands_merged(size, tau, contrast, use_mean_neuron=True, use_z_score=False, use_fisher=False, folder='Results'):

    X1, X2, Y1, Y2, color_trials, color_inc, desc = build_histogram_datasets_amp_vs_mean(size, tau, contrast,
                                                                            use_mean_neuron=use_mean_neuron,
                                                                            use_z_score=use_z_score,
                                                                            use_fisher=use_fisher,
                                                                            path=os.path.join('..', '..'))
    print(X1.shape, X2.shape, Y1.shape, Y2.shape)
    gains1 = information_gains(X1, Y1)
    gains2 = information_gains(X2, Y2)

    color_trials = get_diff_projections(color_trials)
    desc += '_Diff'

    bands = add_info_gains_bands(color_trials, color_inc, gains1, size[0] * size[1] * size[2], desc + '_amplitudes')
    bands = add_info_gains_bands(bands, color_inc, gains2, size[0] * size[1] * size[2], desc + '_mean')

    orientations_values = [0, 0, 0, 45, 45, 45, 90, 90, 90, 135, 135, 135, 180, 180, 180, 225, 225, 225, 270, 270, 270, 315, 315, 315, 'gain', 'gain']
    plot = plot_bands_cv(bands, orientations_values, color_height=10, color_width=1, events=(0 ,2700, 3100),
                        title=desc, save=True, save_name=os.path.join(folder, desc + '.png'))

def plots_1():
    plot_info_gain_with_bands_merged((5, 5, 5), 5, 100)
    '''plot_info_gain_with_bands_merged((5, 5, 5), 10, 100)
    plot_info_gain_with_bands_merged((5, 5, 5), 20, 100)
    plot_info_gain_with_bands_merged((5, 5, 5), 20, 100, use_z_score=True)
    plot_info_gain_with_bands_merged((5, 5, 5), 20, 100, use_fisher=True)
    plot_info_gain_with_bands_merged((5, 5, 5), 50, 100)
    plot_info_gain_with_bands_merged((7, 7, 7), 5, 100)
    plot_info_gain_with_bands_merged((7, 7, 7), 10, 100)
    plot_info_gain_with_bands_merged((7, 7, 7), 20, 100)
    plot_info_gain_with_bands_merged((7, 7, 7), 20, 100, use_z_score=True)
    plot_info_gain_with_bands_merged((7, 7, 7), 20, 100, use_fisher=True)
    plot_info_gain_with_bands_merged((7, 7, 7), 50, 100)
    plot_info_gain_with_bands_merged((10, 10, 10), 5, 100)
    plot_info_gain_with_bands_merged((10, 10, 10), 10, 100)
    plot_info_gain_with_bands_merged((10, 10, 10), 20, 100)
    plot_info_gain_with_bands_merged((10, 10, 10), 20, 100, use_z_score=True)
    plot_info_gain_with_bands_merged((10, 10, 10), 20, 100, use_fisher=True)
    plot_info_gain_with_bands_merged((10, 10, 10), 50, 100)
    '''

if __name__ == '__main__':
    plots_1()
