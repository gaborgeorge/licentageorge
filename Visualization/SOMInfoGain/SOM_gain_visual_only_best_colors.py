import os
import sys
sys.path.append(os.path.join('..', '..'))

import numpy as np

from numba import njit

from Utils.info_gain import information_gains
from Visualization.SOMInfoGain.SOM_gain_visual import make_colors, sort_mirror, add_info_gains_bands
from Classification.SOM.SOM_dataset_builder import build_color_dataset
from Classification.SOM.SOM_dataset_builder import build_histogram_datasets_amp_vs_mean
from Visualization.Plots_cv import plot_bands_cv
from Visualization.Plots_cv import combine_plots
from Visualization.SOM.SOM_projections_utils import get_diff_projections

from config import data_dir, ssd_file, result_file, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END, mili_between_ON_OFF, mili_between_ON_END


def red_bigger_120(color):
    if color[2] > 120:
        return True
    return False

def gain_bigger(gain, max_gain, percent):
    if gain > max_gain * percent:
        return True
    return False

@njit
def update_non_important_colors(bands, important_colors):
        for t in range(bands.shape[0]):
            for c in range(bands.shape[1]):
                # now we are at color level
                ok = False
                for c_i in range(important_colors.shape[0]):
                    if bands[t, c, 0] == important_colors[c_i, 0] and \
                       bands[t, c, 1] == important_colors[c_i, 1] and \
                       bands[t, c, 2] == important_colors[c_i, 2]:
                        ok = True
                        break
                if ok is False:
                    bands[t, c, 0] = 0
                    bands[t, c, 1] = 0
                    bands[t, c, 2] = 0


def leave_only_important_colors(color_trials, color_inc, info_gains, color_count, percent):
    colors = make_colors(color_count, color_inc)

    sort_mirror(info_gains, colors)

    # pick only the 'important' colors
    # NOTE: important may mean different things, this is just like a filter, multiple conditions may be used
    # NOTE: we don't know how many important colors may be
    important_colors = []
    print(info_gains)
    for i in range(colors.shape[0]):
        if gain_bigger(info_gains[i], info_gains.max(), percent) is True:
            important_colors.append(colors[i])

    important_colors = np.array(important_colors)
    print('[{0}] Inportant colors {1}, count {2}'.format(sys._getframe().f_code.co_name, important_colors, important_colors.shape[0]))

    if color_trials.ndim == 4:
        for i in range(color_trials.shape[0]):
            update_non_important_colors(color_trials[i], important_colors)
    elif color_trials.ndim == 3:
        update_non_important_colors(color_trials, important_colors)


def plot_info_gain_with_bands_important_colors(
        size,
        tau,
        contrast,
        use_mean_neuron=False,
        use_z_score=False,
        use_fisher=False,
        percent=0.9,
        use_start=True,
        use_end=True,
        folder='Results'):

    X, Y, color_trials, color_inc, desc = build_color_dataset(size, tau, contrast,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        path=os.path.join('..', '..'),
                                                        use_start=use_start,
                                                        use_end=use_end)

    gains = information_gains(X, Y)
    desc += '_Important_colors_' + str(percent)
    print(gains)
    gains1 = np.copy(gains)
    leave_only_important_colors(color_trials, color_inc, gains1, size[0] * size[1] * size[2], percent)

    bands = add_info_gains_bands(color_trials, color_inc, gains, size[0] * size[1] * size[2], desc)

    orientations_values = [0, 45, 90, 135, 180, 225, 270, 315, 'gain']

    if use_start is False and use_end is False:
        events = [0, mili_between_ON_OFF]
    elif use_start is True and use_end is False:
        events = [0, mili_between_START_ON, mili_between_START_OFF]
    elif use_start is False and use_end is True:
        events = [0, mili_between_ON_OFF, mili_between_ON_END]
    else:
        events = [0, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END]

    plot = plot_bands_cv(bands, orientations_values, color_height=10, color_width=1, events=events,
                        title='', save=True, save_name=os.path.join(folder, desc + '.png'))

def leave_only_important_colors_merged(color_trials, color_inc, i_gains1, i_gains2, color_count, percent):
    colors1 = make_colors(color_count, color_inc)
    colors2 = make_colors(color_count, color_inc)

    gains1 = np.copy(i_gains1)
    gains2 = np.copy(i_gains2)

    sort_mirror(gains1, colors1)
    sort_mirror(gains2, colors2)


    # pick only the 'important' colors
    # NOTE: important may mean different things, this is just like a filter, multiple conditions may be used
    # NOTE: we don't know how many important colors may be
    important_colors1 = []
    for i in range(colors1.shape[0]):
        if gain_bigger(gains1[i], gains1.max(), percent) is True:
            important_colors1.append(colors1[i])

    important_colors2 = []
    for i in range(colors2.shape[0]):
        if gain_bigger(gains2[i], gains2.max(), percent) is True:
            important_colors2.append(colors2[i])

    important_colors1 = np.array(important_colors1)
    important_colors2 = np.array(important_colors2)

    for i in range(color_trials.shape[0]):
        if i % 2 == 0:
            update_non_important_colors(color_trials[i], important_colors1)
        else:
            update_non_important_colors(color_trials[i], important_colors2)

'''
Parameters: size               - number of centroids for SOM
            tau                - integration constant value
            contrast           - contrast value
            use_mean_neuron    - use mean amplitude per channel for spike value
            use_z_score        - if it should apply z-score
            percent            - the minimum amount of info gain to consider
            use_start          - if it should consider the START event
            use_end            - if it should consider the END event
            foder              - where to save the figures
'''
def plot_info_gain_with_bands_important_colors_merged(
                size,
                tau,
                contrast,
                use_mean_neuron=False,
                use_z_score=False,
                use_fisher=False,
                percent=0.9,
                use_start=False,
                use_end=True,
                folder='Results'):
    X1, X2, Y1, Y2, color_trials, color_inc, desc = build_histogram_datasets_amp_vs_mean(size, tau, contrast,
                                                                            use_mean_neuron=use_mean_neuron,
                                                                            use_z_score=use_z_score,
                                                                            use_fisher=use_fisher,
                                                                            use_start=use_start,
                                                                            use_end=use_end,
                                                                            path=os.path.join('..', '..'))
    gains1 = information_gains(X1, Y1)
    gains2 = information_gains(X2, Y2)


    if use_start is False and use_end is False:
        events = [0, mili_between_ON_OFF]
    elif use_start is True and use_end is False:
        events = [0, mili_between_START_ON, mili_between_START_OFF]
    elif use_start is False and use_end is True:
        events = [0, mili_between_ON_OFF, mili_between_ON_END]
    else:
        events = [0, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END]

    desc += '_Important_colors_CONSTANT3' + str(percent)

    original_bands = np.copy(color_trials)

    leave_only_important_colors_merged(color_trials, color_inc, gains1, gains2, size[0] * size[1] * size[2], percent)

    color_trials = get_diff_projections(color_trials)
    original_bands = get_diff_projections(original_bands)
    desc += '_Diff_CONSTANT'

    # important colors
    bands = add_info_gains_bands(color_trials, color_inc, gains1, size[0] * size[1] * size[2], desc + '_amplitudes')
    bands = add_info_gains_bands(bands, color_inc, gains2, size[0] * size[1] * size[2], desc + '_mean')

    # original colors
    original_bands = add_info_gains_bands(original_bands, color_inc, gains1, size[0] * size[1] * size[2], desc + '_amplitudes_ori')
    original_bands = add_info_gains_bands(original_bands, color_inc, gains2, size[0] * size[2] * size[2], desc + '_mean_ori')

    orientations_values = [0, 0, 0, 45, 45, 45, 90, 90, 90, 135, 135, 135, 180, 180, 180, 225, 225, 225, 270, 270, 270, 315, 315, 315, 'gain', 'gain']
    plot1 = plot_bands_cv(bands, orientations_values, color_height=10, color_width=1, events=events,
                        title=desc, save=False)
    plot2 = plot_bands_cv(original_bands, orientations_values, color_height=10, color_width=1, events=events,
                        title=desc + '_ori', save=False)

    combine_plots([plot1, plot2], save=True, save_file=os.path.join(folder, desc + '.png'))

if __name__ == '__main__':
    plot_info_gain_with_bands_important_colors_merged((5, 5, 5), 20, 100, use_mean_neuron=True, use_z_score=True, percent=0.3, use_start=False, use_end=True, folder='DEMO_RESULTS')
