import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

import numpy as np

from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
from DataSet.TrialExtractor import TrialExtractor
from Visualization.SOM.SOM_plots_generator import SOM_plot_merged, SOM_plot, plot_SOM_and_histograms, SOM_plot_merged_and_hist
from Visualization.PSTH.PSTH_heatmap_plot import plot_heatmap
from Visualization.Plots_cv import combine_plots
from config import data_dir, ssd_file, result_file


'''
Params: - extractor     = instance of TrialExtractor
        - som_size      = tuple of form (width, height, depth) for the SOM alg
        - tau           = integration constant used in SOM
        - som_title     = the title of the SOM figure
        - window_size   = number of consecutive timestamps consider for a window
        - window_inc    = the step of the window, in number of values
        - heatmap_title = title of the heatmap figure
        - save          = if it should save the image
        - save_file     = the file where to save the image

Return: - resulted image by combining the SOM and heatmap
'''
def SOM_heapmap_plot(extractor, som_size, tau, som_title, window_size, window_inc, heatmap_title, save=False, save_file=''):
    heatmap_plot = plot_heatmap(extractor, window_size, window_inc,
                    title=heatmap_title , save=False)
    som_plot = SOM_plot(extractor, tau, som_size,
                    title=som_title, save=False)

    img = combine_plots([som_plot, heatmap_plot], save=save,
                    save_file=save_file)

    return img


'''
Params: - extractor1    = instance of TrialExtractor
        - extractor2    = instance of TrialExtractor
        - som_size      = tuple of form (width, height, depth) for the SOM alg
        - tau           = integration constant used in SOM
        - use_diff      = if it should display a new band every two SOM bands
                            that is the difference of the two above
        - som_title     = the title of the SOM figure
        - window_size   = number of consecutive timestamps consider for a window
        - window_inc    = the step of the window, in number of values
        - heatmap_title = title of the heatmap figure
        - save          = if it should save the image
        - save_file     = the file where to save the image

Return: - resulted image by combining the SOM and heatmap
'''
def SOM_merge_heapmap_plot(extractor1, extractor2, som_size, tau, use_diff, som_title, window_size, window_inc, heatmap_title, save=False, save_file=''):
    heatmap_plot = plot_heatmap(extractor1, window_size, window_inc,
                    title=heatmap_title , save=False)
    som_plot = SOM_plot_merged(extractor1, extractor2, tau, som_size, use_diff=use_diff,
                    title=som_title, save=False)

    img = combine_plots([som_plot, heatmap_plot], save=save,
                    save_file=save_file)

    return img


def SOM_hist_heatmap_plot(extractor1, extractor2, som_size, tau, som_title, window_size, window_inc, heatmap_title, hist_title, hist_save_file, SOM_heatmap_save_file):
    heatmap_plot = plot_heatmap(extractor1, window_size, window_inc,
                    title=heatmap_title, save=False)
    som_plot = SOM_plot_merged_and_hist(extractor1, extractor2, tau, som_size,
                    SOM_title=som_title, SOM_save=False, hist_title=hist_title,
                    hist_save=True, hist_save_file=hist_save_file)

    img = combine_plots([som_plot, heatmap_plot], save=True,
                    save_file=SOM_heatmap_save_file)


kohonen_size = [(5, 5, 5)]
tau_values = [20]
window_sizes = [10]
window_incs = [1]

# element 0 = kohonen size
# element 1 = tau value
# element 2 = window_size
# element 3 = window_inc
plot_configuration = [((5, 5, 5), 20, 20, 1),
                    ((5, 5, 5), 30, 30, 1),
                    ((5, 5, 5), 50, 50, 1),
                    ((5, 5, 5), 100, 100, 1),
                    ((7, 7, 7), 20, 20, 1),
                    ((7, 7, 7), 30, 30, 1),
                    ((7, 7, 7), 50, 50, 1),
                    ((7, 7, 7), 100, 100, 1),
                    ((10, 10, 10), 20, 20, 1),
                    ((10, 10, 10), 30, 30, 1),
                    ((10, 10, 10), 50, 50, 1),
                    ((10, 10, 10), 100, 100, 1)]

def SOM_heapmap_plot_generator(hist=True, merge=True, use_mean=False, use_diff=False, use_z_score=False, desc=''):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader, use_mean_neuron=False, use_z_score=use_z_score, use_start=False, use_end=True)
    trial_extractor_mean_neurons = TrialExtractor(spike_reader, trial_meta_reader, use_mean_neuron=True, use_z_score=use_z_score, use_start=False, use_end=True)

    for conf in plot_configuration:
        som_size = conf[0]
        tau = conf[1]
        window_size = conf[2]
        window_step = conf[3]
        if hist is True:
            SOM_hist_heatmap_plot(trial_extractor_amplitudes, trial_extractor_mean_neurons,
                        som_size=som_size, tau=tau, som_title='SOM size=' + str(som_size) + ' Tau=' + str(tau) + ' First with Amplitude ' + desc,
                        window_size=window_size, window_inc=window_step, heatmap_title='HEATMAP window size ' + str(window_size) + ' window increment ' + str(window_step),
                        hist_title='Distances - SOM size=' + str(som_size) + ' Tau=' + str(tau) + ' Distances between amplitude colors and mean colors',
                        hist_save_file=os.path.join('Results', 'Histogram_SOM_size_' + str(som_size) + '_tau_' + str(tau) + '_' + desc + '.png'),
                        SOM_heatmap_save_file=os.path.join('Results', 'SOM_MERGE_size_' + str(som_size) + '_tau_' + str(tau) + '_Heatmap_window_size_' + str(window_size) + '_window_inc_' + str(window_step) + '_' + desc + '.png'))
        elif merge is True:
            SOM_merge_heapmap_plot(trial_extractor_amplitudes, trial_extractor_mean_neurons, save=True,
                         som_size=som_size, use_diff=use_diff, tau=tau, som_title='SOM size=' + str(som_size) + ' Tau=' + str(tau) + ' First with Amplitude ' + desc,
                         window_size=window_size, window_inc=window_step, heatmap_title='HEATMAP window size ' + str(window_size) + ' window increment ' + str(window_step),
                         save_file=os.path.join('Results', 'SOM_MERGE_size_' + str(som_size) + '_tau_' + str(tau) + '_Heatmap_window_size_' + str(window_size) + '_window_inc_' + str(window_step) + '_' + desc + '.png'))
        else:
            SOM_heapmap_plot(trial_extractor_amplitudes, som_size=som_size, tau=tau, som_title='SOM size=' + str(som_size) + ' Tau=' + str(tau) + ' First with Amplitude ' + desc,
                         window_size=window_size, window_inc=window_step, heatmap_title='HEATMAP window size ' + str(window_size) + ' window increment ' + str(window_step),
                         save=True, save_file=os.path.join('Results', 'SOM_size_' + str(som_size) + '_tau_' + str(tau) + '_Heatmap_window_size_' + str(window_size) + '_window_inc_' + str(window_step) + '_' + desc + '.png'))


if __name__ == '__main__':
    SOM_heapmap_plot_generator(hist=True, merge=True, use_mean=False, use_diff=True, use_z_score=True, desc='Use_z_score')
