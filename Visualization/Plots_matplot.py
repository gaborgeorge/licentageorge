import numpy as np
import matplotlib.pyplot as plt
import math
import os
import sys
project_path = os.path.join('..')
sys.path.append(project_path)

from math import exp, expm1
from matplotlib import collections as matcoll

from Utils.KernelDecay import rescale
from Utils.KernelDecay import apply_kernel_function_trial, apply_kernel_zeros_neuron, apply_kernel_decay_neuron
from DataSet.TrialExtractor import TrialExtractor
from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
from config import data_dir, ssd_file, result_file


'''
Parameters: - trial: 2D array where:
                    - Dimension 1 = number of neurons in trial
                    - Dimension 2 = number of values (in time) for a neuron
            - event_times: list of x values where vertical lines will be drawn
            - show: if it should show to image or not
            - save_name: the file in which the image will be saved

Return:     - None

# NOTE: this will always save the image

Description: - every non-zero value of a neuron will be drawn as a vertical line
'''
def show_amplitudes(trial, event_times, show=False, save_name='amplitudes.png'):
    neurons = rescale(trial)

    lines = []

    print('Plot has', neurons.shape[0], 'lines')
    # each neuron will have a row
    for row in range(neurons.shape[0]):
        # add line for neuron
        lines.append(((0, row), (neurons.shape[1], row)))

        # each amplitude corresponds to a unit in time
        for time in range(neurons[row].shape[0]):
            if neurons[row][time] != 0.0:
                lines.append(((time, row), (time, row + neurons[row][time])))

    # add event lines
    for time in event_times:
        lines.append(((time, 0), (time, neurons.shape[0])))

    linecoll = matcoll.LineCollection(lines)
    fig, ax = plt.subplots()
    ax.add_collection(linecoll)

    plt.title('Amplitudes values of spikes of neurons(y axis) in time(x axis)', fontsize=14)

    plt.xlabel('Time (ms)')
    plt.ylabel('Neurons with Amplitudes')

    # Set plot limits
    plt.ylim(0, neurons.shape[0])
    plt.xlim(0, neurons.shape[1])

    fig.savefig(save_name, dpi = 300)

    if show is True:
        manager = plt.get_current_fig_manager()
        manager.resize(*manager.window.maxsize())
        plt.show()

    print('[{0}] COMPLETE - saved in {1}'.format(sys._getframe().f_code.co_name, os.path.abspath(save_name)))


'''
Parameters: - trial: 2D array where:
                    - Dimension 1 = number of neurons in trial
                    - Dimension 2 = number of values (in time) for a neuron
            - event_times: list of x values where vertical lines will be drawn
            - show: if it should show to image or not
            - save_name: the file in which the image will be saved

Return:     - None

# NOTE: this will always save the image

Description: - every 2 consecutive non-zero values will be united by a line
'''
def show_amplitude_decays(trial, event_times, show=False, save_name='kernel_decay.png'):
    neurons = rescale(trial)
    lines = []

    print('Plot has', neurons.shape[0], 'lines')

    for row in range(neurons.shape[0]):
        # add line for neuron
        lines.append(((0, row), (neurons.shape[1], row)))
        for time in range(1, neurons[row].shape[0]):
            if neurons[row][time] > 0.0001 or neurons[row][time - 1] > 0.0001:
                point1 = (time - 1, row + neurons[row][time - 1])
                point2 = (time, row + neurons[row][time])
                lines.append((point1, point2))

    # add event lines
    for time in event_times:
        lines.append(((time, 0), (time, neurons.shape[0])))

    linecoll = matcoll.LineCollection(lines)
    fig, ax = plt.subplots()
    ax.add_collection(linecoll)

    plt.title('Amplitudes values of spikes of neurons(y axis) in time(x axis)', fontsize=14)

    plt.xlabel('Time (ms)')
    plt.ylabel('Neurons with Amplitudes (decaing)')

    # Set plot limits
    plt.ylim(0, neurons.shape[0])
    plt.xlim(0, neurons.shape[1])

    fig.savefig(save_name, dpi = 300)

    if show is True:
        manager = plt.get_current_fig_manager()
        manager.resize(*manager.window.maxsize())
        plt.show()

    print('[{0}] COMPLETE - saved in {1}'.format(sys._getframe().f_code.co_name, os.path.abspath(save_name)))


'''
Parameters: - values    = values for the histogram
            - show      = if to show the histogram on the screen
            - save      = if it should save the histogram to a file
            - save_file = the file in witch to save the histogram, save must be
                            True

Return:    - None
'''
def plot_histogram(values, nr_bins=20, title='', show=False, save=False, save_file=''):
    #print(nr_bins)
    if nr_bins == 0:
        nr_bins = 1
    plt.hist(values, nr_bins, facecolor='blue', alpha=0.5)

    plt.title(title, fontsize=14)

    if save is True:
        plt.savefig(save_file)

    if show is True:
        plt.show()

    plt.close()

def plot_bar(X, title='', show=False, save=False, save_file=''):
    plt.bar(np.arange(X.shape[0]), X)

    plt.title(title, fontsize=14)

    if save is True:
        plt.savefig(save_file)

    if show is True:
        plt.show()

    plt.close()

def plot_connected_lines(X, Y1, Y2, x_label='X', y_label='Y', plot_1_label='1', plot_2_label='2', title='', show=False, save=False, save_file=''):
    plt.plot(X, Y1, 'r', label=plot_1_label)
    plt.plot(X, Y2, 'b', label=plot_2_label)

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='lower right')

    plt.title(title, fontsize=14)

    if save is True:
        plt.savefig(save_file)

    if show is True:
        plt.show()

    plt.close()

def plot_connected_lines_simple(X, Y, x_label='X', y_label='Y', plot_1_label='1', plot_2_label='2', title='', show=False, save=False, save_file=''):
    plt.plot(X, Y)#, 'r', label=plot_1_label)

    plt.xlabel(x_label)
    plt.ylabel(y_label)
    plt.legend(loc='lower right')
    plt.ylim(0.0, 1.0)

    plt.title(title, fontsize=14)

    if save is True:
        plt.savefig(save_file)

    if show is True:
        plt.show()

    plt.close()

def plot_bar_chart(X, Y, x_label='X', y_label='Y', title='', show=False, save=False, save_file=''):
    plt.bar(X, Y)

    plt.xlabel(x_label)
    plt.ylabel(y_label)

    plt.ylim(0.0, 1.0)

    plt.title(title)
    if save is True:
        plt.savefig(save_file)

    if show is True:
        plt.show()

    plt.close()

def show_trials():
    data_dir_1 = os.path.join(project_path, data_dir)
    trials = TrialExtractor(SpikeTimestampReader(data_dir_1, ssd_file), TrialMetadataReader(os.path.join(data_dir_1, result_file)), use_start=True)
    print(trials.trials[2]['events_timestamps'])
    trials.spike_reader.metadata.print_metadata()

    '''
    for trial in trials.trials:
        events_time = trial['events_timestamps']
        print(events_time['end'] - events_time['off'])


    ok_0 = False
    ok_45 = False
    for trial_all in trials.trials:
        if trial_all['meta']['direction'] == 90 and ok_0 is False:
            trial = trial_all['spikes']
            trial_kernel = apply_kernel_function_trial(trial, 4200, 20, apply_kernel_zeros_neuron)
            print(np.transpose(trial_kernel).shape)
            show_amplitudes(trial_kernel, events_time.values(), show=True)
            ok_0 = True

            trial_kernel_decay = apply_kernel_function_trial(trial, 4200, 20, apply_kernel_decay_neuron)
            show_amplitude_decays(trial_kernel_decay, events_time.values(), show=True)
    '''

def scale(values, new_min, new_max):
    min = values.min()
    max = values.max()

    values = (values - min) * (new_max - new_min) / (max - min) + new_min

    return values

def test_fisher():
    values1 = np.random.rand(1000)
    values2 = np.random.rand(700) / 2.0
    values3 = np.random.rand(500) / 4.0

    values = np.hstack([values1, values2, values3])
    min = values.min()
    max = values.max()
    values = scale(values, -0.9, 0.9)

    plot_histogram(values, show=True, save=False)

    values_fisher = np.arctanh(values)
    values_fisher = scale(values_fisher, min, max)
    plot_histogram(values_fisher, show=True, save=False)



if __name__ == '__main__':
    show_trials()
