import cv2 as cv
import numpy as np
import os
import time
import math
import sys

from numba import njit


'''
Parameters:     - img: 3D array (image) where:
                        - Dimension 1 = number of rows in image
                        - Dimension 2 = number of cols in image
                        - Dimesnion 3 = number of channels per color (usually 3)
                - orientaions: 4D array where:
                        - Dimension 1 = number of bands to be drawned
                        - Dimension 2 = number of rows in each band
                        - Dimesnion 3 = number of colors in each color
                        - Dimension 4 = number of channels per color
                - shpae: how many pixel rows should be skipped when drawing new
                            band
                - color_height: how many pixel rows a color should be
                - color_width: how many pixel cols a color should be

Result:         - None
'''
@njit
def color_img(img, orientations, space, color_height, color_width):
    offset = 0

    # orientations.shape[0] = nunber orientations (bands in img)
    # orientations.shape[1] = number of trials per orientations (rows in img)
    # orientations.shape[2] = number of values per trial (cols in img)
    # orientations.shape[3] = number of color channels (pixel color in img)

    for k in range(orientations.shape[0]):
        for i in range(orientations.shape[1]):
            for j in range(orientations.shape[2]):
                for h in range(color_height):
                    row_idx = color_height * (i + offset) + h
                    for w in range(color_width):
                        col_idx = color_width * j + w
                        img[row_idx, col_idx, 0] = orientations[k, i, j, 0]
                        img[row_idx, col_idx, 1] = orientations[k, i, j, 1]
                        img[row_idx, col_idx, 2] = orientations[k, i, j, 2]

        offset += orientations.shape[1] + space


# config values for plot_bands_cv
text_color = (0, 0, 0)
event_lines_color = (0, 0, 255)
backgournd_value = 255
x_axis_rows = 50
title_rows = 150
y_axis_cols = 100
bands_space = 3
value_x_inc = 500

'''
parameters: - bands_colors: a 3D or 4D array where:
                            - Dimension 1 = number of bands
                            - Dimension 2 = number of rows per band
                            - Dimension 3 = number of colors per band
                                    # NOTE: in case of the 3D array this will be the intensity values
                            - Dimension 4 = number of channels per color
                                    # NOTE: for 4D array
            - y_values: the values for the y axis, one for each band
            - color_height: number of pixel rows for a color
            - color_width: number of pixel cols for a color
            - color_funct: a function uses to put the colors for the bands
            - events: x coordonates that will create vertical lines
            - title: title of image
            - save: to save or not the image
            - save_name: name of the file in which the picture will be saved

return: - img: the image formed
                # NOTE: This could be used to combine multiple images
'''
def plot_bands_cv(bands_colors, y_values, color_height, color_width, color_func=color_img, events=(0, 1002, 3674, 4175), title='test', save=True, save_name='test.png'):
    print('[{0}] STARTED'.format(sys._getframe().f_code.co_name))
    start = time.time()

    bands_rows = color_height * (bands_colors.shape[0] * (bands_colors.shape[1] + bands_space) - bands_space) # number orientations * (row in an orientation + space between them)
    bands_cols = color_width * bands_colors.shape[2]

    #print(bands_rows, bands_cols)

    # calc number of lines and coloms for img
    img_rows = bands_rows + x_axis_rows + title_rows
    img_cols = bands_cols + y_axis_cols

    # default img is white
    img = np.full((img_rows, img_cols, 3), backgournd_value, dtype=np.uint8)

    img_bands = img[title_rows:title_rows + bands_rows, y_axis_cols:] # only the orientations part of plot

    # draw event lines
    for event in events:
        cv.line(img_bands, (event, 0), (event, img_bands.shape[0]), event_lines_color, 5)

    # extract the region for the orientations and color it
    color_func(img_bands, bands_colors, bands_space, color_height, color_width)

    # write title, do (y, x)
    cv.putText(img, title, (y_axis_cols, title_rows//2), cv.FONT_HERSHEY_SIMPLEX, 2.0, text_color, thickness=2, lineType=cv.LINE_AA)

    # write x labels
    start_rows = title_rows + bands_rows
    start_cols = y_axis_cols
    values_x_axs = bands_colors.shape[2] # number values in trial
    start_value = 0

    for _ in range(values_x_axs // value_x_inc + 1):
        cv.putText(img, str(start_value), (start_cols, start_rows + 40), cv.FONT_HERSHEY_SIMPLEX, 1.0, text_color, thickness=2, lineType=cv.LINE_AA)
        start_value += value_x_inc
        start_cols += value_x_inc

    # write y labels
    start_x = y_axis_cols // 3
    inc_y = (bands_colors.shape[1] + bands_space) * color_height # rows for one orientation
    start_y = title_rows + inc_y // 2
    for i in range(bands_colors.shape[0]):
        cv.putText(img, str(y_values[i]), (start_x, start_y), cv.FONT_HERSHEY_SIMPLEX, 1.0, text_color, thickness=2, lineType=cv.LINE_AA)
        start_y += inc_y

    if save is True:
        cv.imwrite(save_name, img)
        print('[{0}] COMPLETE - saved in {1}, time {2}'.format(sys._getframe().f_code.co_name, os.path.abspath(save_name), time.time() - start))
    else:
        print('[{0}] COMPLETE - time {1}'.format(sys._getframe().f_code.co_name, time.time() - start))

    return img


'''
Parameters: - images: list of 3D arrays, each element representing an image, where
                    - Dimension 1 = number of rows
                    - Dimension 2 = number of cols
                    - Dimension 3 = number of channels per color (usually 3)
            - save_file: path to a file to be created where the images combined
                    will be saved

Result: - image resulted by combining the elements of images

Description: - the resulted image will be a grid where each element will be an
            image
             - the space in grid will be an array, its dimensions are computed
            based on the shapes of the input images as follows:
                    - the biggest number of rows will be the number of rows in
                        each space of the grid
                    - the biggest number of cols will be the number of cols in
                        each space of the grid

# NOTE: currently the len of the images list is not expected to be more that 4
        so no spacial optimization is currently required

# NOTE: currently I consider that it is not required to put more that 2 images
        on a row of the grid, the PURPOSE of this is to visualize in parallel
        images that are different but relate to the same thing

'''
def combine_plots(images, save=True, save_file=None):
    start = time.time()

    # verfiy number of channels
    assert images[0].shape[2] == 3

    max_rows = 0
    max_cols = 0

    # find the maxinum number of rows and maximum number of cols in images
    for image in images:
        if image.shape[0] > max_rows:
            max_rows = image.shape[0]
        if image.shape[1] > max_cols:
            max_cols = image.shape[1]

    # determine the sizes of the plot
    grid_cols = 2
    grid_rows = int(math.ceil(len(images) / 2))
    space_between_figures = 10

    plot_rows = grid_rows * max_rows + space_between_figures * (grid_rows + 1)
    plot_cols = grid_cols * max_cols + space_between_figures * (grid_cols + 1)

    # create the plot image
    plot = np.full((plot_rows, plot_cols, 3), 255, dtype=np.uint8)

    row = space_between_figures
    col = space_between_figures

    for i in range(grid_rows):
        for j in range(grid_cols):
            if i * grid_cols + j >= len(images):
                break
            row = space_between_figures + i * (space_between_figures + max_rows)
            col = space_between_figures + j * (space_between_figures + max_cols)

            image = images[i * grid_cols + j]
            plot[row:row + image.shape[0], col:col + image.shape[1]] = image

    if save is True:
        cv.imwrite(save_file, plot)
        print('[{0}] COMPLETE - saved in {1}, time {2}'.format(sys._getframe().f_code.co_name, os.path.abspath(save_file), time.time() - start))
    else:
        print('[{0}] COMPLETE - time {1}'.format(sys._getframe().f_code.co_name, time.time() - start))

    return plot

def test_same_size_2_imgs_combine_plots():
    img1 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)
    img2 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)

    img = combine_plots([img1, img2], save=False)

    cv.imshow('image',img)
    cv.waitKey(0)
    cv.destroyAllWindows()

def test_diff_size_2_imgs_combine_plots():
    img1 = np.random.randint(256, size=(200, 200, 3), dtype=np.uint8)
    img2 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)

    img = combine_plots([img1, img2], save=False)

    cv.imshow('image',img)
    cv.waitKey(0)
    cv.destroyAllWindows()

    img1 = np.random.randint(256, size=(200, 20, 3), dtype=np.uint8)
    img2 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)

    img = combine_plots([img1, img2], save=False)

    cv.imshow('image',img)
    cv.waitKey(0)
    cv.destroyAllWindows()

    img1 = np.random.randint(256, size=(10, 200, 3), dtype=np.uint8)
    img2 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)

    img = combine_plots([img1, img2], save=False)

    cv.imshow('image',img)
    cv.waitKey(0)
    cv.destroyAllWindows()


def test_diff_size_imgs_combine_plots():

    img1 = np.random.randint(256, size=(200, 20, 3), dtype=np.uint8)
    img2 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)
    img3 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)
    img4 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)
    img5 = np.random.randint(256, size=(100, 100, 3), dtype=np.uint8)

    img = combine_plots([img1, img2, img3, img4, img5], save=False)

    cv.imshow('image',img)
    cv.waitKey(0)
    cv.destroyAllWindows()

if __name__ == "__main__":
    test_diff_size_imgs_combine_plots()
