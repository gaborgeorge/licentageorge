import numpy as np
import sys
import os
sys.path.append(os.path.join('..', '..'))

from Utils.KernelDecay import apply_kernel_function_trial, apply_kernel_decay_neuron, apply_kernel_decay_neuron_constant

'''
Parameters: trial_extractor1,
            trial_extractor2,
            tau,
            contrast - choose only trials with this contrast value,
            count,

Tries to get 10 trials for a specific constrast

# NOTE: In current datasets their are 10 trials for each contrast for every orientation:
            if this changes MODIFY the following function to give less trials that it is
            asked for
'''
def __get_trials_for_contrast_2extractors(trial_extractor1, trial_extractor2, tau, contrast, count=10):
    # trials for an orientation are put in a list
    orientations = [[], [], [], [], [], [], [], []]
    trials1 = [[], [], [], [], [], [], [], []]
    trials2 = [[], [], [], [], [], [], [], []]

    orientation_inc = 45

    for i in range(len(trial_extractor1.trials)):
        if trial_extractor1.trials[i]['meta']['contrast'] == contrast:
            orientation = trial_extractor1.trials[i]['meta']['direction'] // orientation_inc
            orientations[orientation].append(i)

    # for each orientation extract a random permutation of trials
    for i in range(len(orientations)):
        # get permutations of indexes for trials with orientations i, for both extractors
        permutation = np.arange(len(orientations[i]))
        np.random.shuffle(permutation)
        permutation = permutation[:count]

        for j in permutation:
            trial_values1 = trial_extractor1.trials[orientations[i][j]]['spikes']
            trial_values2 = trial_extractor2.trials[orientations[i][j]]['spikes']

            trial_decay1 = apply_kernel_function_trial(trial_values1, trial_extractor1.mili_duration, tau, apply_kernel_decay_neuron)
            trial_features1 = np.transpose(trial_decay1)
            # NOTE:
            trial_decay2 = apply_kernel_function_trial(trial_values2, trial_extractor2.mili_duration, tau, apply_kernel_decay_neuron)#apply_kernel_decay_neuron)
            trial_features2 = np.transpose(trial_decay2)

            trials1[i].append(trial_features1)
            trials2[i].append(trial_features2)

    return trials1, trials2

'''
Parameters: trials_extractor,
            tau,
            contrast - of the trial,
            count,

Tries to get 10 trials for a specific constrast

# NOTE: In current datasets their are 10 trials for each contrast for every orientation:
            if this changes MODIFY the following function to give less trials that it is
            asked for
'''
def __get_trials_for_contrast(trial_extractor, tau, contrast, count=10):
    orientations_trials = [[], [], [], [], [], [], [], []]

    orientations = [[], [], [], [], [], [], [], []]
    orientation_inc = 45

    for i in range(len(trial_extractor.trials)):
        orientation = trial_extractor.trials[i]['meta']['direction'] // orientation_inc

        if (len(orientations[orientation]) >= count):
            continue

        if trial_extractor.trials[i]['meta']['contrast'] == contrast:
            orientations_trials[orientation].append(i)

    # extract randomly the trials
    for i in range(len(orientations_trials)):
        permutation = np.arange(len(orientations_trials[i]))
        np.random.shuffle(permutation)
        permutation = permutation[:count]

        for j in permutation:
            trial_values = trial_extractor.trials[orientations_trials[i][j]]['spikes']
            trial_decay = apply_kernel_function_trial(trial_values, trial_extractor.mili_duration, tau, apply_kernel_decay_neuron)
            trial_features = np.transpose(trial_decay)
            orientations[i].append(trial_features)

    return orientations


def __get_trials_for_orientations(trial_extractor, tau, count=10):
    # trials for an orientation are put in a list
    orientations = [[], [], [], [], [], [], [], []]

    orientation_inc = 45

    # we know orientations are 0, 45, .... 315
    for i in range(len(trial_extractor.trials)):
        # get orientations
        orientation_idx = trial_extractor.trials[i]['meta']['direction'] // orientation_inc

        # check if more trials needed for this orientations
        if (len(orientations[orientation_idx]) >= count):
            continue

        trial_values = trial_extractor.trials[i]['spikes']
        # NOTE:
        trial_decay = apply_kernel_function_trial(trial_values, trial_extractor.mili_duration, tau, apply_kernel_decay_neuron)#apply_kernel_decay_neuron)
        trial_features = np.transpose(trial_decay)
        orientations[orientation_idx].append(trial_features)

    return orientations

def __get_trials_for_orientations_random(trial_extractor1, trial_extractor2, tau, count=10):
    # trials for an orientation are put in a list
    orientations = [[], [], [], [], [], [], [], []]
    trials1 = [[], [], [], [], [], [], [], []]
    trials2 = [[], [], [], [], [], [], [], []]

    orientation_inc = 45

    # get the indexes of trials for orientations
    for i in range(len(trial_extractor1.trials)):
        # get orientations
        orientation_idx = trial_extractor1.trials[i]['meta']['direction'] // orientation_inc
        orientations[orientation_idx].append(i) # trial i was orientation orientation_idx
    # for each orientation extract a random permutation of trials
    for i in range(len(orientations)):
        # get permutations of indexes for trials with orientations i, for both extractors
        permutation = np.arange(len(orientations[i]))
        np.random.shuffle(permutation)
        permutation = permutation[:count]

        for j in permutation:
            trial_values1 = trial_extractor1.trials[orientations[i][j]]['spikes']
            trial_values2 = trial_extractor2.trials[orientations[i][j]]['spikes']

            trial_decay1 = apply_kernel_function_trial(trial_values1, trial_extractor1.mili_duration, tau, apply_kernel_decay_neuron)
            trial_features1 = np.transpose(trial_decay1)
            ## NOTE:
            trial_decay2 = apply_kernel_function_trial(trial_values2, trial_extractor2.mili_duration, tau, apply_kernel_decay_neuron)
            trial_features2 = np.transpose(trial_decay2)

            trials1[i].append(trial_features1)
            trials2[i].append(trial_features2)

    return trials1, trials2


'''
Parameters: - trial_extractor: instance of DataSet.TrialExtractor.py
            - tau: integration constant
            - count: number of trials to be extracted per orientation
            - contrast: only extract trials with this value for the contrast
                        default -1: meaning dont look at contrast

Results:    - 2D arry where:
                - Dimension 1 = number of samples
                - Dimension 2 = number of neurons
'''
def build_dataset(trial_extractor, tau=20, count=10, contrast=0):
    print('[{0}] START'.format(sys._getframe().f_code.co_name))

    if contrast == 0:
        orientations = __get_trials_for_orientations(trial_extractor, tau, count)
    else:
        orientations = __get_trials_for_contrast(trial_extractor, tau, contrast, count)
    dataset = []
    # combine every trial for on orientation to one
    for i in range(len(orientations)):
        dataset.append(np.vstack(orientations[i]))
    # combine trials for all orientations into one dataset
    dataset = np.vstack(dataset)
    print('[{0}] COMPLETE - dataset shape {1}'.format(sys._getframe().f_code.co_name, dataset.shape))
    return dataset


'''
Parameters: - trial_extractor1: instance of DataSet.TrialExtractor.py
            - trial_extractor2: instance of DataSet.TrialExtractor.py
            - tau: integration constant
            - count: number of trials to be extracted per orientation
            - random: if the trials should be extracted random or not
            - contrast: only extract trials with this value for the contrast
                        default -1: meaning dont look at contrast

Results:    - 2D arry where:
                - Dimension 1 = number of samples
                - Dimension 2 = number of neurons
'''
def build_merged_dataset(trial_extractor1, trial_extractor2, tau=20, count=10, random=False, contrast=0):
    print('[{0}] START'.format(sys._getframe().f_code.co_name))

    if random is True:
        if contrast == 0:
            orientations_1, orientations_2 = __get_trials_for_orientations_random(trial_extractor1, trial_extractor2, tau, count)
        else:
            orientations_1, orientations_2 = __get_trials_for_contrast_2extractors(trial_extractor1, trial_extractor2, tau, contrast, count)
    else:
        if contrast == 0:
            orientations_1 = __get_trials_for_orientations(trial_extractor1, tau, count)
            orientations_2 = __get_trials_for_orientations(trial_extractor2, tau, count)
        else:
            orientations_1 = __get_trials_for_contrast(trial_extractor1, tau, contrast, count)
            orientations_2 = __get_trials_for_contrast(trial_extractor2, tau, contrast, count)
    dataset = []

    # alternate elements for the datasets
    '''
    OBS: len(dataset_1) == len(dataset_2)
    '''
    for i in range(len(orientations_1)):
        dataset.append(np.vstack(orientations_1[i]))
        dataset.append(np.vstack(orientations_2[i]))
    # combine trials for all orientations into one dataset
    dataset = np.vstack(dataset)
    print('[{0}] COMPLETE - dataset shape {1}'.format(sys._getframe().f_code.co_name, dataset.shape))
    return dataset

#TO DO: maybe these two functions need to be moved în classification module, because there is where they are used#
