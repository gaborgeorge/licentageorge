import numpy as np
import os
import sys
sys.path.append(os.path.join('..', '..'))

from Visualization.Color.colormap import colormap


def to_color(values, current_max):
    return values * (255 // current_max)


def projections_to_plot(projections, trial_shape, trials_per_orientation, cube_max, orientations_count = 8):
    return to_color(projections.reshape((orientations_count, trials_per_orientation, trial_shape[1], 3)), cube_max)


'''
Parameters: - projections:  4D array reprezenting the result of aplying SOM
                            to a dataset formed by merging 2 separate dataset
                            assumes that the second dimension is an even number
                    - Dimensino 1 = number of orientations
                    - Dimension 2 = number of trials per orientations
                    - Dimension 3 = number of colors per trial
                    - Dimension 4 = number of channels per color
Result: 4D array resulted by subtracting in pairs of two the orientations

Description: - projections will be usually a (16, 10, a, 3) vector where 16 is 2 *
     the number of unique orientations
             - it will create a (24, 10, a, 3) numpy array by taking the first
     2 elements of projections and making the distance and converting to JET colormap
'''
def get_diff_projections(projections):
    result_shape = (projections.shape[0] + projections.shape[0] // 2, projections.shape[1], projections.shape[2], projections.shape[3])
    result = np.zeros(result_shape, dtype=np.uint8)

    # build the colormap of all differences
    distances = np.empty([projections.shape[0] // 2, projections.shape[1], projections.shape[2]], dtype=np.float32)
    for i in range(projections.shape[0] // 2):
        distances[i] = np.sqrt(np.sum(np.absolute(projections[2 * i] - projections[2 * i + 1]) ** 2, axis=2))

    diff_colors = colormap(distances)

    i_result = 0
    for i in range(projections.shape[0] // 2):
        result[i_result] = projections[2 * i]
        result[i_result + 1] = projections[2 * i + 1]
        result[i_result + 2] = diff_colors[i]
        i_result += 3

    return result


'''
Parameters: - projections: 4D array reprezenting the result of aplying SOM
                            to a dataset formed by merging 2 separate dataset
                            assumes that the second dimension is an even number
                    - Dimensino 1 = number of orientations
                    - Dimension 2 = number of trials per orientations
                    - Dimension 3 = number of colors per trial
                    - Dimension 4 = number of channels per color
Result: 1D array of float32 reprezenting distances of the colors
'''
def get_diff_distances(projections):

    result = np.empty([projections.shape[0] // 2 * projections.shape[1] * projections.shape[2]], dtype=np.float32)
    start = 0

    for i in range(projections.shape[0] // 2):
        v1 = projections[2 * i]
        v2 = projections[2 * i + 1]

        distances = np.sqrt(np.sum(np.absolute(v1 - v2) ** 2, axis=projections.ndim - 2))
        distances = np.ravel(distances)

        result[start:start + distances.size] = distances
        start += distances.size

    return result

if __name__ == '__main__':
    a = np.random.randint(256, size=(2, 2, 2, 2))
    #b = get_diff_projections(a)

    #a = np.random.randint(256, size=(2, 2, 2))
    b = get_diff_differences(a)
    print(a)
    print(b)
