import numpy as np
import time
import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

from DataSet.TrialExtractor import TrialExtractor
from Visualization.SOM.SOMdataBuilder import build_merged_dataset, build_dataset
from Visualization.SOM.Alg.SOM import SOM
from Visualization.Plots_cv import plot_bands_cv
from Visualization.Plots_matplot import plot_histogram
from Visualization.SOM.SOM_projections_utils import get_diff_projections, projections_to_plot, get_diff_distances
from config import mili_between_START_ON, mili_between_START_END, mili_between_START_OFF
'''
Params: - trial_extractor = instance of TrialExtractor
        - tau             = integration constant
        - size            = (width, height, depth) tuple for the size of the SOM
        - title            = title of the figure
        - save             = if it should save the figure
        - save_file        = the file in which to save to figure if save is True

Return: - 3D array representing the image resulted
'''
def SOM_plot(trial_extractor, tau, size, contrast, title='', save=False, save_file='', events=[0, mili_between_START_ON, mili_between_START_END, mili_between_START_OFF]):
    start = time.time()

    dataset = build_dataset(trial_extractor, tau=tau, contrast=contrast)
    orientations_count = 8

    # create som
    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.project(dataset)
    plot_projections = projections_to_plot(projections, (trial_extractor.spike_reader.metadata.no_units, trial_extractor.mili_duration),
                            10, size[0] - 1, orientations_count=orientations_count)

    orientations_values = [0, 45, 90, 135, 180, 225, 270, 315]

    plot = plot_bands_cv(plot_projections, orientations_values, color_height=10, color_width=1, events=events,
                title=title, save=save, save_name=save_file)

    print('[{0}] COMPLETE - time {1}, size {2}, tau {3}'.format(sys._getframe().f_code.co_name, time.time() - start, size, tau))

    return plot




'''
Params: - trial_extractor1 = instance of TrialExtractor
        - trial_extractor2 = instance of TrialExtractor
        - tau              = integration constant
        - size             = (width, height, depth) tuple for the size of the SOM
        - contrasts        = the contrast of the trials
        - title            = title of the figure
        - save             = if it should save the figure
        - save_file        = the file in which to save to figure if save is True
        - use_diff         = if it should draw after each 2 bands a third one that is
                                the difference of the 2
        - use_C            = if it should use the C alg for SOM

Return: - 3D array representing the image resulted
'''
def SOM_plot_merged(trial_extractor1, trial_extractor2, tau, size, contrast, title='', save=False, save_file='', use_diff=True, use_C=False, events=[0, mili_between_START_ON, mili_between_START_END, mili_between_START_OFF]):
    # create big dataset
    dataset = build_merged_dataset(trial_extractor1, trial_extractor2, tau=tau, contrast=contrast, random=True)
    orientations_count = 16

    if use_C is True:
        projections = getProjectionForOrientation(dataset, size[0])
    else:
        # create som
        som = SOM(size[0], size[1], size[2])
        som.fit(dataset)
        projections = som.project(dataset)
        plot_projections = projections_to_plot(projections, (trial_extractor1.spike_reader.metadata.no_units, trial_extractor1.mili_duration),
                            10, size[0] - 1, orientations_count=orientations_count)

    if use_diff is True:
        plot_projections = get_diff_projections(plot_projections)
        orientations_values = [0, 0, 0, 45, 45, 45, 90, 90, 90, 135, 135, 135, 180, 180, 180, 225, 225, 225, 270, 270, 270, 315, 315, 315]
    else:
        orientations_values = [0, 0, 45, 45, 90, 90, 135, 135, 180, 180, 225, 225, 270, 270, 315, 315]


    plot = plot_bands_cv(plot_projections, orientations_values, color_height=10, color_width=1, events=events,
                title=title,
                save=save, save_name=save_file)
    print('[{0}] COMPLETE - size {1}, tau {2}'.format(sys._getframe().f_code.co_name, size, tau))

    return plot
