import time
import sys
import os
project_path = os.path.join('..', '..')
sys.path.append(project_path)

import numpy as np

from DataSet.TrialExtractor import TrialExtractor
from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
from Visualization.SOM.SOMdataBuilder import build_merged_dataset, build_dataset
from Visualization.SOM.Alg.SOM import SOM
from Visualization.Plots_cv import plot_bands_cv
from Visualization.Plots_matplot import plot_histogram
from Visualization.SOM.SOM_projections_utils import get_diff_projections, projections_to_plot, get_diff_distances
from Visualization.SOM.SOM_plots import SOM_plot, SOM_plot_merged
from config import data_dir, ssd_file, result_file, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END, mili_between_ON_OFF, mili_between_ON_END

'''
Parameters: - trial_extractor1
            - trial_extractor2
            - tau
            - size
            - use_log - if it should aply log to the distances
            - title
            - save
            - save_file
Return:     - None
'''
def SOM_plot_distance_histogram(trial_extractor1, trial_extractor2, tau, size, use_log=False, title='', save=False, save_file=''):
    dataset = build_merged_dataset(trial_extractor1, trial_extractor2, tau, random=True)

    # create som
    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.project(dataset)
    plot_projections = projections_to_plot(projections, (trial_extractor1.spike_reader.metadata.no_units, trial_extractor1.mili_duration),
                        10, size[0] - 1, orientations_count=16)

    distances = get_diff_distances(plot_projections)
    print('Unique distances', np.unique(distances))
    if use_log is True:
        distances[distances == 0] = 0.1
        distances = np.log(distances)
    print('Unique log distances', np.unique(distances))

    plot_histogram(distances, title=title, save=save, save_file=save_file)

    print('[{0}] COMPLETE - saved in {1}'.format(sys._getframe().f_code.co_name, save_file))


def SOM_plot_merged_and_hist(trial_extractor1, trial_extractor2, tau, size, SOM_title='', SOM_save=False, SOM_save_file='', hist_title='', hist_save=False, hist_save_file=''):
    dataset = build_merged_dataset(trial_extractor1, trial_extractor2, tau, random=True)

    # create SOM

    som = SOM(size[0], size[1], size[2])
    som.fit(dataset)
    projections = som.project(dataset)
    plot_projections = projections_to_plot(projections, (trial_extractor1.spike_reader.metadata.no_units, trial_extractor1.mili_duration),
                        10, size[0] - 1, orientations_count=16)

    # som Plot
    SOM_projections = get_diff_projections(plot_projections)
    orientations_values = [0, 0, 0, 45, 45, 45, 90, 90, 90, 135, 135, 135, 180, 180, 180, 225, 225, 225, 270, 270, 270, 315, 315, 315]

    plot = plot_bands_cv(SOM_projections, orientations_values, color_height=10, color_width=1, events=(0, 2700, 3100),
            title=SOM_title,
            save=SOM_save, save_name=SOM_save_file)
    print('[{0}] SOM plot COMPLETE - size {1}, tau {2}, saved in {3}'.format(sys._getframe().f_code.co_name, size, tau, SOM_save_file))

    # histogram Plot
    distances = get_diff_distances(plot_projections)
    print(distances.shape)
    print('[{0}] Unique distances {1}'.format(sys._getframe().f_code.co_name, np.unique(distances)))

    plot_histogram(distances, title=hist_title, save=hist_save, save_file=hist_save_file)

    print('[{0}] Historgram plot COMPLETE - saved in {1}'.format(sys._getframe().f_code.co_name, hist_save_file))

    return plot

# TO DO rewrite, face histograma de diferente
def plot_histograms(use_mean=False, use_z_score=False, use_log=False, desc=''):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader, use_mean_neuron=False, use_z_score=use_z_score, use_start=False, use_end=True)
    trial_extractor_mean_neurons = TrialExtractor(spike_reader, trial_meta_reader, use_mean_neuron=True, use_z_score=use_z_score, use_start=False, use_end=True)

    for size in kohonen_size:
        for tau in tau_values:
            start = time.time()

            SOM_plot_distance_histogram(trial_extractor_amplitudes, trial_extractor_mean_neurons, tau, size, use_log=use_log,
                            title='Distances - SOM size=' + str(size) + ' Tau=' + str(tau) + '\nDistances between amplitude colors and mean colors',
                            save=True,
                            save_file=os.path.join('Results_test', 'Histogram_SOM_size_' + str(size) + '_tau_' + str(tau) + '_' + desc + '.png'))

            print('[{0}] COMPLETE - time {1}, size {2}, tau {3}'.format(sys._getframe().f_code.co_name, time.time() - start, size, tau))

# TO DO rewrite, face o histograma de diferente + SOM plot
def plot_SOM_and_histograms(use_mean=False, use_z_score=False, desc=''):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader, use_mean_neuron=False, use_z_score=use_z_score, use_start=False, use_end=True)
    trial_extractor_mean_neurons = TrialExtractor(spike_reader, trial_meta_reader, use_mean_neuron=True, use_z_score=use_z_score, use_start=False, use_end=True)

    for size in kohonen_size:
        for tau in tau_values:
            start = time.time()

            SOM_plot_merged_and_hist(trial_extractor_amplitudes, trial_extractor_mean_neurons, tau, size,
                            SOM_title='SOM size=' + str(size) + ' Tau=' + str(tau) + ' First with Amplitude, trial extracted random ' + desc,
                            SOM_save=True,
                            SOM_save_file=os.path.join('Results_test', 'MERGE_SOM_size_' + str(size) + '_Tau_' + str(tau) + '_' + desc + '.png'),
                            hist_title='Distances - SOM size=' + str(size) + ' Tau=' + str(tau) + '\n Distances between amplitude colors and mean colors',
                            hist_save=True,
                            hist_save_file=os.path.join('Results_test', 'Histogram_SOM_size_' + str(size) + '_tau_' + str(tau) + '_' + desc + '.png'))

            print('[{0}] COMPLETE - time {1}, size {2}, tau {3}'.format(sys._getframe().f_code.co_name, time.time() - start, size, tau))

kohonen_size = [(5, 5, 5)]#[(5, 5, 5), (7, 7, 7), (10, 10, 10)]
tau_values = [30]#, 100, 500, 1000, 1500]


'''
Parameters: sizes - for SOM
            tau,
            contrasts,
            use_mean - if it should set the amplitude to the mean amplitude of neurons
            use_mean_neuron - if it should set the amplitudes to the mean amplitudes of each neuron
            use_z_score - amplitudes in [0.5, 1.5] mean = 1, no amplitude = 0
            use_start - if it should consider the start of the trial, if False
                            begin form ON
            use_end - if it should consider the end of the trial, if False end
                            at OFF
            save_dir - directory where to save the plot
'''
#GOOD
def build_SOM_plot_simple(sizes, taus, contrasts, use_mean=False, use_mean_neuron=False, use_z_score=False, use_fisher=False, use_start=False, use_end=True, save_dir='Results'):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))

    trial_extractor = TrialExtractor(spike_reader, trial_meta_reader,
                                        use_mean_neuron=use_mean,
                                        use_mean_amplitude=use_mean,
                                        use_z_score=use_z_score,
                                        use_fisher=use_fisher,
                                        use_start=use_start,
                                        use_end=use_end)

    if use_fisher is True:
        fisher_desc = 'Fisher_Z_'
        #plot_histogram(np.array(trial_extractor.normal_amplitudes), show=True, save=False)
        #plot_histogram(np.array(trial_extractor.fisher_amplitudes), show=True, save=False)
    else:
        fisher_desc = ''

    if use_mean is True:
        use_mean_desc = 'Mean_Amplitude_'
    elif use_mean_neuron is True:
        use_mean_desc = 'Mean_neuron_Amplitude_'
    else:
        use_mean_desc = ''

    if use_z_score is True:
        z_desc = 'Z_score_'
    else:
        z_desc = ''

    if use_start is True:
        start_desc = 'From_Start_'
    else:
        start_desc = 'From_ON_'

    if use_end is True:
        end_desc = 'To_End_'
    else:
        end_desc = 'To_OFF_'

    for size in sizes:
        for tau in taus:
            for contrast in contrasts:
                size_desc = 'Size_' + str(size) + '_'
                tau_desc = 'Tau_' + str(tau) + '_'
                if contrast == -1:
                    contrast_desc = ''
                else:
                    contrast_desc = 'Contrast_' + str(contrast) + '_'

                title = size_desc + tau_desc + contrast_desc + z_desc + use_mean_desc + fisher_desc + start_desc + end_desc

                SOM_plot(trial_extractor, tau, size, contrast,
                    title=title,
                    save=True,
                    save_file=os.path.join(save_dir, title + '.png'))

#GOOD
def build_SOM_plots_merged(sizes, taus, contrasts, use_mean_amp=False, use_mean_neuron=True, use_z_score=False, use_diff=True, use_fisher=False, use_start=False, use_end=True, save_dir='Results_merged'):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))

    trial_extractor_amplitudes = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=False,
                                                        use_mean_neuron=False,
                                                        use_z_score=use_z_score,
                                                        use_fisher=use_fisher,
                                                        use_start=use_start,
                                                        use_end=use_end)
    trial_extractor_mean = TrialExtractor(spike_reader, trial_meta_reader,
                                                        use_mean_amplitude=use_mean_amp,
                                                        use_mean_neuron=use_mean_neuron,
                                                        use_fisher=use_fisher,
                                                        use_z_score=use_z_score,
                                                        use_start=use_start,
                                                        use_end=use_end)
    if use_fisher is True:
        fisher_desc = 'Fisher_Z_'
    else:
        fisher_desc = ''

    if use_mean_amp is True:
        use_mean_desc = 'Mean_Amplitude_'
    elif use_mean_neuron is True:
        use_mean_desc = 'Mean_neuron_Amplitude_'
    else:
        use_mean_desc = ''

    if use_z_score is True:
        z_desc = 'Z_score_'
    else:
        z_desc = ''

    if use_start is True:
        start_desc = 'From_Start_'
    else:
        start_desc = 'From_ON_'

    if use_end is True:
        end_desc = 'To_End_'
    else:
        end_desc = 'To_OFF_'

    for size in sizes:
        for tau in taus:
            for contrast in contrasts:

                size_desc = 'Size_' + str(size) + '_'
                tau_desc = 'Tau_' + str(tau) + '_'
                if contrast == -1:
                    contrast_desc = ''
                else:
                    contrast_desc = 'Contrast_' + str(contrast) + '_'

                title = 'MERGE_' + size_desc + tau_desc + contrast_desc + z_desc + use_mean_desc + fisher_desc + start_desc + end_desc

                SOM_plot_merged(trial_extractor_amplitudes, trial_extractor_mean, tau, size, contrast, use_diff=use_diff,
                    title=title,
                    save=True,
                    save_file=os.path.join(save_dir, title + '.png'))



'''
Parameters: sizes              - list of number of centroids for SOM
            taus               - list of integration constants values
            contrasts          - list of contrast values
            merge              - if it should use a 2 datasets
            use_diff           - if it should add contrast bands if merge =True
            use_mean_amplitude - use mean amplitude for spike value
            use_mean_neuron    - use mean amplitude per channel for spike value
            use_z_score        - if it should apply z-score
            use_start          - if it should consider the START event
            use_end            - if it should consider the END event
            save_dir           - where to save the figures

Description: - for every pair (size, tau, contrast) will result a figure
'''
def build_plots(sizes,
                taus,
                contrasts,
                merge=False,
                use_diff=False,
                use_mean_amplitude=False,
                use_mean_neuron=False,
                use_z_score=False,
                use_fisher=False,
                use_start=False,
                use_end=False,
                save_dir='Results'):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))

    trial_extractor_amplitudes = TrialExtractor(
                                    spike_reader,
                                    trial_meta_reader,
                                    use_mean_amplitude=False,
                                    use_mean_neuron=False,
                                    use_z_score=use_z_score,
                                    use_fisher=use_fisher,
                                    use_start=use_start,
                                    use_end=use_end)
    trial_extractor_mean_neurons = TrialExtractor(
                                    spike_reader,
                                    trial_meta_reader,
                                    use_mean_amplitude=use_mean_amplitude,
                                    use_mean_neuron=use_mean_neuron,
                                    use_z_score=use_z_score,
                                    use_fisher=use_fisher,
                                    use_start=use_start,
                                    use_end=use_end)

    if use_fisher is True:
        fisher_desc = 'Fisher_Z_'
    else:
        fisher_desc = ''

    if use_mean_amplitude is True:
        use_mean_desc = 'Mean_Amplitude_'
    elif use_mean_neuron is True:
        use_mean_desc = 'Mean_neuron_Amplitude_'
    else:
        use_mean_desc = ''

    if use_z_score is True:
        z_desc = 'Z_score_'
    else:
        z_desc = ''

    if use_start is True:
        start_desc = 'From_Start_'
    else:
        start_desc = 'From_ON_'

    if use_end is True:
        end_desc = 'To_End_'
    else:
        end_desc = 'To_OFF_'

    if use_start is False and use_end is False:
        events = [0, mili_between_ON_OFF]
    elif use_start is True and use_end is False:
        events = [0, mili_between_START_ON, mili_between_START_OFF]
    elif use_start is False and use_end is True:
        events = [0, mili_between_ON_OFF, mili_between_ON_END]
    else:
        events = [0, mili_between_START_ON, mili_between_START_OFF, mili_between_START_END]

    for size in sizes:
        for tau in taus:
            for contrast in contrasts:

                # title and save_file name fields
                if contrast == -1:
                    contrast_desc = ''
                else:
                    contrast_desc = 'Contrast_' + str(contrast) + '_'
                size_desc = 'Size_' + str(size) + '_'
                tau_desc = 'Tau_' + str(tau) + '_'
                if use_z_score is True:
                    z_desc = 'Z_score_'
                else:
                    z_desc = ''

                start = time.time()
                # create big dataset
                if merge is True:

                    title = 'MERGE_' + size_desc + tau_desc + contrast_desc + z_desc + use_mean_desc + fisher_desc + start_desc + end_desc

                    SOM_plot_merged(trial_extractor_amplitudes, trial_extractor_mean_neurons, tau, size, contrast, use_diff=use_diff,
                        title=title,
                        save=True,
                        save_file=os.path.join(save_dir, title + '.png'), events=events)
                else:

                    title = size_desc + tau_desc + contrast_desc + z_desc + use_mean_desc + fisher_desc + start_desc + end_desc

                    SOM_plot(trial_extractor_amplitudes, tau, size, contrast,
                        title=title,
                        save=True,
                        save_file=os.path.join(save_dir, title + '.png'), events=events)


                print('[{0}] COMPLETE - time {1}, size {2}, tau {3}'.format(sys._getframe().f_code.co_name, time.time() - start, size, tau))

if __name__ == '__main__':
    #build_plots(sizes=[(5, 5, 5)], taus=[20], contrasts=[100], merge=True, use_diff=True, use_start=False, use_end=True, use_z_score=True, use_mean_neuron=True, save_dir='DEMO_RESULTS')
    build_plots(sizes=[(10, 10, 10)], taus=[20], contrasts=[100], merge=True, use_diff=True, use_start=False, use_end=True, use_z_score=True, use_mean_neuron=True, save_dir='DEMO_RESULTS')
