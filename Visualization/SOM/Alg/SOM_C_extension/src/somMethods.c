#include <Python.h>
#include <numpy/arrayobject.h>
#include <stdint.h>
#include <time.h> 
#include <math.h>

#define float64_t double

int Cfib(int n)
{
    if (n < 2)
        return n;
    else
        return Cfib(n-1) + Cfib(n-2);
}
 
static PyObject* fib(PyObject* self, PyObject* args)
{
    int n;
 
    if (!PyArg_ParseTuple(args, "i", &n))
        return NULL;

    return Py_BuildValue("i", Cfib(n));
}

static PyObject* version(PyObject* self)
{
    return Py_BuildValue("s", "Version 2.0");
}

#define np_access_1D_int32(array, i) (*(int32_t*)((int8_t*)PyArray_DATA(array) +                \
                                (i) * PyArray_STRIDES(array)[0]))
#define np_access_2D_int32(array, i, j) (*((int8_t*)PyArray_DATA(array) +                \
                                    (i) * PyArray_STRIDES(array)[0] +   \
                                    (j) * PyArray_STRIDES(array)[1]))
#define np_dimension(array, i) (array->dimensions[(i)])
#define np_access_2D_float64(array, i, j) (*(float64_t*)((int8_t*)PyArray_DATA(array) + \
                                            i * PyArray_STRIDES(array)[0] + \
                                            j * PyArray_STRIDES(array)[1]))
#define np_access_1D_float64(array, i) (*(float64_t*)((int8_t*)PyArray_DATA(array) + \
                                        (i) * PyArray_STRIDES(array)[0]))           


static PyObject* showNumpyArray(PyObject* self, PyObject* args)
{
    PyArrayObject *array_np;
    if (!PyArg_ParseTuple(args, "O!", &PyArray_Type, &array_np)) {
        return NULL;
    }
    printf("%p\n", array_np);

    for (int i = 0; i < np_dimension(array_np, 0); ++i)
    {
        printf("%d ", np_access_1D_int32(array_np, i));
        np_access_1D_int32(array_np, i) = 2 * np_access_1D_int32(array_np, i);
    }

    printf("size int %d\n", sizeof(int));
    uint32_t p = np_dimension(array_np, 0);
    printf("p %d\n", p);
    return Py_BuildValue("i", np_dimension(array_np, 0));
}

/*--------------------------------------------------------
FUnctions below are strictly for the SOM algorithm
--------------------------------------------------------*/
static uint32_t * random_perm(uint32_t n)
{
    int rand_indx;
    uint32_t * perm = (uint32_t*) malloc(n * sizeof(uint32_t));
    if (perm == NULL)
    {
        printf("[%s.%s] Memory allocation failed\n", __FILE__, __FUNCTION__);
        return NULL;
    }

    for (uint32_t i = 0; i < n; ++i)
    {
        perm[i] = i;
    }

    // shuffle
    for (uint32_t i = 0; i < n; ++i)
    {
        rand_indx = rand() % n;
        perm[i] = perm[i] ^ rand_indx;
        rand_indx = perm[i] ^ rand_indx;
        perm[i] = perm[i] ^ rand_indx;
    }

    return perm;
}

static uint32_t * random_values_within_limit(uint32_t limit, uint32_t cnt)
{
    uint32_t * values = (uint32_t*) malloc(cnt * sizeof(uint32_t));
    if (values == NULL)
    {
        printf("[%s.%s] Memory allocation failed\n", __FILE__, __FUNCTION__);
        return NULL;
    }

    for (uint32_t i = 0; i < cnt; ++i)
    {
        values[i] = rand() % limit;
    }
    return values;
}

typedef struct __ARRAY_STR
{
    uint32_t * data;
    uint32_t size;
} my_array_t;

static float64_t euclidian_dist(PyArrayObject *x, uint32_t row_x, PyArrayObject *y, uint32_t row_y, uint32_t len)
{
    float64_t dist = 0.0;
    for (uint32_t i = 0; i < len; ++i)
    {
        dist += pow(np_access_2D_float64(x, row_x, i) - np_access_2D_float64(y, row_y, i), 2);
    }
    return sqrt(dist);
}

static float64_t get_radial_kernel(uint32_t t, float64_t l)
{
    return pow(2.17828, -(t / l));
}

static my_array_t * get_neighbours(PyArrayObject * grid, uint32_t grid_rows, uint32_t grid_cols, uint32_t grid_row, uint32_t t , float64_t l, float64_t base_neighbourhood)
{
    float64_t radius = base_neighbourhood * get_radial_kernel(t, l);
    my_array_t * neighbours = (my_array_t *) malloc(sizeof(my_array_t));
    if (neighbours == NULL)
    {
        printf("[%s.%s] Memory allocation failed\n", __FILE__, __FUNCTION__);
        return NULL;
    }
    neighbours->data = (uint32_t*) malloc(grid_rows * sizeof(uint32_t));
    if (neighbours->data == NULL)
    {
        printf("[%s.%s] Memory allocation failed\n", __FILE__, __FUNCTION__);
        return NULL;
    }
    neighbours->size = 0;

    for (uint32_t i = 0; i < grid_rows; ++i)
    {
        if (euclidian_dist(grid, grid_row, grid, i, grid_cols) <= radius)
        {
            neighbours->data[neighbours->size] = i;
            neighbours->size++;
        }
    }

    return neighbours;
}

static uint32_t find_bmu(PyArrayObject *centroids, uint32_t centroids_rows, PyArrayObject *samples, uint32_t samples_idx, uint32_t samples_cols)
{
    float64_t dist = 0.0;

    float64_t closest_dist = euclidian_dist(samples, samples_idx, centroids, 0, samples_cols);
    uint32_t closest_idx = 0;
    for (uint32_t i = 1; i < centroids_rows; ++i)
    {
        dist = euclidian_dist(samples, samples_idx, centroids, i, samples_cols);
        if (dist < closest_dist)
        {
            closest_dist = dist;
            closest_idx = 0;
        }
    }

    return closest_idx;
}

static PyObject* fit(PyObject* self, PyObject* args)
{
    // function parameters
    float64_t base_neighbourhood;
    PyArrayObject *samples;
    PyArrayObject *centroids;
    PyArrayObject *grid;

    // function variables
    uint32_t samples_rows;
    uint32_t samples_cols;
    uint32_t centroids_rows;
    uint32_t grid_rows;
    uint32_t grid_cols;

    uint32_t xi;
    uint32_t * centroids_perm = NULL;
    uint32_t * samples_order = NULL;
    my_array_t * neighbours = NULL;
    uint32_t bmu_indx;
    float64_t old_val;
    float64_t l;
    float64_t alpha;

    // extract inputs
    if (!PyArg_ParseTuple(args, "dO!O!O!",
                                &base_neighbourhood,
                                &PyArray_Type, &samples,
                                &PyArray_Type, &centroids,
                                &PyArray_Type, &grid))
                                {
                                    printf("[%s.%s] Something wrong with inputs\n", __FILE__, __FUNCTION__);
                                    return NULL;
                                }
    samples_rows = np_dimension(samples, 0);
    samples_cols = np_dimension(samples, 1);
    centroids_rows = np_dimension(centroids, 0);
    grid_rows = np_dimension(grid, 0);
    grid_cols = np_dimension(grid, 1);

    // debug info
    printf("[%s.%s] Samples rows %d, cols %d; Centroids rows %d; Grid rows %d, cols %d; Base neighborhood %lf\n",
                __FILE__, __FUNCTION__, samples_rows, samples_cols, centroids_rows, grid_rows, grid_cols, base_neighbourhood);

    // begin algorithm
    
    // init starting centroids
    srand(time(0)); 
    centroids_perm = random_values_within_limit(samples_rows, centroids_rows);
    
    for (uint32_t i = 0; i < centroids_rows; ++i)
    {
        for (uint32_t j = 0; j < samples_cols; ++j)
        {
            np_access_2D_float64(centroids, i, j) = np_access_2D_float64(samples, centroids_perm[i], j);
        }
    }

    l = samples_rows / base_neighbourhood;
    alpha = 1.0;
    samples_order = random_perm(samples_rows);


    // updating centroids
    for (uint32_t i = 0; i < samples_rows; ++i)
    {
        xi = samples_order[i];
        bmu_indx = find_bmu(centroids, centroids_rows, samples, xi, samples_cols);

        neighbours = get_neighbours(grid, grid_rows, grid_cols, bmu_indx, samples_order[i], l, base_neighbourhood);

        // update neurons
        for (uint32_t u = 0; u < neighbours->size; u++)
        {
            for (uint32_t v = 0; v < samples_cols; v++)
            {
                old_val = np_access_2D_float64(centroids, u, v);
                np_access_2D_float64(centroids, u, v) = old_val + alpha * (np_access_2D_float64(samples, xi, v) - old_val);
            }
        }

        alpha -= 1 / (float64_t)samples_rows;

        free(neighbours->data);
        free(neighbours);
    }


    // free memory
    free(centroids_perm);
    free(samples_order);

    Py_INCREF(Py_None);
    return Py_None;
}
 
static PyObject* project(PyObject* self, PyObject* args)
{
    PyArrayObject *samples;
    PyArrayObject *centroids;
    PyArrayObject *grid;
    PyArrayObject *projections;
    
    uint32_t samples_rows;
    uint32_t samples_cols;
    uint32_t centroids_rows;
    uint32_t bmu_idx;

    // extract inputs
    if (!PyArg_ParseTuple(args, "O!O!O!O!",
                                &PyArray_Type, &samples,
                                &PyArray_Type, &centroids,
                                &PyArray_Type, &grid,
                                &PyArray_Type, &projections))
                                {
                                    printf("[%s.%s] Something wrong with inputs\n", __FILE__, __FUNCTION__);
                                    return NULL;
                                }

    samples_rows = np_dimension(samples, 0);
    samples_cols = np_dimension(samples, 1);
    centroids_rows = np_dimension(centroids, 0);

    printf("[%s.%s] Samples rows %d, cols %d; Centroids rows %d\n", __FILE__, __FUNCTION__, samples_rows, samples_cols, centroids_rows);

    for (uint32_t i = 0; i < samples_rows; ++i)
    {
        bmu_idx = find_bmu(centroids, centroids_rows, samples, i, samples_cols);
        np_access_2D_int32(projections, i, 0) = np_access_2D_int32(grid, bmu_idx, 0);
        np_access_2D_int32(projections, i, 1) = np_access_2D_int32(grid, bmu_idx, 1);
        np_access_2D_int32(projections, i, 2) = np_access_2D_int32(grid, bmu_idx, 2);
    }

    Py_INCREF(Py_None);
    return Py_None;
}

static PyMethodDef myMethods[] = {
    {"fib", fib, METH_VARARGS, "Calculate the Fibonacci numbers."},
    {"version", (PyCFunction)version, METH_NOARGS, "Returns the version."},
    {"showNumpyArray", showNumpyArray, METH_VARARGS, "Test with"},
    {"fit", fit, METH_VARARGS, "Updates centroids according to samples."},
    {"project", project, METH_VARARGS, "Projects samples to centroids."},
    {NULL, NULL, 0, NULL}
};
 
static struct PyModuleDef somMethods = {
	PyModuleDef_HEAD_INIT,
	"somUtils9", //name of module.
	"Self Organizing Map Module",
	-1,
	myMethods
};

PyMODINIT_FUNC PyInit_somUtils9(void)
{
    import_array();
    return PyModule_Create(&somMethods);
}