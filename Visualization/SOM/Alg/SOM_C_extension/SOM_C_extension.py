import numpy as np 
import sys
import time

import somUtils9

def get3DGridAsPoints(width, height, depth):
    grid = np.zeros((width * height * depth, 3), dtype=np.uint32)
    n = 0
    for i in range(width):
        for j in range(height):
            for k in range(depth):
                grid[n, 0] = i
                grid[n, 1] = j
                grid[n, 2] = k
                n += 1
    return grid

def computeBaseNeighborhoodFromGrid(X):
    return np.max(X) / 3

class SOM:
    def __init__(self, width, height, depth):
        self.grid = get3DGridAsPoints(width, height, depth)
        self.centroids_cnt = width * height * depth
        self.base_neighbourhood = computeBaseNeighborhoodFromGrid(self.grid)

    def fit(self, data):
        print('[{0}.{1}] STARTED'.format(self.__class__.__name__, sys._getframe().f_code.co_name))
        start = time.time()

        self.centroids = np.zeros((self.centroids_cnt, data.shape[1]), dtype=np.int32)

        somUtils9.fit(self.base_neighbourhood, data, self.centroids, self.grid)

        print('[{0}.{1}] COMPLETE - time {2}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, time.time() - start))

    def project(self, data):
        print('[{0}.{1}] STARTED'.format(self.__class__.__name__, sys._getframe().f_code.co_name))
        start = time.time()

        projections = np.zeros((data.shape[0], 3), dtype=np.uint32)
        somUtils9.project(data, self.centroids, self.grid, projections)

        print('[{0}.{1}] COMPLETE - time {2}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, time.time() - start))
        return projections

if __name__ == "__main__":
    m = SOM(10, 10, 10)
    X = np.random.rand(20000, 71)
    m.fit(X)
    p = m.project(X)
    print(p[:100])