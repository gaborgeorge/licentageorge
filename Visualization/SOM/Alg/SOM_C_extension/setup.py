from distutils.core import setup, Extension
import numpy
import os

module = Extension('somUtils9', sources = [os.path.join('src', 'somMethods.c'], include_dirs=[numpy.get_include()], extra_compile_args=["-Ofast", "-march=native"])

setup (name = 'somUtils9',
        version = '1.0',
        description = 'This is a package for somMethods',
        ext_modules = [module])
