'''currently this file is not used'''

import numpy as np
from math import sqrt, log
import time
import inspect

def get_3d_grid(width, height, depth):
    # create a grid, I don't realy understand to logic of 
    # putting the values are there are
    grid = np.mgrid[0:width, 0:height, 0:depth]

    # make 3 rows from the grid, (3, -1) means: make 3 rows and figure out how long should they be
    # and take the transpose to have 3 columns
    return grid.reshape(3, -1).T

def radial_kernel(t, lamb):
    return 2.17828 ** (-(t / float(lamb)))

def euclidian_distance(x, m):
    return np.sqrt(np.sum(np.power(np.subtract(x, m), 2), axis=1))

class KohonenMap3D:
    def __init__(self, width, height, depth):
        self.alpha = 1
        self.grid = get_3d_grid(width, height, depth)
        self.neurons_count = self.grid.shape[0]
        self.base_neighborhood = np.max(self.grid) / 2.0
        # when computing the projections, the current method creates
        # a matrix of dimension (number_samples, number_neurons, number_feature)
        # which can became to big to fit in memory
        # so we will project in batches
        self.batch_size = 1500
    
    # Expect X to pe a 2D matrix where every row is a sample
    def fit(self, X):
        # get number of samples and theis dimensions
        n, _ = X.shape
        
        # get random indexes to extract the neurons
        neuron_indexes = np.random.permutation(n)[0:self.neurons_count]
        
        # get initial neurons
        self.neurons = X[neuron_indexes]
        
        # training
        t = 0
        X_order = np.random.permutation(n) 
        self.l = n / log(self.base_neighborhood)
        
        for i in X_order:
            # get current sample
            x_i = X[i]
            
            # find closest neuron from current sample
            bmu_index = self.find_BMU(x_i)
            
            # update neurons
            self.update(bmu_index, t, x_i)
            
            # update learning rate
            self.alpha -= 1 / float(n)
            t += 1
            
    def find_BMU(self, x):
        # get distances between sample x and all the neurons and return the 
        # neuron index with minimum distance
        return np.argmin(euclidian_distance(x, self.neurons))
    
    def update(self, bmu_index, t, sample):
        radius = self.base_neighborhood * radial_kernel(t, self.l)
        bmu_coord = self.neurons[bmu_index]
        
        # get mask of neurons close enough to the bmu
        mask = euclidian_distance(bmu_coord, self.neurons) <= radius
        
        # update neurons
        # neurons[mask] means all the neurons that have the corresponding value in mask True
        # so only a subset of neurons are updated
        self.neurons[mask] += self.alpha * (sample - self.neurons[mask])
       
    # Expect X to pe a 2D matrix where every row is a sample
    # The results will be values in range [0-10, 0-10, 0-10] - may need to be rescalled 
    def project(self, X):
        #item_size = X.itemsize
        batch_count = X.shape[0] // self.batch_size
        number_features = X.shape[1]

        # the commented solution wont work because the input X currently is constructed from Arrays that 
        # were transposed, which only changes the strides and shape of the array not the data in memory
    
        #batches = np.lib.stride_tricks.as_strided(X, 
        #                                    shape=(batch_count, self.batch_size, number_features),
        #                                    strides=(item_size * self.batch_size * number_features, item_size * number_features, item_size))
        
        # interpret data as batches
        batches = X[:batch_count * self.batch_size].reshape(batch_count, self.batch_size, number_features)

        # one batch may be inconplet, extract it
        incomplet_batch = X[batch_count * self.batch_size:]
        projections = np.array([]).reshape(0, self.grid.shape[1])
        # process the complet batches
        for batch in batches:
            projections = np.vstack((projections, self.get_closest_projections(batch)))

        # process the incomplet batch if is not empty
        if incomplet_batch.shape[0] != 0:
            projections = np.vstack((projections, self.get_closest_projections(incomplet_batch)))

        return projections

    def get_closest_projections(self, batch):
        # get difference between every row in the batch and every eneuron
        differences = batch.reshape(batch.shape[0], 1, batch.shape[1]) - self.neurons

        # get all distances between every row of the batch and every neuron
        distances = np.sum((np.power(differences, 2)), axis=2)
        
        # get for every row of batch the corresponding color index
        color_indexes = np.argmin(distances, axis=1)
        
        return self.grid[color_indexes]

    # This should be used after the Map is trained, after 'fit'
    def export_neurons(self, file):
        np.save(file + '.npy', self.neurons)

    # Should be used after creating a Map, to avoid the 'fit' method
    def import_neurons(self, file):
        self.neurons = np.load(file + '.npy')


if __name__ == "__main__":
    ko_map = KohonenMap3D(10, 10, 10)

    X = np.random.rand(2000, 71)
    ko_map.fit(X)

    projections = ko_map.project(X)