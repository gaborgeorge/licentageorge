''' Currently this file is not used '''

import numpy as np
from math import sqrt, log
import inspect
import time

def get_3d_grid(width, height, depth):
    grid = np.zeros([width * height * depth, 3])
    i = 0

    for x in range(0, width):
        for y in range(0, height):
            for z in range(0, depth):
                grid[i, 0] = x
                grid[i, 1] = y
                grid[i, 2] = z
                i += 1

    return grid


def radial_kernel(t, lamb):
    return 2.17828 ** (-(t / float(lamb)))


def euclidean_distances(v1, v2):
    distance = 0
    for i in range(0, v1.size):
        distance += (v1[i] - v2[i]) * (v1[i] - v2[i])
    return sqrt(distance)


class KohonenMap3D:
    def __init__(self, width, height, depth, grid_type=get_3d_grid, distance=euclidean_distances):
        self.alpha = 1
        self.distance = distance
        self.grid = grid_type(width, height, depth)
        self._neurons_count = width * height * depth
        self._base_neighborhood = max(max(max(self.grid[:, 0]), max(self.grid[:, 1])), max(self.grid[:, 2])) / 2.0
        #may need to change or add as parameter [ for jurjut]
        self.neurons = None
        self.iterations = None

    def fit(self, data):
        n, dim = data.shape
        self.neurons = np.zeros([self._neurons_count, dim])  # vectors of size equal to expected input data
        self.iterations = n

        # Init neurons with random data points
        sample = np.random.permutation(range(n))    # take a permutation of the input data
        sample = sample[0:self._neurons_count]      # take only the first neuron count indexes

        for p, i in enumerate(sample):              # init neurons
            self.neurons[p, :] = np.array(data[i, :])
            #print(self.neurons[p, :])
        #print(self.neurons)

        data_index_order = np.random.permutation(range(n))  # create an order for taking data points

        t = 0
        for i in data_index_order:
            x_i = np.array(data[i, :])
            #print(x_i, i, data.shape)
            bmu_index = self.__find_BMU(x_i)
            neuron_neighbors_index = self.__get_neighbors(bmu_index, t)

            # update neurons
            for neuron_index in neuron_neighbors_index:
                m_i = self.neurons[neuron_index, :]
                self.neurons[neuron_index, :] = m_i + self.alpha * (x_i - m_i)  # may also need to change (jurjut)

            self.alpha -= 1 / float(self.iterations)
            t += 1


        return self.__projection(data)          # we only need the 3D representation of the data


    def __find_BMU(self, x):
        closest_neuron = -1
        closest_distance = -1

        for i, neuron in enumerate(self.neurons):
            #print(x, neuron)
            dist = self.distance(x, neuron)
            if dist < closest_distance or closest_distance == -1:
                closest_distance = dist
                closest_neuron = i

        return closest_neuron

    def __get_neighbors(self, neuron_idx, t):
        l = self.iterations / log(self._base_neighborhood)
        radius = self._base_neighborhood * radial_kernel(t, l)
        neuron_coord = np.array(self.grid[neuron_idx, :])
        neighbors = []

        for i, coord in enumerate(self.grid):
            coord = np.array(coord)
            if self.distance(coord, neuron_coord) <= radius:  # this will include the center neuron too
                neighbors.append(i)

        return neighbors

    def __projection(self, data):
        _, dim = self.grid.shape
        n, _ = data.shape

        print(n, dim)
        projections = np.zeros(shape=(n, dim))

        # project data in 3D
        for i in range(n):
            bmu_index = self.__find_BMU(np.array(data[i, :]))
            projections[i, :] = self.grid[bmu_index, :]    # not sure if it will work as is

        return projections


if __name__ == "__main__":
    ko_map = KohonenMap3D(10, 10, 10)

    X = np.random.rand(2000, 71)
    projections = ko_map.fit(X)

