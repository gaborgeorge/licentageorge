import numpy as np
import multiprocessing as mp
import sys
import time
import os

from math import pow, sqrt, log
from numba import njit
from concurrent.futures import ThreadPoolExecutor

MERGE = ''

alpha = 1.0

def get3DGridAsPoints(width, height, depth):
    grid = np.zeros((width * height * depth, 3), dtype=np.int32)
    n = 0
    for i in range(width):
        for j in range(height):
            for k in range(depth):
                grid[n, 0] = i
                grid[n, 1] = j
                grid[n, 2] = k
                n += 1
    return grid

@njit
def getRadialKernel(t, lamb):
    return pow(2.17828, (-(t/lamb)))

@njit
def getEuclideanDistance(x1, x2):
    d = np.sum(((x1 - x2)**2))
    return sqrt(d)

@njit
def computeBaseNeighborhoodFromGrid(X):
    return np.max(X) / 3

@njit
def findBMU(x, neurons):
    closest_index = 0
    closest_dist = getEuclideanDistance(x, neurons[0])
    for i in range(1, neurons.shape[0]):
        dist = getEuclideanDistance(x, neurons[i])
        if dist < closest_dist:
            closest_dist = dist
            closest_index = i
    return closest_index

@njit
def getProjection(grid, data, neurons):
    projection = np.zeros((data.shape[0], 3), dtype=np.float32)

    for i in range(data.shape[0]):
        bmuIndex = findBMU(data[i], neurons)
        projection[i, 0] = grid[bmuIndex, 0]
        projection[i, 1] = grid[bmuIndex, 1]
        projection[i, 2] = grid[bmuIndex, 2]

    return projection

@njit
def getCentroProjection(grid, data, neurons):
    projection = np.zeros((data.shape[0], data.shape[1]), dtype=np.float32)

    for i in range(data.shape[0]):
        bmuIndex = findBMU(data[i], neurons)
        projection[i] = neurons[bmuIndex]

    return projection


def projectData(grid, data, neurons):
    cpu_count = mp.cpu_count()
    with mp.Pool(cpu_count) as exe:
        results = exe.map(getProjection, np.array_split(data, cpu_count))
    result = np.vstack(results)

    return result


@njit
def updateNeurons(neighbours, xi, neurons, alpha, n):
    for n_in in range(neighbours.shape[0]):
        for j in range(neurons.shape[1]):
            old_val = neurons[neighbours[n_in], j]
            neurons[neighbours[n_in], j] = old_val + alpha * (xi[j] - old_val)

    return alpha - 1 / n

class SOM:
    '''
    parameters: - width: first dimension of the grid
                - height: second dimension of the grid
                - depth: third dimension of the grid

    Description: - width * height * depth will be the numbers of clusters(colors)
    '''
    def __init__(self, width, height, depth):
        self.width = width
        self.height = height
        self.depth = depth
        self.alpha = alpha
        self.grid = get3DGridAsPoints(width, height, depth)
        self.neurons_count = width * height * depth
        self.baseNeightborhood = computeBaseNeighborhoodFromGrid(self.grid)
        self.iterations = None
        self.neurons = None


    def getNeighbours(self, bmuIndex, t):
        neightbours = []

        l = self.iterations / log(self.baseNeightborhood)
        radius = self.baseNeightborhood * getRadialKernel(t, l)
        neuronCoord = self.grid[bmuIndex]

        for i in range(self.grid.shape[0]):
            if getEuclideanDistance(self.grid[i], neuronCoord) <= radius:
                neightbours.append(i)

        return np.array(neightbours)

    '''
    parameters: - data : 2D array where:
                    - Dimension 1 = number of samples
                    - Dimension 2 = number of features per samples (neurons)
    return:     - None

    Description: - Maps a set of vectors to the input space (samples), like a
        clustering
    '''
    def fit(self, data):
        print('[{0}.{1}] STARTED'.format(self.__class__.__name__, sys._getframe().f_code.co_name))
        start = time.time()

        n = data.shape[0]
        sampleRandomPerm = np.random.permutation(n)[0:self.neurons_count]
        self.neurons = data[sampleRandomPerm]
        #self.neurons = data[np.arange(self.neurons_count)]

        #np.savetxt('NEURONS_INIT_P.txt', self.neurons)

        self.iterations = n

        takenDataIndexOrder = np.random.permutation(n)

        bmu_indexs = np.arange(n)
        file = '../../NEURONS_TRAIN_' + str(self.neurons_count) + '.npy'
        ''' # NOTE: this if is for the DEMO, it sould be removed'''
        if os.path.isfile(file) is True:
            self.neurons = np.load(file)
        else:
            for i in range(n):
                xi = data[i]
                bmuIndex = findBMU(xi, self.neurons)
                bmu_indexs[i] = bmuIndex

                neighbours = self.getNeighbours(bmuIndex, takenDataIndexOrder[i])
                #neighbours = self.getNeighbours(bmuIndex, i)

                #self.neurons[neighbours] = self.neurons[neighbours] + \
                #                             self.alpha * (xi - self.neurons[neighbours])

                self.alpha = updateNeurons(neighbours, xi,self.neurons, self.alpha, n)

            np.save(file, self.neurons)

        print('[{0}.{1}] COMPLETE - time {2}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, time.time() - start))

    '''
    parameters: - data: - 2D array where:
                            - Dimension 1: number of samples
                            - Dimension 2: number of neurons per sample(neurons)
    return: - 2D array where:
                        - Dimension 1: number of samples
                        - Dimension 2: number of color channels (3)

    Description: - for every sample finds the colors cluster, each cluster is
                        in a 3D grid, and its coordinates in the grid are assign
                        to it
    '''
    def project(self, data):
        print('[{0}.{1}] STARTED'.format(self.__class__.__name__, sys._getframe().f_code.co_name))
        start = time.time()

        file = '../../NEURONS_PROJECT_' + MERGE + str(self.neurons_count) + '.npy'

        if os.path.isfile(file) is True:
            result = np.load(file)
        else:
            result = getProjection(self.grid, data, self.neurons)
            np.save(file, result)

        print('[{0}.{1}] COMPLETE - dataset shape {2}, time {3}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, result.shape, time.time() - start))
        return result

    def centro_project(self, data):
        print('[{0}.{1}] STARTED'.format(self.__class__.__name__, sys._getframe().f_code.co_name))
        start = time.time()

        file = '../../CENTRO_PROJECT_' + MERGE + str(self.neurons_count) + '.npy'
        if os.path.isfile(file) is True:
            result = np.load(file)
        else:
            result = getCentroProjection(self.grid, data, self.neurons)
            np.save(file, result)

        print('[{0}.{1}] COMPLETE - dataset shape {2}, time {3}'.format(self.__class__.__name__, sys._getframe().f_code.co_name, result.shape, time.time() - start))
        return result

if __name__ == "__main__":
    ko_map = SOM(10, 10, 10)

    X = np.random.rand(20000, 71)
    ko_map.fit(X)

    projections = ko_map.project(X)
