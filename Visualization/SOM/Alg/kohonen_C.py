import numpy as np
import time
import subprocess

def list_to_string(list):
    str_to_ret = ""
    for el in list:
        str_to_ret += str(el) + " "
    str_to_ret += "\n"
    #print(str_to_ret)
    return str_to_ret

def write_data_to_file_for_C(data, size):
    f = open("data.txt", "w")
    f.write(str(size) + "\n")
    f.write(str(data.shape[0]) + " " + str(data.shape[1]) +"\n")
    for d in data:
        #print(list_to_string(d))
        f.write(list_to_string(d))
    f.close()


def read_data_generated_from_C(data):
    return np.loadtxt('projection.txt')

def getProjectionForOrientation(data, size):
    print("Write data to file...")
    write_data_to_file_for_C(data, size)

    print("Start subprocess...")
    start = time.time()
    p1 = subprocess.Popen('D:\\Programming\\Licenta\\Neuro-final-alex\\neuroscience-final\\Kohonen3DC\\x64\\Release\\OpenCVApplication.exe').wait()

    print("Read data from file...")
    projections = read_data_generated_from_C(data)
    print('Total time', time.time() - start)
    return projections