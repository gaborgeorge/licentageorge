import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

import numpy as np
import cv2 as cv
import time

from numba import njit

from DataSet.TrialMetadataReader import TrialMetadataReader
from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialExtractor import TrialExtractor
from config import ssd_file, result_file, data_dir
from Visualization.Plots_cv import plot_bands_cv
from Visualization.PSTH.PSTH_data_builder import PSTH_build_histograms
from Visualization.Color.colormap import colormap

'''
Parameters:     - img: 3D array (image) where:
                        - Dimension 1 = number of rows in image
                        - Dimension 2 = number of cols in image
                        - Dimesnion 3 = number of channels per color (usually 3)
                - orientaions: 3D array where:
                        - Dimension 1 = number of bands to be drawned
                        - Dimension 2 = number of rows in each band
                        - Dimesnino 3 = number of cols (intensities) in each row
                - shpae: how many pixel rows should be skipped when drawing new
                            band
                - color_height: how many pixel rows a color should be
                - color_width: how many pixel cols a color should be

Result:         - None

Description:    - this will use color RED and the values in the --orientaions--
                    will be the intensity of RED
'''
@njit
def PSTH_color_img(img, orientations, space, color_height, color_width):
    offset = 0

    # orientations.shape[0] = nunber orientations (bands in img)
    # orientations.shape[1] = number of trials per orientations (rows in img)
    # orientations.shape[2] = number of values per trial (cols in img)

    for k in range(orientations.shape[0]):
        for i in range(orientations.shape[1]):
            for j in range(orientations.shape[2]):
                for h in range(color_height):
                    row_idx = color_height * (i + offset) + h
                    for w in range(color_width):
                        col_idx = color_width * j + w
                        img[row_idx, col_idx, 0] = 0
                        img[row_idx, col_idx, 1] = 0
                        img[row_idx, col_idx, 2] = orientations[k, i, j]

        offset += orientations.shape[1] + space


'''
Params: - trial_extractor = instance of DataSet.TrialExtractor
        - winodw_size     = interval of values to be used
        - window_step     = the step between consecutive windows
        - title           = title of the figure
        - save            = if it should save the image
        - save_file       = if save is true where to save the image
        - color_height    = how many row pixels for a color
        - color_width     = how many colomn pixels for a color
        - events          = where to draw vertical lines

Result: - 3D array representing the image of the heatmap
'''
def __plot_heatmap(trial_extractor, window_size, window_step, title, save=False, save_file='', color_height=10, color_width=1, events=(0, 2672, 3173)):
    psth_dataset = PSTH_build_histograms(trial_extractor=trial_extractor, window_size=window_size, window_step=window_step)

    # scale to 0-255
    psth_dataset = psth_dataset * 255 // np.amax(psth_dataset)
    #psth_dataset = (((psth_dataset - 0) * (255 - 150)) / (np.amax(psth_dataset) - 0)) + 150
    heatmap = plot_bands_cv(psth_dataset, [0, 45, 90, 135, 180, 225, 270, 315], color_func=PSTH_color_img,
                            events=events, color_height=color_height, color_width=color_width,
                            title=title,
                            save=save, save_name=save_file)
    return heatmap


def plot_heatmap(trial_extractor, window_size, window_step, title, contrast=0, save=False, save_file='', color_height=10, color_width=1, events=(0, 2672, 3173)):
    psth_dataset = PSTH_build_histograms(trial_extractor=trial_extractor, window_size=window_size, window_step=window_step, contrast=contrast)

    colors = colormap(psth_dataset)

    heatmap = plot_bands_cv(colors, [0, 45, 90, 135, 180, 225, 270, 315],
                            events=events, color_height=color_height, color_width=color_width,
                            title=title,
                            save=save, save_name=save_file)
    return heatmap

'''
Params: - window_sizes = list of int values to be used as the size of the window
        - window_steps  = list of int values to be used as the step of the window

Return: None

Description: Will save an image for each combination of values in window_sizes
                and window_steps
'''
def heatmap_plots(window_sizes, window_steps):
    spike_reader = SpikeTimestampReader(data_dir, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir, result_file))
    trial_extractor = TrialExtractor(spike_reader=spike_reader, trial_meta_reader=trial_meta_reader)

    for window_size in window_sizes:
        for window_step in window_steps:
            __plot_heatmap(trial_extractor=trial_extractor, window_size=window_size, window_step=window_step,
                            title='HEATMAP window size ' + str(window_size) + 'window increment ' + str(window_step) + ' trial extracted random',
                            save=True, save_file=os.path.join('Results', 'HEATMAP_window_size_' + str(window_size) + '_window_step_' + str(window_step) + '_trials_random.png'))

'''
Parameters: - window_sizes, list of the sizes to be used for the windows
            - window_steps, list of window steps
            - contrast, list of contrast values, 0 means that all contrasts
                        will be used
            - use_start, if the part of the trials between Begin trial and
                        Stimul ON should be considered
            - use_end, if the part of the trials between Stimul OFF and
                        End trial should be considered
            - save_dir, the folder where the resulted figure will be saved

Return: None

Description: For every combination of values from window_sizes, window_steps and
        contrast a figure will be created. The figure will present the PSTH
        histogramfor every orientation the activity, on a JET scale, of every channel.
'''
def heatmap_plots_colormap(window_sizes, window_steps, contrasts, use_start=False,
                            use_end=True, save_dir='Results'):
    data_dir_1 = os.path.join(project_path, data_dir)
    spike_reader = SpikeTimestampReader(data_dir_1, ssd_file)
    trial_meta_reader = TrialMetadataReader(os.path.join(data_dir_1, result_file))
    trial_extractor = TrialExtractor(spike_reader=spike_reader, trial_meta_reader=trial_meta_reader)

    if use_start is True:
        start_desc = 'From_Strart_'
    else:
        start_desc = 'From_ON_'

    if use_end is True:
        end_desc = 'To_end_'
    else:
        end_desc = 'To_OFF_'

    for window_size in window_sizes:
        for window_step in window_steps:
            for contrast in contrasts:

                w_size_desc = 'Window_Size_' + str(window_size) + '_'
                w_step_desc = 'Window_Step_' + str(window_step) + '_'
                if contrast is -1:
                    contrast_desc = ''
                else:
                    contrast_desc = 'Contrast_' + str(contrast) + '_'

                title = 'HEATMAP_' + w_size_desc + w_step_desc + contrast_desc + start_desc + end_desc

                plot_heatmap(trial_extractor=trial_extractor, window_size=window_size, window_step=window_step, contrast=contrast,
                            title=title,
                            save=True,
                            save_file=os.path.join(save_dir, title + '.png'))


if __name__ == '__main__':
    heatmap_plots_colormap(window_sizes=[100], window_steps=[1], contrasts=[100], save_dir='DEMO_RESULTS')
