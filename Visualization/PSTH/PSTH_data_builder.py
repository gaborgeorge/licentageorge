import sys
import os
project_path = os.path.join('..', '..')
sys.path.append(project_path)

import numpy as np
import time

from numba import njit

from config import data_dir, ssd_file, result_file
from Utils.KernelDecay import apply_kernel_zeros_neuron, apply_kernel_function_trial

trial_neuron_values_cnt = 3300
'''
parameters: - trials_windows - a 4D array where:
                                    - Dimension 1 = number of trials
                                    - Dimension 2 = number of neurons per trial
                                    - Dimension 3 = number of windows per neuron
                                    - Dimension 4 = number of values per window
            - values_windows - a 2D array where:
                                    - Dimension 1 = number of neurons
                                    - Dimension 2 = number of values(windows)
return: None

Description: - counts how many non-zero values are in the windows corresponding
                to one neuron in all trials

OBS - usually all trials should be for the same orientation
'''
@njit
def __PSTH_apply_window_3D(trials_windows, values_windows):
    # loop over trial
    for t in range(trials_windows.shape[0]):
        # loop over neurons
        for i in range(trials_windows.shape[1]):
            # loop over windows
            for j in range(trials_windows.shape[2]):
                # loop over values
                for k in range(trials_windows.shape[3]):
                    # value in window != 0
                    if trials_windows[t, i, j, k] != 0.0:
                        # index i, j will reprezent the value j of neuron i in every trial
                        values_windows[i, j] += 1


'''
parameters:  - orientations_trials - a 4D array where:
                        - Dimension 1 = number of orientation
                        - Dimension 2 = number of trials per orientation
                        - Dimension 3 = number of neurons per trial
                        - Dimension 4 = number of values per trial
             - window_size - how many continuous values to put in one window
             - window_step - how many elements to pass until next window

return       - orientations_window_values - a 3D array where:
                        - Dimension 1 = number of orientation
                        - Dimesnion 2 = number of neurons per trial
                        - Dimension 3 = number of windows that fitted

Description: - for every orientation will call --__PSTH_apply_window_3D-- to its
                trials which will count how many times a specific neuron has
                been activ during an interval of values (window_size)

OBS:         - this function uses STRIDES to create the windows of time, make use
                orientations_trials has not been TRANSPOSED !!!!!
             - because of window_size their may be values that remain unused because they
                dont make a window (the last values), a message will appear in this case
'''
def __PSTH_histogram_4D(orientations_trials, window_size, window_step):
    neurons_cnt = orientations_trials.shape[2]
    values_cnt = orientations_trials.shape[3]

    windows_cnt = (values_cnt - window_size) / window_step + 1
    # check if elements will not be considered
    if windows_cnt.is_integer() is False:
        print('[{0}] Incomplit aplication window of size {1} with increment {2}. Number of windows resulted {3}'
            .format(sys._getframe().f_code.co_name, window_size, window_step, windows_cnt))
    windows_cnt = int(windows_cnt)

    orientations_window_values = np.zeros((orientations_trials.shape[0], neurons_cnt, windows_cnt), dtype=np.uint32)
    for i in range(orientations_window_values.shape[0]):
        # extract all trials for orientation i and convert them
        trials = orientations_trials[i]
        trials_windows = np.lib.stride_tricks.as_strided(trials, shape=(trials.shape[0], neurons_cnt, windows_cnt, window_size),
                                strides=(trials.strides[0], trials.strides[1], window_step * trials.itemsize, trials.itemsize))
        __PSTH_apply_window_3D(trials_windows=trials_windows, values_windows=orientations_window_values[i])

    return orientations_window_values


'''
parameters: - trial_extractor - instance of class DataSet/TrialExtractor.py
            - window_size     - how many values in time should be added up
            - window_step      - how many values should be skipped until next
                                    windows
            - count           - how many trials should be used for each
                                    orientation
            - orientations_cnt - how many orientations should be considered

return:     - the result of appling --__PSTH_histogram_4D-- on the trials taken from the
                trial_extractor

Description: - this will randomly choose --count-- trials for every orientation
'''
def PSTH_build_histograms(trial_extractor, window_size=10, window_step=1, contrast=0, count=10):
    print('[{0}] START'.format(sys._getframe().f_code.co_name))
    orientations_cnt=8
    orientations = [[], [], [], [], [], [], [], []]
    trials = [[], [], [], [], [], [], [], []]
    orientation_inc = 45

    # put the index of each trial in a list corresponding to its orientation
    if contrast == 0:
        for i in range(len(trial_extractor.trials)):
            orientation_idx = trial_extractor.trials[i]['meta']['direction'] // orientation_inc
            orientations[orientation_idx].append(i)
    else:
        for i in range(len(trial_extractor.trials)):
            if contrast == trial_extractor.trials[i]['meta']['contrast']:
                orientation = trial_extractor.trials[i]['meta']['direction'] // orientation_inc
                orientations[orientation].append(i)

    for i in range(len(orientations)):
        permutation = np.arange(len(orientations[i]))
        np.random.shuffle(permutation)
        permutation = permutation[:count]

        for j in permutation:
            trial_values = trial_extractor.trials[orientations[i][j]]['spikes']

            # should be neurons on rows, and values on cols, which we need
            trial_decay = apply_kernel_function_trial(trial_values, trial_extractor.mili_duration, 0, apply_kernel_zeros_neuron)

            trials[i].append(trial_decay)

    dataset = []
    for i in range(len(trials)):
        dataset.append(np.vstack(trials[i]))
    dataset = np.vstack(dataset)
    dataset = dataset.reshape((orientations_cnt, count, trial_extractor.spike_reader.metadata.no_units, trial_extractor.mili_duration))

    result = __PSTH_histogram_4D(dataset, window_size, window_step)

    print('[{0}] COMPLETE - dataset shape {1}'.format(sys._getframe().f_code.co_name, result.shape))
    return result


'''-------------------------------------------------------'''
'''--- THE REST OF THE FUCNTION ARE NOT CURRENTLY USED ---'''
'''-------------------------------------------------------'''

'''
parameters: - trials: a 3D array where:
                    - Dimension 1 = number of trials
                    - Dimension 2 = number of neurons per trial
                    - Dimension 3 = number of values per neuron
            - window: value of time used to gather values (time interval)
            - window_step: how much the window is moving at each step

return: 2D array where:
            - Dimension 1 = number of neurons neurons
            - Dimension 2 = number of windows

OBS:        - works similar to --__PSTH_histogram_4D--, the difference is that this is
                like taking all trials for a single orienation
'''
def __PSTH_histogram_3D(trials, window_size, window_step):
    rows = trials.shape[1]
    values_cnt = trials.shape[2]

    windows_cnt = (values_cnt - window_size) / window_step + 1

    if windows_cnt.is_integer() is False:
        print('[{0}] Incomplit aplication window of size {1} with increment {2}. Number of windows resulted {3}'
            .format(sys._getframe().f_code.co_name, window_size, window_step, windows_cnt))
    windows_cnt = int(windows_cnt)
    trials_windows = np.lib.stride_tricks.as_strided(trials, shape=(trials.shape[0], rows, windows_cnt, window_size),
                            strides=(trials.strides[0], trials.strides[1], window_step * trials.itemsize, trials.itemsize))
    values_windows = np.zeros((trials_windows.shape[1], trials_windows.shape[2]), dtype=np.uint32)

    __PSTH_apply_window_3D(trials_windows, values_windows)

    return values_windows


'''
parameters: - trial_windows: 3D array where:
                - Dimesnion 1 = number of neurons
                - Dimension 2 = number of windows per neuron
                - Dimesnion 3 = number of values per window
            - values_windows: 2D array where:
                - Dimension 1 = number of neurons
                - Dimension 2 = number of windows per neuron

result: None

Description: - it summes the values in each window
'''
@njit
def PSTH_apply_window_2D(trial_windows, values_windows):
    for i in range(trial_windows.shape[0]):
        for j in range(trial_windows.shape[1]):
            for k in range(trial_windows.shape[2]):
                values_windows[i, j] += trial_windows[i, j, k]


'''
parameters: - trial: a 2D array where:
                    - Dimension 1: number of neurons
                    - Dimension 2: number of values per neuron
            - window: value of timp used to gather values (time interval)
            - window_step: how much the window is moving at each step
return      - 2-D array where:
                    - Dimension 1: number of neurons
                    - Dimension 2: values for each aplication of window
'''
def __PSTH_convert_2D(trial, window_size, window_step=1):
    rows = trial.shape[0]
    cols = trial.shape[1]

    windows_cnt = (cols - window_size) / window_step + 1
    if windows_cnt.is_integer() is False:
        print('[{0}] Incomplit aplication window of size {1} with increment {2}. Number of windows resulted {3}'
            .format(sys._getframe().f_code.co_name, window_size, window_step, windows_cnt))
    windows_cnt = int(windows_cnt)
    trial_windows = np.lib.stride_tricks.as_strided(trial, shape=(rows, windows_cnt, window_size), strides=(cols * trial.itemsize, window_step * trial.itemsize, trial.itemsize))

    values_windows = np.zeros((trial_windows.shape[0], trial_windows.shape[1]), dtype=np.uint32)

    PSTH_apply_window_2D(trial_windows, values_windows)

    return values_windows


'''
The following are test functions
'''

def test_histogram_dim_3D():
    trials = np.random.randint(3, size=(2, 3, 10))
    print(trials)
    result = __PSTH_histogram_3D(trials, 3, 1)
    print(result)

def test_histogram_dim_2D():
    trial = np.random.randint(2, size=(5, 10))
    print(trial)
    __PSTH_convert_2D(trial, 3, 1)

def test_histogram_dim_4D():
    orientations_trials = np.random.randint(2, size=(2, 2, 3, 4))
    print(orientations_trials)
    result = __PSTH_histogram_4D(orientations_trials, 3, 1)
    print(result)

if __name__ == '__main__':
    test_histogram_dim_3D()
    test_histogram_dim_2D()
    print('ok')
