\contentsline {chapter}{Lista tabelelor}{iv}% 
\contentsline {chapter}{Lista figurilor}{v}% 
\contentsline {chapter}{Capitolul{} \numberline {1}Introducere}{1}% 
\contentsline {section}{\numberline {1.1}Contextul proiectului}{2}% 
\contentsline {section}{\numberline {1.2}Tema lucr\IeC {\u a}rii}{2}% 
\contentsline {section}{\numberline {1.3}Structura lucr\IeC {\u a}rii}{3}% 
\contentsline {chapter}{Capitolul{} \numberline {2}Obiectivele Proiectului}{4}% 
\contentsline {section}{\numberline {2.1}Motiva\IeC {\textcommabelow t}ia}{4}% 
\contentsline {section}{\numberline {2.2}Obiective}{5}% 
\contentsline {section}{\numberline {2.3}Dificult\IeC {\u a}\IeC {\textcommabelow t}i}{5}% 
\contentsline {section}{\numberline {2.4}Cerin\IeC {\textcommabelow t}e func\IeC {\textcommabelow t}ionale}{5}% 
\contentsline {section}{\numberline {2.5}Cerin\IeC {\textcommabelow t}e nonfunc\IeC {\textcommabelow t}ionale}{6}% 
\contentsline {section}{\numberline {2.6}Caz de utilizare}{6}% 
\contentsline {chapter}{Capitolul{} \numberline {3}Studiu Bibliografic}{8}% 
\contentsline {section}{\numberline {3.1}Tehnici de extragere a activit\IeC {\u a}\IeC {\textcommabelow t}ii neuronilor}{8}% 
\contentsline {section}{\numberline {3.2}Vizualizarea tiparelor de desc\IeC {\u a}rcare}{9}% 
\contentsline {section}{\numberline {3.3}\IeC {\^I}nv\IeC {\u a}\IeC {\textcommabelow t}area automat\IeC {\u a} aplicat\IeC {\u a} pe date extrase din creier}{11}% 
\contentsline {subsection}{\numberline {3.3.1}Clasificare utiliz\IeC {\^a}nd tipare reie\IeC {\textcommabelow s}ite din metode de vizualizare}{12}% 
\contentsline {section}{\numberline {3.4}Clasificarea datelor temporale}{12}% 
\contentsline {chapter}{Capitolul{} \numberline {4}Analiz\u {a} \c {s}i Fundamentare Teoretic\u {a}}{14}% 
\contentsline {section}{\numberline {4.1}Descrierea experimentului.}{14}% 
\contentsline {subsection}{\numberline {4.1.1}Provoc\IeC {\u a}rile introduse de setul de date}{15}% 
\contentsline {subsection}{\numberline {4.1.2}Amplitudinea desc\IeC {\u a}rc\IeC {\u a}rii}{16}% 
\contentsline {section}{\numberline {4.2}Responsabilit\IeC {\u a}\IeC {\textcommabelow t}ile sistemului}{16}% 
\contentsline {subsection}{\numberline {4.2.1}Procesarea datelor}{17}% 
\contentsline {subsection}{\numberline {4.2.2}Convolu\IeC {\textcommabelow t}ia \IeC {\^\i }n timp}{18}% 
\contentsline {section}{\numberline {4.3}Vizualizare}{19}% 
\contentsline {subsection}{\numberline {4.3.1}Hart\IeC {\u a} auto-organizatoare}{19}% 
\contentsline {subsubsection}{Func\IeC {\textcommabelow t}ionare}{20}% 
\contentsline {subsection}{\numberline {4.3.2}PSTH}{22}% 
\contentsline {subsubsection}{Func\IeC {\textcommabelow t}ionare}{23}% 
\contentsline {subsubsection}{Scara de culori}{23}% 
\contentsline {section}{\numberline {4.4}Clasificare}{24}% 
\contentsline {subsection}{\numberline {4.4.1}C\IeC {\^a}\IeC {\textcommabelow s}tigul de informa\IeC {\textcommabelow t}ie}{24}% 
\contentsline {subsection}{\numberline {4.4.2}Arbori de decizie}{25}% 
\contentsline {subsection}{\numberline {4.4.3}Support Vector Machines}{25}% 
\contentsline {subsection}{\numberline {4.4.4}Re\IeC {\textcommabelow t}ele neuronale convolu\IeC {\textcommabelow t}ionale}{26}% 
\contentsline {subsection}{\numberline {4.4.5}Tr\IeC {\u a}s\IeC {\u a}turi considerate pentru clasificare}{27}% 
\contentsline {subsubsection}{Histograma culorilor}{27}% 
\contentsline {subsubsection}{\IeC {\^I}ntreaga prob\IeC {\u a}}{27}% 
\contentsline {section}{\numberline {4.5}Evaluarea clasificatorilor}{28}% 
\contentsline {subsection}{\numberline {4.5.1}Matricea de confuzie}{28}% 
\contentsline {subsection}{\numberline {4.5.2}Acurate\IeC {\textcommabelow t}ea}{29}% 
\contentsline {chapter}{Capitolul{} \numberline {5}Proiectare de Detaliu \c {s}i Implementare}{30}% 
\contentsline {section}{\numberline {5.1}Tehnologii utilizate}{30}% 
\contentsline {subsubsection}{Python}{30}% 
\contentsline {subsubsection}{Biblioteci utilizate}{31}% 
\contentsline {section}{\numberline {5.2}Arhitectura sistemului}{31}% 
\contentsline {section}{\numberline {5.3}Extragerea datelor}{32}% 
\contentsline {subsection}{\numberline {5.3.1}Fi\IeC {\textcommabelow s}ierele de date}{32}% 
\contentsline {subsubsection}{MetadataReader}{33}% 
\contentsline {subsubsection}{TrialMetadataReader}{34}% 
\contentsline {subsubsection}{SpikeAndTimestampReader}{34}% 
\contentsline {subsubsection}{TrialExtractor}{35}% 
\contentsline {section}{\numberline {5.4}Utilitare}{36}% 
\contentsline {subsubsection}{KernelDecay}{36}% 
\contentsline {subsubsection}{InfoGain}{37}% 
\contentsline {section}{\numberline {5.5}Modulul de vizualizare}{37}% 
\contentsline {subsubsection}{Colormap}{38}% 
\contentsline {subsubsection}{PlotsCV}{38}% 
\contentsline {subsection}{\numberline {5.5.1}Vizualizarea colectiv\IeC {\u a} a canalelor}{39}% 
\contentsline {subsubsection}{SomDataBuilder}{39}% 
\contentsline {subsubsection}{SomProjectionsUtils}{40}% 
\contentsline {subsubsection}{SOM}{41}% 
\contentsline {subsubsection}{SomPlots}{41}% 
\contentsline {subsubsection}{SomPlotsGenerator}{41}% 
\contentsline {subsubsection}{SomGainVisualOnlyBestColors}{42}% 
\contentsline {subsection}{\numberline {5.5.2}Vizualizarea individual\IeC {\u a} a canalelor}{42}% 
\contentsline {subsubsection}{PsthDataBuilder}{42}% 
\contentsline {subsubsection}{PsthHeatmapPlot}{43}% 
\contentsline {section}{\numberline {5.6}Modulul de clasificare}{43}% 
\contentsline {subsubsection}{ClassificationUtils}{43}% 
\contentsline {subsection}{\numberline {5.6.1}Clasificatori}{44}% 
\contentsline {subsubsection}{DecisionTree}{44}% 
\contentsline {subsubsection}{SupportVectorMachines}{44}% 
\contentsline {subsubsection}{CNN1d}{44}% 
\contentsline {subsection}{\numberline {5.6.2}Clasificarea datelor utiliz\IeC {\^a}nd tipare rezultate \IeC {\^\i }n urma clusteriz\IeC {\u a}rii}{45}% 
\contentsline {subsubsection}{SomDatasetBuilder}{46}% 
\contentsline {subsubsection}{SomClassification}{46}% 
\contentsline {subsubsection}{SomTool}{46}% 
\contentsline {subsection}{\numberline {5.6.3}Clasificarea datelor utiliz\IeC {\^a}nd convolu\IeC {\textcommabelow t}ia \IeC {\^\i }n timp}{47}% 
\contentsline {subsubsection}{RawUtils}{47}% 
\contentsline {subsubsection}{RawDatasetBuilder}{47}% 
\contentsline {subsubsection}{RawClassification}{47}% 
\contentsline {subsubsection}{RawTool}{48}% 
\contentsline {chapter}{Capitolul{} \numberline {6}Testare \c {s}i Validare}{50}% 
\contentsline {section}{\numberline {6.1}Evaluarea ipotezelor de codificare, prin metode de vizualizare}{50}% 
\contentsline {subsection}{\numberline {6.1.1}Vizualizarea activit\IeC {\u a}\IeC {\textcommabelow t}ii popula\IeC {\textcommabelow t}iei de neuroni}{50}% 
\contentsline {subsection}{\numberline {6.1.2}Vizualizarea activit\IeC {\u a}\IeC {\textcommabelow t}ii individuale pe canale}{53}% 
\contentsline {section}{\numberline {6.2}Evaluarea modulului de clasificare}{53}% 
\contentsline {subsection}{\numberline {6.2.1}Clasificarea utiliz\IeC {\^a}nd tr\IeC {\u a}s\IeC {\u a}turi reie\IeC {\textcommabelow s}ite din clusterizare}{54}% 
\contentsline {subsection}{\numberline {6.2.2}Clasificarea utiliz\IeC {\^a}nd tiparele rezultate \IeC {\^\i }n urma convolu\IeC {\textcommabelow t}iei}{56}% 
\contentsline {chapter}{Capitolul{} \numberline {7}Manual de Instalare \c {s}i Utilizare}{59}% 
\contentsline {section}{\numberline {7.1}Resurse Necesare}{59}% 
\contentsline {section}{\numberline {7.2}Instalare}{59}% 
\contentsline {section}{\numberline {7.3}Func\IeC {\textcommabelow t}ionalit\IeC {\u a}\IeC {\textcommabelow t}i}{60}% 
\contentsline {chapter}{Capitolul{} \numberline {8}Concluzii}{61}% 
\contentsline {section}{\numberline {8.1}Solu\IeC {\textcommabelow t}ia propus\IeC {\u a} \IeC {\textcommabelow s}i contribu\IeC {\textcommabelow t}iile personale}{61}% 
\contentsline {section}{\numberline {8.2}Dezvolt\IeC {\u a}ri ulterioare}{62}% 
\contentsline {chapter}{Bibliografie}{64}% 
\contentsline {chapter}{Anexa{} \numberline {A}Rezultate}{66}% 
\contentsline {chapter}{Anexa{} \numberline {B}Secven\IeC {\textcommabelow t}e relevante de cod}{76}% 
\contentsline {chapter}{Anexa{} \numberline {C}Lucr\IeC {\u a}ri publicate}{87}% 
