import os
import sys
sys.path.append(os.path.join('..', '..', '..', 'Classification', 'DatasetBuilder'))
sys.path.append(os.path.join('..', '..', '..', 'Classification', 'InfoGain'))
sys.path.append(os.path.join('..', '..', '..'))

import numpy as np
import color_dataset
import info_gain

import unittest

class TestColorDataset(unittest.TestCase):

    def test_build_color_dataset(self):

        size = (5, 5, 5)
        tau = 20
        contrast = 100

        X, Y, color_trials, desc = color_dataset.build_color_dataset(size, tau, contrast)

        orientations_values = [0, 45, 90, 135, 180, 225, 270, 315]
        plot = plot_bands_cv(color_trials, orientations_values, color_height=10, color_width=1, events=(0, 2700, 3100),
                    title=desc, save=True, save_name=)

        #info_gain.discretization(X, Y)
        #info_gain.information_gains(X, Y)

    # test for color_array_to_hist
    def test_color_array_to_hist(self):
        colors = np.array([[0, 0, 63],
                            [252, 0, 63],
                            [0, 0, 63],
                            [0, 0, 63],
                            [0, 0, 63],
                            [0, 0, 63],
                            [0, 63, 252]], dtype=np.uint32)
        color_inc = 63
        histogram = np.zeros((125,), dtype=np.uint32)

        expected_histogram = np.copy(histogram)
        expected_histogram[1] = 5
        expected_histogram[25 * 4 + 1] = 1
        expected_histogram[5 * 1 + 4] = 1

        color_dataset.color_array_to_hist(histogram, colors, color_inc)

        for i in range(expected_histogram.shape[0]):
            self.assertEqual(expected_histogram[i], histogram[i])


if __name__ == '__main__':
    unittest.main()
