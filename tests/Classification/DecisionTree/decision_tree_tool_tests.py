import unittest
import os
import sys
sys.path.append(os.path.join('..', '..', '..'))

import numpy as np

from Classification.Classifiers.DecisionTree.decision_tree_tool import save_results_split_data

class DecisionTreeTool_tests(unittest.TestCase):

    def test_save_results_split_data(self):
        ss_amp = [[[1, 2, 3, 4], [2, 3, 4, 5]], [[6, 7, 8, 9], [4, 4, 2, 1]]]
        cs_amp = [[[np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])],
                   [np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])]],
                  [[np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])],
                   [np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])]]]
        ss_mean = [[[2, 1, 2, 3], [2, 3, 2, 1]], [[4, 4, 4, 4], [1, 1, 1, 1]]]
        cs_mean = [[[np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])],
                   [np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])]],
                  [[np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])],
                   [np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]]), np.array([[1, 1], [1, 1]])]]]
        desc = 'test'
        s_iterations = 4
        c_iterations = 2
        d = ''

        save_results_split_data(ss_amp, cs_amp, ss_mean, cs_mean, desc, s_iterations, c_iterations, d)

if __name__ == '__main__':
    unittest.main()
