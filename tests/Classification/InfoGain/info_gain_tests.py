import unittest
import os
import sys
sys.path.append(os.path.join('..', '..', '..', 'Classification', 'InfoGain'))

import numpy as np

import info_gain

class TestInfoGain(unittest.TestCase):

    # entropy tests`
    def test_entropy_1_class(self):
        # test that entropy 0 when their is only one class
        test_array_0 = np.full((10,), 5)
        entropy = info_gain.entropy(test_array_0)
        self.assertEqual(entropy, 0.0)

    def test_entropy_max(self):
        # build an array with 2 classes equal in number of samples
        Y = np.hstack((np.full((5,), 1), np.full((5,), 4)))
        entropy = info_gain.entropy(Y)
        self.assertEqual(entropy, 1.0)

    def test_entropy_normal(self):
        # 9 values for 1 class and 5 for another, should be 0.94
        Y = np.hstack((np.full((9,), 1), np.full((5,), 2)))
        entropy = info_gain.entropy(Y)
        self.assertAlmostEqual(entropy, 0.94, places=2)

        # 6 for class 1, 1 for class 2, should be 0.592
        Y = np.hstack((np.full((6,), 1), np.full((1,), 2)))
        entropy = info_gain.entropy(Y)
        self.assertAlmostEqual(entropy, 0.592, places=2)

    # info gain tests
    def test_info_gain_diff_sizes(self):
        X = np.full((4,), 1)
        Y = np.full((3,), 2)

        with self.assertRaises(Exception): info_gain.information_gain(X, Y)

    def test_info_gain_normal(self):
        # values taken from https://www.youtube.com/watch?v=eLlYSpVjH94&t=336s
        X = np.array([1, 1, 1, 1, 0, 0, 0, 1, 0, 0, 0, 1, 0, 1], dtype=np.uint32)
        Y = np.array([0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0], dtype=np.uint32)
        gain = info_gain.information_gain(X, Y)
        self.assertAlmostEqual(gain, 0.151, places=2)

        X = np.array([0, 0, 1, 2, 2, 2, 1, 0, 0, 2, 0, 1, 1, 2], dtype=np.uint32)
        Y = np.array([0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0], dtype=np.uint32)
        gain = info_gain.information_gain(X, Y)
        self.assertAlmostEqual(gain, 0.246, places=2)

        X = np.array([0, 1, 0, 0, 0, 1, 1, 0, 0, 0, 1, 1, 0, 1], dtype=np.uint32)
        Y = np.array([0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0], dtype=np.uint32)
        gain = info_gain.information_gain(X, Y)
        self.assertAlmostEqual(gain, 0.048, places=2)

    def test_info_gain_min(self):
        X = np.array([1, 1, 0, 0], dtype=np.uint32)
        Y = np.array([0, 1, 0, 1], dtype=np.uint32)
        gain = info_gain.information_gain(X, Y)
        self.assertAlmostEqual(gain, 0.0, places=2)

    def test_info_gain_max(self):
        X = np.array([1, 1, 0, 0], dtype=np.uint32)
        Y = np.array([0, 0, 1, 1], dtype=np.uint32)
        gain = info_gain.information_gain(X, Y)
        self.assertAlmostEqual(gain, 1.0, places=2)

    # test sort_mirror
    def test_sort_mirror(self):
        x = np.array([1, 3, 2, 4, 6, 5, 5], dtype=np.uint32)
        y = np.array([1, 3, 2, 4, 6, 5, 5], dtype=np.uint32)
        info_gain.sort_mirror(x, y)

        result = np.array([1, 2, 3, 4, 5, 5, 6], dtype=np.uint32)
        for i in range(result.shape[0]):
            self.assertEqual(x[i], result[i])
            self.assertEqual(y[i], result[i])

    # test entropy_based_discretization
    def test_entropy_based_discretization(self):
        x = np.array([4, 5, 8, 12, 15], dtype=np.uint32)
        y = np.array([0, 1, 0, 1, 1], dtype=np.uint32)

        thresholds = info_gain.entropy_based_discretization(x, y)
        self.assertEqual(4.5, thresholds[0])
        self.assertEqual(10.0, thresholds[1])

    # test discretization
    def test_discretization(self):
        x = np.array([[4], [5], [8], [12], [15]], dtype=np.uint32)
        y = np.array([0, 1, 0, 1, 1], dtype=np.uint32)
        expected = np.array([[0], [1], [2], [3], [3]])

        x_c = info_gain.discretization(x, y)
        for i in range(expected.shape[0]):
            for j in range(expected.shape[1]):
                self.assertEqual(expected[i, j], x_c[i, j])

    # test infromation gains
    def test_information_gains(self):
        x = np.array([[0, 0, 0, 0],
                      [0, 0, 0, 1],
                      [1, 0, 0, 0],
                      [2, 1, 0, 0],
                      [2, 2, 1, 0],
                      [2, 2, 1, 1],
                      [1, 2, 1, 1],
                      [0, 1, 0, 0],
                      [0, 2, 1, 0],
                      [2, 1, 1, 0],
                      [0, 1, 1, 1],
                      [1, 1, 0, 1],
                      [1, 0, 1, 0],
                      [2, 1, 0, 1]])

        y = np.array([0, 0, 1, 1, 1, 0, 1, 0, 1, 1, 1, 1, 1, 0])
        expected_gains = np.array([0.246, 0.029, 0.151, 0.048])

        info_gains = info_gain.information_gains(x, y)

        for i in range(expected_gains.shape[0]):
            self.assertAlmostEqual(expected_gains[i], info_gains[i], places=2)

if __name__ == '__main__':
    unittest.main()
