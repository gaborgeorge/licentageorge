import unittest
import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

from DataSet.MetadataReader import MetadataReader


data_dir = os.path.join(project_path, 'Data', 'M017_0002_sorted_full')
ssd_file = 'M017_S001_SRCS3L_25,50,100_0002.ssd'

class TestMetaDataReader(unittest.TestCase):

    def test_metadata_reader(self):
        reader = MetadataReader(data_dir, ssd_file)

        no_units = 29
        no_spike_per_unit = [390, 510, 247, 115, 30, 338, 695, 220, 728, 126, 219, 506, 5988, 1381, 1603, 2786, 660, 541, 367, 599, 349, 714, 80, 178, 390, 22, 60, 47, 154]
        spike_event_sampling_freq = 32000.0
        waveform_spike_event_sampling_freq = 32000.0
        waveform_length_samples = 58
        waveform_spike_align_offset = 19
        no_events = 960
        file_spike_timestamps = 'M017_S001_SRCS3L_25,50,100_0002.ssdst'
        file_spike_waveform = 'M017_S001_SRCS3L_25,50,100_0002.ssduw'
        file_unit_statistics = 'M017_S001_SRCS3L_25,50,100_0002.ssdus'
        file_event_timestamps = 'M017_S001_SRCS3L_25,50,100_0002.ssdet'
        file_events_codes = 'M017_S001_SRCS3L_25,50,100_0002.ssdec'

        self.assertEqual(reader.no_units, no_units)
        for i in range(no_units):
            self.assertEqual(reader.no_spikes_per_unit[i], no_spike_per_unit[i])
        self.assertEqual(reader.spike_event_sampling_freq, spike_event_sampling_freq)
        self.assertEqual(reader.waveform_spike_event_sampling_freq, waveform_spike_event_sampling_freq)
        self.assertEqual(reader.waveform_length_samples, waveform_length_samples)
        self.assertEqual(reader.waveform_spike_align_offset, waveform_spike_align_offset)
        self.assertEqual(reader.no_events, no_events)
        self.assertEqual(reader.file_spike_timestamps, file_spike_timestamps)
        self.assertEqual(reader.file_spike_waveform, file_spike_waveform)
        self.assertEqual(reader.file_unit_statistics, file_unit_statistics)
        self.assertEqual(reader.file_event_timestamps, file_event_timestamps)
        self.assertEqual(reader.file_events_codes, file_events_codes)

if __name__ == '__main__':
    unittest.main()
