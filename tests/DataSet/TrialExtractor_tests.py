import unittest
import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

from DataSet.SpikeAndTimestampReader import SpikeTimestampReader
from DataSet.TrialMetadataReader import TrialMetadataReader
from DataSet.TrialExtractor import TrialExtractor

data_dir = os.path.join(project_path, 'Data', 'M017_0002_sorted_full')
result_file = 'Results M017_S001_SRCS3L_25,50,100_0002 Variable contrast, all orientations.csv'
ssd_file = 'M017_S001_SRCS3L_25,50,100_0002.ssd'

class TestTrialExtractor(unittest.TestCase):

    def test_normal(self):
        trials = TrialExtractor(SpikeTimestampReader(data_dir, ssd_file),
                                TrialMetadataReader(os.path.join(data_dir, result_file)),
                                    use_mean_neuron=True)

        trial_cnt = 240
        events_timestamps = {'on': 0, 'off': 2672, 'end': 3173}
        neurons_first_8_trials_0 = [[],
                           [(260, 39.57976792279412), (1058, 39.57976792279412), (2799, 39.57976792279412)],
                           [(2946, 61.69782704959514)],
                           [],
                           [],
                           [],
                           [(1058, 48.11379271582734), (1269, 48.11379271582734), (1610, 48.11379271582734), (1937, 48.11379271582734), (2799, 48.11379271582734)],
                           [(2946, 53.72660245028409)]]
        trial_meta = {'cond_number': 1, 'contrast': 100, 'direction': 0}

        self.assertEqual(len(trials.trials), trial_cnt)
        self.assertEqual(trials.trials[0]['events_timestamps']['on'], events_timestamps['on'])
        self.assertEqual(trials.trials[0]['events_timestamps']['off'], events_timestamps['off'])
        self.assertEqual(trials.trials[0]['events_timestamps']['end'], events_timestamps['end'])
        for i in range(8):
            for j in range(len(neurons_first_8_trials_0[i])):
                self.assertAlmostEqual(trials.trials[0]['spikes'][i][j], neurons_first_8_trials_0[i][j], places=2)
        self.assertEqual(trials.trials[0]['meta']['cond_number'], trial_meta['cond_number'])
        self.assertEqual(trials.trials[0]['meta']['contrast'], trial_meta['contrast'])
        self.assertEqual(trials.trials[0]['meta']['direction'], trial_meta['direction'])

    def test_z_score(self):
        trials = TrialExtractor(SpikeTimestampReader(data_dir, ssd_file), TrialMetadataReader(os.path.join(data_dir, result_file)),
                                use_z_score=True)
        neurons_first_8_trials_0 = [[],
                                [(260, 0.38511143414048254), (1058, 0.7812125739048134), (2799, 1.0564435862949677)],
                                [(2946, 0.5580804657510037)],
                                [],
                                [],
                                [],
                                [(1058, 0.6164506875155478), (1269, 1.402588710924038), (1610, 0.6795164221265941), (1937, 1.0335634927822521), (2799, 0.4857714108840464)],
                                [(2946, 0.7732776398777947)]]

        for i in range(8):
            for j in range(len(neurons_first_8_trials_0[i])):
                self.assertAlmostEqual(trials.trials[0]['spikes'][i][j], neurons_first_8_trials_0[i][j], places=2)
        

if __name__ == '__main__':
    unittest.main()
