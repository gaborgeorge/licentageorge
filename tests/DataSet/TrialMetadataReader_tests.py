import unittest
import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

from DataSet.TrialMetadataReader import TrialMetadataReader

data_dir = os.path.join(project_path, 'Data', 'M017_0002_sorted_full')
result_file = 'Results M017_S001_SRCS3L_25,50,100_0002 Variable contrast, all orientations.csv'

class TestTrialMetadataReader(unittest.TestCase):

    def test_trial_metadata_reader(self):
        trial_meta_reader = TrialMetadataReader(os.path.join(data_dir, result_file))

        contrasts_first_10 = [100, 100, 50, 25, 25, 50, 50, 100, 25, 100]
        directions_first_10 = [0, 90, 0, 225, 0, 45, 90, 45, 90, 315]

        for i in range(10):
            self.assertEqual(trial_meta_reader.trials_meta[i]['contrast'], contrasts_first_10[i])
        for i in range(10):
            self.assertEqual(trial_meta_reader.trials_meta[i]['direction'], directions_first_10[i])

if __name__ == '__main__':
    unittest.main()
