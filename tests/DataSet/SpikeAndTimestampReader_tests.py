import unittest
import os
import sys
project_path = os.path.join('..', '..')
sys.path.append(project_path)

from DataSet.SpikeAndTimestampReader import SpikeTimestampReader

data_dir = os.path.join(project_path, 'Data', 'M017_0002_sorted_full')
ssd_file = 'M017_S001_SRCS3L_25,50,100_0002.ssd'

class TestSpikeAndTimestampReader(unittest.TestCase):

    def test_spike_reader(self):
        spike_reader = SpikeTimestampReader(data_dir, ssd_file)

        timestamps_first_5_neuron_0 = [1006461, 1022605, 1345125, 1345869, 2955340]
        amplitudes_first_5_neuron_0 = [57.18285, 51.657085, 48.897568, 56.854607, 76.641914]
        event_timestamps_first_trial = [216688, 248753, 334260, 350293]
        event_codes_first_12 = [128, 129, 150, 192, 128, 129, 150, 192, 128, 129, 150, 192]
        mean_amplitudes_first_10_neurons = [56.33087941, 39.57976792, 61.69782705,
                                            35.12413595, 45.23347982, 37.5366182,
                                            48.11379272, 53.72660245, 43.67123111,
                                            89.76753162]
        standard_deviations_first_10_neurons = [7.07497763, 7.30123829, 11.50790021,
                                            6.00286284, 9.08006918, 4.94987297,
                                            14.09727894, 10.39635074, 10.72628077,
                                            13.50608535]


        for i in range(5):
            self.assertEqual(spike_reader.timestamps[0][i], timestamps_first_5_neuron_0[i])
        for i in range(5):
            self.assertAlmostEqual(spike_reader.amplitudes[0][i], amplitudes_first_5_neuron_0[i], places=2)
        for i in range(4):
            self.assertEqual(spike_reader.trial_timestamps[0][i], event_timestamps_first_trial[i])
        for i in range(12):
            self.assertEqual(spike_reader.event_codes[i], event_codes_first_12[i])
        for i in range(10):
            self.assertAlmostEqual(spike_reader.mean_amplitudes[i], mean_amplitudes_first_10_neurons[i], places=2)
        for i in range(10):
            self.assertAlmostEqual(spike_reader.standard_deviations[i], standard_deviations_first_10_neurons[i], places=2)

if __name__ == '__main__':
    unittest.main()
